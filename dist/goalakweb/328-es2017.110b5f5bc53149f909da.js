(self.webpackChunkgoalakweb = self.webpackChunkgoalakweb || []).push([
    [328], { 9328: function(t, e, i) { "use strict";
            i.r(e), i.d(e, { PartialsModule: function() { return ee } }); var s = i(8583),
                n = i(3018),
                o = i(4402),
                r = i(7574),
                a = i(9796),
                l = i(8002),
                c = i(1555);

            function h(t, e) { return new r.y(i => { const s = t.length; if (0 === s) return void i.complete(); const n = new Array(s); let r = 0,
                        a = 0; for (let l = 0; l < s; l++) { const c = (0, o.D)(t[l]); let h = !1;
                        i.add(c.subscribe({ next: t => { h || (h = !0, a++), n[l] = t }, error: t => i.error(t), complete: () => { r++, r !== s && h || (a === s && i.next(e ? e.reduce((t, e, i) => (t[e] = n[i], t), {}) : n), i.complete()) } })) } }) } let d = (() => { class t { constructor(t, e) { this._renderer = t, this._elementRef = e, this.onChange = t => {}, this.onTouched = () => {} }
                        setProperty(t, e) { this._renderer.setProperty(this._elementRef.nativeElement, t, e) }
                        registerOnTouched(t) { this.onTouched = t }
                        registerOnChange(t) { this.onChange = t }
                        setDisabledState(t) { this.setProperty("disabled", t) } } return t.\u0275fac = function(e) { return new(e || t)(n.Y36(n.Qsj), n.Y36(n.SBq)) }, t.\u0275dir = n.lG2({ type: t }), t })(),
                u = (() => { class t extends d {} return t.\u0275fac = function() { let e; return function(i) { return (e || (e = n.n5z(t)))(i || t) } }(), t.\u0275dir = n.lG2({ type: t, features: [n.qOj] }), t })(); const g = new n.OlP("NgValueAccessor"),
                p = { provide: g, useExisting: (0, n.Gpc)(() => f), multi: !0 },
                m = new n.OlP("CompositionEventMode"); let f = (() => { class t extends d { constructor(t, e, i) { super(t, e), this._compositionMode = i, this._composing = !1, null == this._compositionMode && (this._compositionMode = ! function() { const t = (0, s.q)() ? (0, s.q)().getUserAgent() : ""; return /android (\d+)/.test(t.toLowerCase()) }()) }
                    writeValue(t) { this.setProperty("value", null == t ? "" : t) }
                    _handleInput(t) {
                        (!this._compositionMode || this._compositionMode && !this._composing) && this.onChange(t) }
                    _compositionStart() { this._composing = !0 }
                    _compositionEnd(t) { this._composing = !1, this._compositionMode && this.onChange(t) } } return t.\u0275fac = function(e) { return new(e || t)(n.Y36(n.Qsj), n.Y36(n.SBq), n.Y36(m, 8)) }, t.\u0275dir = n.lG2({ type: t, selectors: [
                        ["input", "formControlName", "", 3, "type", "checkbox"],
                        ["textarea", "formControlName", ""],
                        ["input", "formControl", "", 3, "type", "checkbox"],
                        ["textarea", "formControl", ""],
                        ["input", "ngModel", "", 3, "type", "checkbox"],
                        ["textarea", "ngModel", ""],
                        ["", "ngDefaultControl", ""]
                    ], hostBindings: function(t, e) { 1 & t && n.NdJ("input", function(t) { return e._handleInput(t.target.value) })("blur", function() { return e.onTouched() })("compositionstart", function() { return e._compositionStart() })("compositionend", function(t) { return e._compositionEnd(t.target.value) }) }, features: [n._Bn([p]), n.qOj] }), t })(); const _ = new n.OlP("NgValidators"),
                v = new n.OlP("NgAsyncValidators");

            function b(t) { return null != t }

            function y(t) { const e = (0, n.QGY)(t) ? (0, o.D)(t) : t; return (0, n.CqO)(e), e }

            function Z(t) { let e = {}; return t.forEach(t => { e = null != t ? Object.assign(Object.assign({}, e), t) : e }), 0 === Object.keys(e).length ? null : e }

            function A(t, e) { return e.map(e => e(t)) }

            function C(t) { return t.map(t => function(t) { return !t.validate }(t) ? t : e => t.validate(e)) }

            function x(t) { return null != t ? function(t) { if (!t) return null; const e = t.filter(b); return 0 == e.length ? null : function(t) { return Z(A(t, e)) } }(C(t)) : null }

            function T(t) { return null != t ? function(t) { if (!t) return null; const e = t.filter(b); return 0 == e.length ? null : function(t) { return function(...t) { if (1 === t.length) { const e = t[0]; if ((0, a.k)(e)) return h(e, null); if ((0, c.K)(e) && Object.getPrototypeOf(e) === Object.prototype) { const t = Object.keys(e); return h(t.map(t => e[t]), t) } } if ("function" == typeof t[t.length - 1]) { const e = t.pop(); return h(t = 1 === t.length && (0, a.k)(t[0]) ? t[0] : t, null).pipe((0, l.U)(t => e(...t))) } return h(t, null) }(A(t, e).map(y)).pipe((0, l.U)(Z)) } }(C(t)) : null }

            function S(t, e) { return null === t ? [e] : Array.isArray(t) ? [...t, e] : [t, e] } let O = (() => { class t { constructor() { this._rawValidators = [], this._rawAsyncValidators = [], this._onDestroyCallbacks = [] }
                        get value() { return this.control ? this.control.value : null }
                        get valid() { return this.control ? this.control.valid : null }
                        get invalid() { return this.control ? this.control.invalid : null }
                        get pending() { return this.control ? this.control.pending : null }
                        get disabled() { return this.control ? this.control.disabled : null }
                        get enabled() { return this.control ? this.control.enabled : null }
                        get errors() { return this.control ? this.control.errors : null }
                        get pristine() { return this.control ? this.control.pristine : null }
                        get dirty() { return this.control ? this.control.dirty : null }
                        get touched() { return this.control ? this.control.touched : null }
                        get status() { return this.control ? this.control.status : null }
                        get untouched() { return this.control ? this.control.untouched : null }
                        get statusChanges() { return this.control ? this.control.statusChanges : null }
                        get valueChanges() { return this.control ? this.control.valueChanges : null }
                        get path() { return null }
                        _setValidators(t) { this._rawValidators = t || [], this._composedValidatorFn = x(this._rawValidators) }
                        _setAsyncValidators(t) { this._rawAsyncValidators = t || [], this._composedAsyncValidatorFn = T(this._rawAsyncValidators) }
                        get validator() { return this._composedValidatorFn || null }
                        get asyncValidator() { return this._composedAsyncValidatorFn || null }
                        _registerOnDestroy(t) { this._onDestroyCallbacks.push(t) }
                        _invokeOnDestroyCallbacks() { this._onDestroyCallbacks.forEach(t => t()), this._onDestroyCallbacks = [] }
                        reset(t) { this.control && this.control.reset(t) }
                        hasError(t, e) { return !!this.control && this.control.hasError(t, e) }
                        getError(t, e) { return this.control ? this.control.getError(t, e) : null } } return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275dir = n.lG2({ type: t }), t })(),
                w = (() => { class t extends O {get formDirective() { return null }
                        get path() { return null } } return t.\u0275fac = function() { let e; return function(i) { return (e || (e = n.n5z(t)))(i || t) } }(), t.\u0275dir = n.lG2({ type: t, features: [n.qOj] }), t })();
            class P extends O { constructor() { super(...arguments), this._parent = null, this.name = null, this.valueAccessor = null } } let V = (() => { class t extends class { constructor(t) { this._cd = t }
                    is(t) { var e, i, s; return "submitted" === t ? !!(null === (e = this._cd) || void 0 === e ? void 0 : e.submitted) : !!(null === (s = null === (i = this._cd) || void 0 === i ? void 0 : i.control) || void 0 === s ? void 0 : s[t]) } } { constructor(t) { super(t) } } return t.\u0275fac = function(e) { return new(e || t)(n.Y36(P, 2)) }, t.\u0275dir = n.lG2({ type: t, selectors: [
                        ["", "formControlName", ""],
                        ["", "ngModel", ""],
                        ["", "formControl", ""]
                    ], hostVars: 14, hostBindings: function(t, e) { 2 & t && n.ekj("ng-untouched", e.is("untouched"))("ng-touched", e.is("touched"))("ng-pristine", e.is("pristine"))("ng-dirty", e.is("dirty"))("ng-valid", e.is("valid"))("ng-invalid", e.is("invalid"))("ng-pending", e.is("pending")) }, features: [n.qOj] }), t })();

            function I(t, e) { t.forEach(t => { t.registerOnValidatorChange && t.registerOnValidatorChange(e) }) }

            function k(t, e) { t._pendingDirty && t.markAsDirty(), t.setValue(t._pendingValue, { emitModelToViewChange: !1 }), e.viewToModelUpdate(t._pendingValue), t._pendingChange = !1 }

            function q(t, e) { const i = t.indexOf(e);
                i > -1 && t.splice(i, 1) }

            function M(t) { return (D(t) ? t.validators : t) || null }

            function E(t) { return Array.isArray(t) ? x(t) : t || null }

            function N(t, e) { return (D(e) ? e.asyncValidators : t) || null }

            function U(t) { return Array.isArray(t) ? T(t) : t || null }

            function D(t) { return null != t && !Array.isArray(t) && "object" == typeof t }
            class F { constructor(t, e) { this._hasOwnPendingAsyncValidator = !1, this._onCollectionChange = () => {}, this._parent = null, this.pristine = !0, this.touched = !1, this._onDisabledChange = [], this._rawValidators = t, this._rawAsyncValidators = e, this._composedValidatorFn = E(this._rawValidators), this._composedAsyncValidatorFn = U(this._rawAsyncValidators) }
                get validator() { return this._composedValidatorFn }
                set validator(t) { this._rawValidators = this._composedValidatorFn = t }
                get asyncValidator() { return this._composedAsyncValidatorFn }
                set asyncValidator(t) { this._rawAsyncValidators = this._composedAsyncValidatorFn = t }
                get parent() { return this._parent }
                get valid() { return "VALID" === this.status }
                get invalid() { return "INVALID" === this.status }
                get pending() { return "PENDING" == this.status }
                get disabled() { return "DISABLED" === this.status }
                get enabled() { return "DISABLED" !== this.status }
                get dirty() { return !this.pristine }
                get untouched() { return !this.touched }
                get updateOn() { return this._updateOn ? this._updateOn : this.parent ? this.parent.updateOn : "change" }
                setValidators(t) { this._rawValidators = t, this._composedValidatorFn = E(t) }
                setAsyncValidators(t) { this._rawAsyncValidators = t, this._composedAsyncValidatorFn = U(t) }
                clearValidators() { this.validator = null }
                clearAsyncValidators() { this.asyncValidator = null }
                markAsTouched(t = {}) { this.touched = !0, this._parent && !t.onlySelf && this._parent.markAsTouched(t) }
                markAllAsTouched() { this.markAsTouched({ onlySelf: !0 }), this._forEachChild(t => t.markAllAsTouched()) }
                markAsUntouched(t = {}) { this.touched = !1, this._pendingTouched = !1, this._forEachChild(t => { t.markAsUntouched({ onlySelf: !0 }) }), this._parent && !t.onlySelf && this._parent._updateTouched(t) }
                markAsDirty(t = {}) { this.pristine = !1, this._parent && !t.onlySelf && this._parent.markAsDirty(t) }
                markAsPristine(t = {}) { this.pristine = !0, this._pendingDirty = !1, this._forEachChild(t => { t.markAsPristine({ onlySelf: !0 }) }), this._parent && !t.onlySelf && this._parent._updatePristine(t) }
                markAsPending(t = {}) { this.status = "PENDING", !1 !== t.emitEvent && this.statusChanges.emit(this.status), this._parent && !t.onlySelf && this._parent.markAsPending(t) }
                disable(t = {}) { const e = this._parentMarkedDirty(t.onlySelf);
                    this.status = "DISABLED", this.errors = null, this._forEachChild(e => { e.disable(Object.assign(Object.assign({}, t), { onlySelf: !0 })) }), this._updateValue(), !1 !== t.emitEvent && (this.valueChanges.emit(this.value), this.statusChanges.emit(this.status)), this._updateAncestors(Object.assign(Object.assign({}, t), { skipPristineCheck: e })), this._onDisabledChange.forEach(t => t(!0)) }
                enable(t = {}) { const e = this._parentMarkedDirty(t.onlySelf);
                    this.status = "VALID", this._forEachChild(e => { e.enable(Object.assign(Object.assign({}, t), { onlySelf: !0 })) }), this.updateValueAndValidity({ onlySelf: !0, emitEvent: t.emitEvent }), this._updateAncestors(Object.assign(Object.assign({}, t), { skipPristineCheck: e })), this._onDisabledChange.forEach(t => t(!1)) }
                _updateAncestors(t) { this._parent && !t.onlySelf && (this._parent.updateValueAndValidity(t), t.skipPristineCheck || this._parent._updatePristine(), this._parent._updateTouched()) }
                setParent(t) { this._parent = t }
                updateValueAndValidity(t = {}) { this._setInitialStatus(), this._updateValue(), this.enabled && (this._cancelExistingSubscription(), this.errors = this._runValidator(), this.status = this._calculateStatus(), "VALID" !== this.status && "PENDING" !== this.status || this._runAsyncValidator(t.emitEvent)), !1 !== t.emitEvent && (this.valueChanges.emit(this.value), this.statusChanges.emit(this.status)), this._parent && !t.onlySelf && this._parent.updateValueAndValidity(t) }
                _updateTreeValidity(t = { emitEvent: !0 }) { this._forEachChild(e => e._updateTreeValidity(t)), this.updateValueAndValidity({ onlySelf: !0, emitEvent: t.emitEvent }) }
                _setInitialStatus() { this.status = this._allControlsDisabled() ? "DISABLED" : "VALID" }
                _runValidator() { return this.validator ? this.validator(this) : null }
                _runAsyncValidator(t) { if (this.asyncValidator) { this.status = "PENDING", this._hasOwnPendingAsyncValidator = !0; const e = y(this.asyncValidator(this));
                        this._asyncValidationSubscription = e.subscribe(e => { this._hasOwnPendingAsyncValidator = !1, this.setErrors(e, { emitEvent: t }) }) } }
                _cancelExistingSubscription() { this._asyncValidationSubscription && (this._asyncValidationSubscription.unsubscribe(), this._hasOwnPendingAsyncValidator = !1) }
                setErrors(t, e = {}) { this.errors = t, this._updateControlsErrors(!1 !== e.emitEvent) }
                get(t) { return function(t, e, i) { if (null == e) return null; if (Array.isArray(e) || (e = e.split(".")), Array.isArray(e) && 0 === e.length) return null; let s = t; return e.forEach(t => { s = s instanceof R ? s.controls.hasOwnProperty(t) ? s.controls[t] : null : s instanceof J && s.at(t) || null }), s }(this, t) }
                getError(t, e) { const i = e ? this.get(e) : this; return i && i.errors ? i.errors[t] : null }
                hasError(t, e) { return !!this.getError(t, e) }
                get root() { let t = this; for (; t._parent;) t = t._parent; return t }
                _updateControlsErrors(t) { this.status = this._calculateStatus(), t && this.statusChanges.emit(this.status), this._parent && this._parent._updateControlsErrors(t) }
                _initObservables() { this.valueChanges = new n.vpe, this.statusChanges = new n.vpe }
                _calculateStatus() { return this._allControlsDisabled() ? "DISABLED" : this.errors ? "INVALID" : this._hasOwnPendingAsyncValidator || this._anyControlsHaveStatus("PENDING") ? "PENDING" : this._anyControlsHaveStatus("INVALID") ? "INVALID" : "VALID" }
                _anyControlsHaveStatus(t) { return this._anyControls(e => e.status === t) }
                _anyControlsDirty() { return this._anyControls(t => t.dirty) }
                _anyControlsTouched() { return this._anyControls(t => t.touched) }
                _updatePristine(t = {}) { this.pristine = !this._anyControlsDirty(), this._parent && !t.onlySelf && this._parent._updatePristine(t) }
                _updateTouched(t = {}) { this.touched = this._anyControlsTouched(), this._parent && !t.onlySelf && this._parent._updateTouched(t) }
                _isBoxedValue(t) { return "object" == typeof t && null !== t && 2 === Object.keys(t).length && "value" in t && "disabled" in t }
                _registerOnCollectionChange(t) { this._onCollectionChange = t }
                _setUpdateStrategy(t) { D(t) && null != t.updateOn && (this._updateOn = t.updateOn) }
                _parentMarkedDirty(t) { return !t && !(!this._parent || !this._parent.dirty) && !this._parent._anyControlsDirty() } }
            class L extends F { constructor(t = null, e, i) { super(M(e), N(i, e)), this._onChange = [], this._applyFormState(t), this._setUpdateStrategy(e), this._initObservables(), this.updateValueAndValidity({ onlySelf: !0, emitEvent: !!this.asyncValidator }) }
                setValue(t, e = {}) { this.value = this._pendingValue = t, this._onChange.length && !1 !== e.emitModelToViewChange && this._onChange.forEach(t => t(this.value, !1 !== e.emitViewToModelChange)), this.updateValueAndValidity(e) }
                patchValue(t, e = {}) { this.setValue(t, e) }
                reset(t = null, e = {}) { this._applyFormState(t), this.markAsPristine(e), this.markAsUntouched(e), this.setValue(this.value, e), this._pendingChange = !1 }
                _updateValue() {}
                _anyControls(t) { return !1 }
                _allControlsDisabled() { return this.disabled }
                registerOnChange(t) { this._onChange.push(t) }
                _unregisterOnChange(t) { q(this._onChange, t) }
                registerOnDisabledChange(t) { this._onDisabledChange.push(t) }
                _unregisterOnDisabledChange(t) { q(this._onDisabledChange, t) }
                _forEachChild(t) {}
                _syncPendingControls() { return !("submit" !== this.updateOn || (this._pendingDirty && this.markAsDirty(), this._pendingTouched && this.markAsTouched(), !this._pendingChange) || (this.setValue(this._pendingValue, { onlySelf: !0, emitModelToViewChange: !1 }), 0)) }
                _applyFormState(t) { this._isBoxedValue(t) ? (this.value = this._pendingValue = t.value, t.disabled ? this.disable({ onlySelf: !0, emitEvent: !1 }) : this.enable({ onlySelf: !0, emitEvent: !1 })) : this.value = this._pendingValue = t } }
            class R extends F { constructor(t, e, i) { super(M(e), N(i, e)), this.controls = t, this._initObservables(), this._setUpdateStrategy(e), this._setUpControls(), this.updateValueAndValidity({ onlySelf: !0, emitEvent: !!this.asyncValidator }) }
                registerControl(t, e) { return this.controls[t] ? this.controls[t] : (this.controls[t] = e, e.setParent(this), e._registerOnCollectionChange(this._onCollectionChange), e) }
                addControl(t, e, i = {}) { this.registerControl(t, e), this.updateValueAndValidity({ emitEvent: i.emitEvent }), this._onCollectionChange() }
                removeControl(t, e = {}) { this.controls[t] && this.controls[t]._registerOnCollectionChange(() => {}), delete this.controls[t], this.updateValueAndValidity({ emitEvent: e.emitEvent }), this._onCollectionChange() }
                setControl(t, e, i = {}) { this.controls[t] && this.controls[t]._registerOnCollectionChange(() => {}), delete this.controls[t], e && this.registerControl(t, e), this.updateValueAndValidity({ emitEvent: i.emitEvent }), this._onCollectionChange() }
                contains(t) { return this.controls.hasOwnProperty(t) && this.controls[t].enabled }
                setValue(t, e = {}) { this._checkAllValuesPresent(t), Object.keys(t).forEach(i => { this._throwIfControlMissing(i), this.controls[i].setValue(t[i], { onlySelf: !0, emitEvent: e.emitEvent }) }), this.updateValueAndValidity(e) }
                patchValue(t, e = {}) { null != t && (Object.keys(t).forEach(i => { this.controls[i] && this.controls[i].patchValue(t[i], { onlySelf: !0, emitEvent: e.emitEvent }) }), this.updateValueAndValidity(e)) }
                reset(t = {}, e = {}) { this._forEachChild((i, s) => { i.reset(t[s], { onlySelf: !0, emitEvent: e.emitEvent }) }), this._updatePristine(e), this._updateTouched(e), this.updateValueAndValidity(e) }
                getRawValue() { return this._reduceChildren({}, (t, e, i) => (t[i] = e instanceof L ? e.value : e.getRawValue(), t)) }
                _syncPendingControls() { let t = this._reduceChildren(!1, (t, e) => !!e._syncPendingControls() || t); return t && this.updateValueAndValidity({ onlySelf: !0 }), t }
                _throwIfControlMissing(t) { if (!Object.keys(this.controls).length) throw new Error("\n        There are no form controls registered with this group yet. If you're using ngModel,\n        you may want to check next tick (e.g. use setTimeout).\n      "); if (!this.controls[t]) throw new Error(`Cannot find form control with name: ${t}.`) }
                _forEachChild(t) { Object.keys(this.controls).forEach(e => { const i = this.controls[e];
                        i && t(i, e) }) }
                _setUpControls() { this._forEachChild(t => { t.setParent(this), t._registerOnCollectionChange(this._onCollectionChange) }) }
                _updateValue() { this.value = this._reduceValue() }
                _anyControls(t) { for (const e of Object.keys(this.controls)) { const i = this.controls[e]; if (this.contains(e) && t(i)) return !0 } return !1 }
                _reduceValue() { return this._reduceChildren({}, (t, e, i) => ((e.enabled || this.disabled) && (t[i] = e.value), t)) }
                _reduceChildren(t, e) { let i = t; return this._forEachChild((t, s) => { i = e(i, t, s) }), i }
                _allControlsDisabled() { for (const t of Object.keys(this.controls))
                        if (this.controls[t].enabled) return !1;
                    return Object.keys(this.controls).length > 0 || this.disabled }
                _checkAllValuesPresent(t) { this._forEachChild((e, i) => { if (void 0 === t[i]) throw new Error(`Must supply a value for form control with name: '${i}'.`) }) } }
            class J extends F { constructor(t, e, i) { super(M(e), N(i, e)), this.controls = t, this._initObservables(), this._setUpdateStrategy(e), this._setUpControls(), this.updateValueAndValidity({ onlySelf: !0, emitEvent: !!this.asyncValidator }) }
                at(t) { return this.controls[t] }
                push(t, e = {}) { this.controls.push(t), this._registerControl(t), this.updateValueAndValidity({ emitEvent: e.emitEvent }), this._onCollectionChange() }
                insert(t, e, i = {}) { this.controls.splice(t, 0, e), this._registerControl(e), this.updateValueAndValidity({ emitEvent: i.emitEvent }) }
                removeAt(t, e = {}) { this.controls[t] && this.controls[t]._registerOnCollectionChange(() => {}), this.controls.splice(t, 1), this.updateValueAndValidity({ emitEvent: e.emitEvent }) }
                setControl(t, e, i = {}) { this.controls[t] && this.controls[t]._registerOnCollectionChange(() => {}), this.controls.splice(t, 1), e && (this.controls.splice(t, 0, e), this._registerControl(e)), this.updateValueAndValidity({ emitEvent: i.emitEvent }), this._onCollectionChange() }
                get length() { return this.controls.length }
                setValue(t, e = {}) { this._checkAllValuesPresent(t), t.forEach((t, i) => { this._throwIfControlMissing(i), this.at(i).setValue(t, { onlySelf: !0, emitEvent: e.emitEvent }) }), this.updateValueAndValidity(e) }
                patchValue(t, e = {}) { null != t && (t.forEach((t, i) => { this.at(i) && this.at(i).patchValue(t, { onlySelf: !0, emitEvent: e.emitEvent }) }), this.updateValueAndValidity(e)) }
                reset(t = [], e = {}) { this._forEachChild((i, s) => { i.reset(t[s], { onlySelf: !0, emitEvent: e.emitEvent }) }), this._updatePristine(e), this._updateTouched(e), this.updateValueAndValidity(e) }
                getRawValue() { return this.controls.map(t => t instanceof L ? t.value : t.getRawValue()) }
                clear(t = {}) { this.controls.length < 1 || (this._forEachChild(t => t._registerOnCollectionChange(() => {})), this.controls.splice(0), this.updateValueAndValidity({ emitEvent: t.emitEvent })) }
                _syncPendingControls() { let t = this.controls.reduce((t, e) => !!e._syncPendingControls() || t, !1); return t && this.updateValueAndValidity({ onlySelf: !0 }), t }
                _throwIfControlMissing(t) { if (!this.controls.length) throw new Error("\n        There are no form controls registered with this array yet. If you're using ngModel,\n        you may want to check next tick (e.g. use setTimeout).\n      "); if (!this.at(t)) throw new Error(`Cannot find form control at index ${t}`) }
                _forEachChild(t) { this.controls.forEach((e, i) => { t(e, i) }) }
                _updateValue() { this.value = this.controls.filter(t => t.enabled || this.disabled).map(t => t.value) }
                _anyControls(t) { return this.controls.some(e => e.enabled && t(e)) }
                _setUpControls() { this._forEachChild(t => this._registerControl(t)) }
                _checkAllValuesPresent(t) { this._forEachChild((e, i) => { if (void 0 === t[i]) throw new Error(`Must supply a value for form control at index: ${i}.`) }) }
                _allControlsDisabled() { for (const t of this.controls)
                        if (t.enabled) return !1;
                    return this.controls.length > 0 || this.disabled }
                _registerControl(t) { t.setParent(this), t._registerOnCollectionChange(this._onCollectionChange) } } const B = { provide: P, useExisting: (0, n.Gpc)(() => W) },
                j = (() => Promise.resolve(null))(); let W = (() => { class t extends P { constructor(t, e, i, s) { super(), this.control = new L, this._registered = !1, this.update = new n.vpe, this._parent = t, this._setValidators(e), this._setAsyncValidators(i), this.valueAccessor = function(t, e) { if (!e) return null; let i, s, n; return Array.isArray(e), e.forEach(t => { t.constructor === f ? i = t : Object.getPrototypeOf(t.constructor) === u ? s = t : n = t }), n || s || i || null }(0, s) }
                        ngOnChanges(t) { this._checkForErrors(), this._registered || this._setUpControl(), "isDisabled" in t && this._updateDisabled(t),
                                function(t, e) { if (!t.hasOwnProperty("model")) return !1; const i = t.model; return !!i.isFirstChange() || !Object.is(e, i.currentValue) }(t, this.viewModel) && (this._updateValue(this.model), this.viewModel = this.model) }
                        ngOnDestroy() { this.formDirective && this.formDirective.removeControl(this) }
                        get path() { return this._parent ? [...this._parent.path, this.name] : [this.name] }
                        get formDirective() { return this._parent ? this._parent.formDirective : null }
                        viewToModelUpdate(t) { this.viewModel = t, this.update.emit(t) }
                        _setUpControl() { this._setUpdateStrategy(), this._isStandalone() ? this._setUpStandalone() : this.formDirective.addControl(this), this._registered = !0 }
                        _setUpdateStrategy() { this.options && null != this.options.updateOn && (this.control._updateOn = this.options.updateOn) }
                        _isStandalone() { return !this._parent || !(!this.options || !this.options.standalone) }
                        _setUpStandalone() { var t, e;
                            (function(t, e) { const i = function(t) { return t._rawValidators }(t);
                                null !== e.validator ? t.setValidators(S(i, e.validator)) : "function" == typeof i && t.setValidators([i]); const s = function(t) { return t._rawAsyncValidators }(t);
                                null !== e.asyncValidator ? t.setAsyncValidators(S(s, e.asyncValidator)) : "function" == typeof s && t.setAsyncValidators([s]); const n = () => t.updateValueAndValidity();
                                I(e._rawValidators, n), I(e._rawAsyncValidators, n) })(t = this.control, e = this), e.valueAccessor.writeValue(t.value),
                                function(t, e) { e.valueAccessor.registerOnChange(i => { t._pendingValue = i, t._pendingChange = !0, t._pendingDirty = !0, "change" === t.updateOn && k(t, e) }) }(t, e),
                                function(t, e) { const i = (t, i) => { e.valueAccessor.writeValue(t), i && e.viewToModelUpdate(t) };
                                    t.registerOnChange(i), e._registerOnDestroy(() => { t._unregisterOnChange(i) }) }(t, e),
                                function(t, e) { e.valueAccessor.registerOnTouched(() => { t._pendingTouched = !0, "blur" === t.updateOn && t._pendingChange && k(t, e), "submit" !== t.updateOn && t.markAsTouched() }) }(t, e),
                                function(t, e) { if (e.valueAccessor.setDisabledState) { const i = t => { e.valueAccessor.setDisabledState(t) };
                                        t.registerOnDisabledChange(i), e._registerOnDestroy(() => { t._unregisterOnDisabledChange(i) }) } }(t, e), this.control.updateValueAndValidity({ emitEvent: !1 }) }
                        _checkForErrors() { this._isStandalone() || this._checkParentType(), this._checkName() }
                        _checkParentType() {}
                        _checkName() { this.options && this.options.name && (this.name = this.options.name), this._isStandalone() }
                        _updateValue(t) { j.then(() => { this.control.setValue(t, { emitViewToModelChange: !1 }) }) }
                        _updateDisabled(t) { const e = t.isDisabled.currentValue,
                                i = "" === e || e && "false" !== e;
                            j.then(() => { i && !this.control.disabled ? this.control.disable() : !i && this.control.disabled && this.control.enable() }) } } return t.\u0275fac = function(e) { return new(e || t)(n.Y36(w, 9), n.Y36(_, 10), n.Y36(v, 10), n.Y36(g, 10)) }, t.\u0275dir = n.lG2({ type: t, selectors: [
                            ["", "ngModel", "", 3, "formControlName", "", 3, "formControl", ""]
                        ], inputs: { name: "name", isDisabled: ["disabled", "isDisabled"], model: ["ngModel", "model"], options: ["ngModelOptions", "options"] }, outputs: { update: "ngModelChange" }, exportAs: ["ngModel"], features: [n._Bn([B]), n.qOj, n.TTD] }), t })(),
                Q = (() => { class t {} return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275mod = n.oAB({ type: t }), t.\u0275inj = n.cJS({}), t })(),
                Y = (() => { class t {} return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275mod = n.oAB({ type: t }), t.\u0275inj = n.cJS({ imports: [
                            [Q]
                        ] }), t })(),
                G = (() => { class t {} return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275mod = n.oAB({ type: t }), t.\u0275inj = n.cJS({ imports: [Y] }), t })(); var H = i(7957),
                z = i(5384),
                $ = i(3484),
                X = i(2340),
                K = i(1841),
                tt = i(3342),
                et = i(5304),
                it = i(205); let st = (() => { class t { constructor(t) { this.http = t }
                        makeGetRequest(t, e = !0) { return this.http.get(t).pipe((0, tt.b)(t => {}), (0, et.K)(t => (0, it._)(t))) }
                        makePostRequest(t, e) { return this.http.post(t, e).pipe((0, tt.b)(t => {}), (0, et.K)(t => (0, it._)(t))) } } return t.\u0275fac = function(e) { return new(e || t)(n.LFG(K.eN)) }, t.\u0275prov = n.Yz7({ token: t, factory: t.\u0275fac, providedIn: "root" }), t })(),
                nt = (() => { class t { constructor(t, e) { this.http = t, this.rest = e }
                        generateOTP(t) { return this.rest.makePostRequest(`${X.N.apiUrl}/signin/OtpSignIn`, { CountryPhoneCode: "91", PhoneNumber: t, Source: "Otp" }) }
                        signIn(t, e) { return this.rest.makePostRequest(`${X.N.apiUrl}/signin`, { PhoneNumber: t, OTP: e, Source: "Otp", DeviceType: "iPhone" }) } } return t.\u0275fac = function(e) { return new(e || t)(n.LFG(K.eN), n.LFG(st)) }, t.\u0275prov = n.Yz7({ token: t, factory: t.\u0275fac, providedIn: "root" }), t })();

            function ot(t, e) { if (1 & t) { const t = n.EpF();
                    n.TgZ(0, "div", 3), n.TgZ(1, "h6"), n._uU(2, "Enter Mobile Number "), n.TgZ(3, "span"), n._uU(4, "*"), n.qZA(), n.qZA(), n.TgZ(5, "div", 4), n.TgZ(6, "div", 5), n._UZ(7, "input", 6), n.qZA(), n.TgZ(8, "div", 7), n.TgZ(9, "input", 8), n.NdJ("ngModelChange", function(e) { return n.CHM(t), n.oxw().mobileNo = e }), n.qZA(), n.qZA(), n.qZA(), n.TgZ(10, "div", 9), n.TgZ(11, "button", 10), n.NdJ("click", function() { return n.CHM(t), n.oxw().generateOTP() }), n._uU(12, "Send OTP"), n.qZA(), n.TgZ(13, "button", 11), n._uU(14, "Sign Up"), n.qZA(), n.qZA(), n.qZA() } if (2 & t) { const t = n.oxw();
                    n.xp6(9), n.Q6J("ngModel", t.mobileNo) } }

            function rt(t, e) { if (1 & t) { const t = n.EpF();
                    n.TgZ(0, "div", 12), n.TgZ(1, "h6"), n._uU(2, "Enter OTP"), n.qZA(), n.TgZ(3, "div", 13), n.TgZ(4, "div", 14), n._UZ(5, "i", 15), n.TgZ(6, "input", 16), n.NdJ("ngModelChange", function(e) { return n.CHM(t), n.oxw().otp = e }), n.qZA(), n.qZA(), n.qZA(), n.TgZ(7, "div", 9), n.TgZ(8, "button", 10), n.NdJ("click", function() { return n.CHM(t), n.oxw().signIn() }), n._uU(9, "Login"), n.qZA(), n.TgZ(10, "button", 11), n._uU(11, "Sign Up"), n.qZA(), n.qZA(), n.qZA() } if (2 & t) { const t = n.oxw();
                    n.xp6(6), n.Q6J("ngModel", t.otp) } } let at = (() => { class t { constructor(t, e) { this.authService = t, this._snackBar = e, this.closeMenuEvent = new n.vpe, this.formType = "login", this.mobileNo = "", this.otp = "" }
                    ngOnInit() {}
                    clearAll(t) { this.formType = "login", this.mobileNo = "", this.otp = "", t && this.closeMenuEvent.emit("closeMenu") }
                    generateOTP() { this.mobileNo ? this.authService.generateOTP(this.mobileNo).subscribe(t => { var e;
                            this.formType = "otp", this._snackBar.open("Your OTP: " + (null === (e = null == t ? void 0 : t.Data) || void 0 === e ? void 0 : e.Token), "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }) }, t => { this._snackBar.open("Sorry, Something went wrong. Please try again after some time!", "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }), this.clearAll(!0) }) : this._snackBar.open("Mobile number should not be empty!", "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }) }
                    signIn() { this.otp ? this.authService.signIn(this.mobileNo, this.otp).subscribe(t => { "200" == t.ResponseCode ? (localStorage.setItem("userProfile", JSON.stringify(t)), this._snackBar.open("Login Successful.", "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 })) : this._snackBar.open(t.Message, "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }), this.clearAll(!0) }, t => { this._snackBar.open("Sorry, Something went wrong. Please try again after some time!", "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }), this.clearAll(!0) }) : this._snackBar.open("OTP should not be empty!", "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }) }
                    ngOnDestroy() { this.clearAll(!0) } } return t.\u0275fac = function(e) { return new(e || t)(n.Y36(nt), n.Y36($.ux)) }, t.\u0275cmp = n.Xpm({ type: t, selectors: [
                        ["app-login"]
                    ], inputs: { formType: "formType" }, outputs: { closeMenuEvent: "closeMenuEvent" }, decls: 3, vars: 2, consts: [
                        [1, "dialog-container", 3, "click", "keydown"],
                        ["class", "login-section", 4, "ngIf"],
                        ["class", "otp-section", 4, "ngIf"],
                        [1, "login-section"],
                        [1, "login-form"],
                        [1, "otp-sec"],
                        ["type", "text", "value", "+91 (IN)", "readonly", "", 1, "line-input-field"],
                        [1, "mobile-sec"],
                        ["type", "text", "placeholder", "Ex. 9685745263", 1, "line-input-field", 3, "ngModel", "ngModelChange"],
                        [1, "buttons"],
                        [1, "btn-normal", 3, "click"],
                        [1, "btn-default"],
                        [1, "otp-section"],
                        [1, "otp-form"],
                        [1, "input-group"],
                        [1, "fa", "fa-pen"],
                        ["type", "text", 1, "line-input-field", 3, "ngModel", "ngModelChange"]
                    ], template: function(t, e) { 1 & t && (n.TgZ(0, "div", 0), n.NdJ("click", function(t) { return t.stopPropagation() })("keydown", function(t) { return t.stopPropagation() }), n.YNc(1, ot, 15, 1, "div", 1), n.YNc(2, rt, 12, 1, "div", 2), n.qZA()), 2 & t && (n.xp6(1), n.Q6J("ngIf", "login" == e.formType), n.xp6(1), n.Q6J("ngIf", "otp" == e.formType)) }, directives: [s.O5, f, V, W], styles: [".mat-menu-content{padding:0!important} .mat-menu-panel{max-width:none!important}.dialog-container[_ngcontent-%COMP%]{display:flex;flex-direction:column;justify-content:center}.dialog-container[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{font-size:medium;font-weight:500}.dialog-container[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{color:#eb5d72}.dialog-container[_ngcontent-%COMP%]   .login-form[_ngcontent-%COMP%]{margin-top:30px;width:100%;display:flex;align-items:center}.dialog-container[_ngcontent-%COMP%]   .login-form[_ngcontent-%COMP%]   .otp-sec[_ngcontent-%COMP%]{width:25%;margin-right:10px}.dialog-container[_ngcontent-%COMP%]   .login-form[_ngcontent-%COMP%]   .mobile-sec[_ngcontent-%COMP%]{width:70%;margin-left:10px}.dialog-container[_ngcontent-%COMP%]   .buttons[_ngcontent-%COMP%]{margin-top:40px;width:100%;display:flex;align-items:center;justify-content:space-around}"] }), t })(); const lt = ["menuTrigger"],
                ct = ["loginPage"];

            function ht(t, e) { 1 & t && (n.TgZ(0, "li", 10), n.TgZ(1, "a", 11), n._uU(2, "My Prediction"), n.qZA(), n.qZA()) }

            function dt(t, e) { 1 & t && (n.TgZ(0, "li", 10), n.TgZ(1, "a", 11), n._uU(2, "Results"), n.qZA(), n.qZA()) }

            function ut(t, e) { 1 & t && (n.TgZ(0, "li", 10), n.TgZ(1, "a", 11), n._uU(2, "Winner's Name"), n.qZA(), n.qZA()) }

            function gt(t, e) { 1 & t && (n.TgZ(0, "li", 10), n.TgZ(1, "a", 11), n._uU(2, "Our Prices"), n.qZA(), n.qZA()) }

            function pt(t, e) { if (1 & t && (n.TgZ(0, "li", 20), n._UZ(1, "img", 21), n.qZA()), 2 & t) { n.oxw(); const t = n.MAs(18);
                    n.Q6J("matMenuTriggerFor", t) } }

            function mt(t, e) { if (1 & t) { const t = n.EpF();
                    n.TgZ(0, "li", 22, 23), n.NdJ("click", function() { return n.CHM(t), n.oxw().openLoginDialog() }), n.TgZ(2, "a", 24), n._uU(3, "Login"), n.qZA(), n.qZA() } if (2 & t) { n.oxw(); const t = n.MAs(25);
                    n.Q6J("matMenuTriggerFor", t) } } let ft = (() => { class t { constructor() { this.isCollapsed = !0, this.isLoggedIn = !1 }
                        ngOnInit() { this.checkLoggedInOrNot() }
                        closeLoginDialog() { this.trigger.closeMenu(), this.checkLoggedInOrNot() }
                        openLoginDialog() { this.trigger.openMenu(), this.loginPage.clearAll(!1) }
                        checkLoggedInOrNot() { this.isLoggedIn = !!localStorage.getItem("userProfile") }
                        logout() { localStorage.setItem("userProfile", ""), this.checkLoggedInOrNot() } } return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275cmp = n.Xpm({ type: t, selectors: [
                            ["app-header"]
                        ], viewQuery: function(t, e) { if (1 & t && (n.Gf(lt, 5), n.Gf(ct, 5)), 2 & t) { let t;
                                n.iGM(t = n.CRH()) && (e.trigger = t.first), n.iGM(t = n.CRH()) && (e.loginPage = t.first) } }, decls: 28, vars: 8, consts: [
                            [2, "position", "sticky", "top", "0", "left", "0", "z-index", "999"],
                            [1, "navbar", "navbar-expand-lg", "navbar-dark", "bg-primary", "mb-3"],
                            [1, "container-fluid"],
                            ["routerLink", "/", 1, "navbar-brand"],
                            ["src", "assets/images/Goalak-logo-w.png", "alt", "Goalak"],
                            ["type", "button", "data-bs-toggle", "collapse", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler", 3, "click"],
                            [1, "navbar-toggler-icon"],
                            [1, "collapse", "navbar-collapse"],
                            [1, "navbar-nav", "ms-auto", "mb-2", "mb-lg-0"],
                            ["class", "nav-item", 4, "ngIf"],
                            [1, "nav-item"],
                            ["href", "#", 1, "nav-link"],
                            ["class", "nav-item", 3, "matMenuTriggerFor", 4, "ngIf"],
                            ["profileMenu", "matMenu"],
                            ["mat-menu-item", ""],
                            ["mat-menu-item", "", 3, "click"],
                            ["class", "nav-item", 3, "matMenuTriggerFor", "click", 4, "ngIf"],
                            ["menu", "matMenu"],
                            [3, "formType", "closeMenuEvent"],
                            ["loginPage", ""],
                            [1, "nav-item", 3, "matMenuTriggerFor"],
                            ["src", "assets/images/profile.png", "alt", "", 1, "profile"],
                            [1, "nav-item", 3, "matMenuTriggerFor", "click"],
                            ["menuTrigger", "matMenuTrigger"],
                            [1, "nav-link"]
                        ], template: function(t, e) { 1 & t && (n.TgZ(0, "header", 0), n.TgZ(1, "nav", 1), n.TgZ(2, "div", 2), n.TgZ(3, "a", 3), n._UZ(4, "img", 4), n.qZA(), n.TgZ(5, "button", 5), n.NdJ("click", function() { return e.isCollapsed = !e.isCollapsed }), n._UZ(6, "span", 6), n.qZA(), n.TgZ(7, "div", 7), n.TgZ(8, "ul", 8), n.YNc(9, ht, 3, 0, "li", 9), n.YNc(10, dt, 3, 0, "li", 9), n.YNc(11, ut, 3, 0, "li", 9), n.YNc(12, gt, 3, 0, "li", 9), n.TgZ(13, "li", 10), n.TgZ(14, "a", 11), n._uU(15, "EN"), n.qZA(), n.qZA(), n.YNc(16, pt, 2, 1, "li", 12), n.TgZ(17, "mat-menu", null, 13), n.TgZ(19, "button", 14), n._uU(20, "My Account"), n.qZA(), n.TgZ(21, "button", 15), n.NdJ("click", function() { return e.logout() }), n._uU(22, "Log Out"), n.qZA(), n.qZA(), n.YNc(23, mt, 4, 1, "li", 16), n.TgZ(24, "mat-menu", null, 17), n.TgZ(26, "app-login", 18, 19), n.NdJ("closeMenuEvent", function() { return e.closeLoginDialog() }), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.qZA()), 2 & t && (n.xp6(5), n.uIk("aria-expanded", !e.isCollapsed), n.xp6(4), n.Q6J("ngIf", e.isLoggedIn), n.xp6(1), n.Q6J("ngIf", e.isLoggedIn), n.xp6(1), n.Q6J("ngIf", e.isLoggedIn), n.xp6(1), n.Q6J("ngIf", e.isLoggedIn), n.xp6(4), n.Q6J("ngIf", e.isLoggedIn), n.xp6(7), n.Q6J("ngIf", !e.isLoggedIn), n.xp6(3), n.Q6J("formType", "login")) }, directives: [H.yS, s.O5, z.VK, z.OP, at, z.p6], styles: [".nav-item[_ngcontent-%COMP%]{padding:3px 10px;cursor:pointer}.nav-item[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover{background-color:#ffd710;font-weight:500;color:#000!important;border-radius:2px}.nav-item[_ngcontent-%COMP%]   .profile[_ngcontent-%COMP%]{width:40px;-o-object-fit:cover;object-fit:cover}"] }), t })(),
                _t = (() => { class t { constructor() {}
                        ngOnInit() {} } return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275cmp = n.Xpm({ type: t, selectors: [
                            ["app-footer"]
                        ], decls: 65, vars: 0, consts: [
                            [1, "space-top-80"],
                            [1, "container-fluid", "bottom"],
                            [1, "container", "pt-5", "pb-5"],
                            [1, "row"],
                            [1, "col-lg-3"],
                            ["href", "#", "target", "_self"],
                            [1, "social", "mb-4"],
                            ["href", "#"],
                            ["src", "assets/images/whatsapp.png", "alt", "goalak", 1, "mr-2"],
                            ["src", "assets/images/facebook.png", "alt", "goalak", 1, "mr-2"],
                            ["src", "assets/images/insta.png", "alt", "goalak", 1, "mr-2"],
                            ["src", "assets/images/twitter.png", "alt", "goalak", 1, "mr-2"],
                            ["src", "assets/images/app.png", "alt", "goalak", 1, "app"],
                            [1, "footer", "text-center"]
                        ], template: function(t, e) { 1 & t && (n.TgZ(0, "footer", 0), n.TgZ(1, "div", 1), n.TgZ(2, "div", 2), n.TgZ(3, "div", 3), n.TgZ(4, "div", 4), n.TgZ(5, "h3"), n._uU(6, "COMPANY"), n.qZA(), n.TgZ(7, "ul"), n.TgZ(8, "li"), n.TgZ(9, "a", 5), n._uU(10, "About Us"), n.qZA(), n.qZA(), n.TgZ(11, "li"), n.TgZ(12, "a", 5), n._uU(13, "Blog"), n.qZA(), n.qZA(), n.TgZ(14, "li"), n.TgZ(15, "a", 5), n._uU(16, "Feedback"), n.qZA(), n.qZA(), n.TgZ(17, "li"), n.TgZ(18, "a", 5), n._uU(19, "Sitemap"), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.TgZ(20, "div", 4), n.TgZ(21, "h3"), n._uU(22, "ACCOUNT"), n.qZA(), n.TgZ(23, "ul"), n.TgZ(24, "li"), n.TgZ(25, "a", 5), n._uU(26, "Login"), n.qZA(), n.qZA(), n.TgZ(27, "li"), n.TgZ(28, "a", 5), n._uU(29, "Register"), n.qZA(), n.qZA(), n.TgZ(30, "li"), n.TgZ(31, "a", 5), n._uU(32, "Create Listing"), n.qZA(), n.qZA(), n.TgZ(33, "li"), n.TgZ(34, "a", 5), n._uU(35, "How It Works"), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.TgZ(36, "div", 4), n.TgZ(37, "h3"), n._uU(38, "HELP & CONTACT"), n.qZA(), n.TgZ(39, "ul"), n.TgZ(40, "li"), n.TgZ(41, "a", 5), n._uU(42, "Contact Us"), n.qZA(), n.qZA(), n.TgZ(43, "li"), n.TgZ(44, "a", 5), n._uU(45, "Frequently Asked Questions"), n.qZA(), n.qZA(), n.TgZ(46, "li"), n.TgZ(47, "a", 5), n._uU(48, "Terms & Conditions"), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.TgZ(49, "div", 4), n.TgZ(50, "h3"), n._uU(51, "FOLLOW US"), n.qZA(), n.TgZ(52, "div", 6), n.TgZ(53, "a", 7), n._UZ(54, "img", 8), n.qZA(), n.TgZ(55, "a", 7), n._UZ(56, "img", 9), n.qZA(), n.TgZ(57, "a", 7), n._UZ(58, "img", 10), n.qZA(), n.TgZ(59, "a", 7), n._UZ(60, "img", 11), n.qZA(), n.qZA(), n._UZ(61, "img", 12), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.TgZ(62, "div", 13), n.TgZ(63, "p"), n._uU(64, "Copyright\xa9 2022 Goalak. All rights reserved."), n.qZA(), n.qZA(), n.qZA()) }, styles: [""] }), t })(),
                vt = (() => { class t { constructor() {}
                        ngOnInit() {} } return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275cmp = n.Xpm({ type: t, selectors: [
                            ["app-partials"]
                        ], decls: 3, vars: 0, template: function(t, e) { 1 & t && (n._UZ(0, "app-header"), n._UZ(1, "router-outlet"), n._UZ(2, "app-footer")) }, directives: [ft, H.lC, _t], styles: [""] }), t })(),
                bt = (() => { class t { constructor(t, e) { this.http = t, this.rest = e }
                        getAllNews() { return this.rest.makeGetRequest(`${X.N.apiUrl}/football/sports/getNews`) } } return t.\u0275fac = function(e) { return new(e || t)(n.LFG(K.eN), n.LFG(st)) }, t.\u0275prov = n.Yz7({ token: t, factory: t.\u0275fac, providedIn: "root" }), t })(),
                yt = (() => { class t { constructor(t, e) { this.http = t, this.rest = e }
                        getAllSeries() { return this.rest.makePostRequest(`${X.N.apiUrl}/football/sports/getSeries`, { Params: "Status,TotalMatches,SeriesFlag,SeriesID,hasStandings,seriesGUID", StatusID: [1, 2], OrderBy: "Status" }) }
                        getRoundsBySeries(t) { return this.rest.makePostRequest(`${X.N.apiUrl}/football/series/getRounds`, { SeriesGUID: t, Params: "TotalLevels,RoundID,StatusID,SeriesType,RoundFormat,SeriesIDLive,SeriesStartDate,SeriesEndDate,SeriesStartDateUTC,SeriesEndDateUTC,TotalMatches,SeriesMatchStartDate,Status", SessionKey: "08925584-3927-4517-30cf-c4a910cc4e4c" }) }
                        getMatchesByRounds(t, e) { return this.rest.makePostRequest(`${X.N.apiUrl}/football/sports/getMatches`, { Params: "isPredicted,TeamNameShortVisitor,TeamNameShortLocal,Status,TeamFlagLocal,TeamFlagVisitor,MatchStartDateTimeUTC,WinPoints,PredictionData,Statistics,MatchScoreDetails,SeriesName,SeriesID,StatusID,SeriesGUID", SeriesGUID: t, SessionKey: "08925584-3927-4517-30cf-c4a910cc4e4c", RoundID: e, Status: "Pending" }) } } return t.\u0275fac = function(e) { return new(e || t)(n.LFG(K.eN), n.LFG(st)) }, t.\u0275prov = n.Yz7({ token: t, factory: t.\u0275fac, providedIn: "root" }), t })(); const Zt = "undefined" != typeof window && window || {}; let At;

            function Ct() { if (void 0 === Zt.document) return "bs4"; const t = Zt.document.createElement("span");
                t.innerText = "testing bs version", t.classList.add("d-none"), t.classList.add("pl-1"), Zt.document.head.appendChild(t); const e = t.getBoundingClientRect(),
                    i = Zt.getComputedStyle(t).paddingLeft; return !e || e && 0 !== e.top ? (Zt.document.head.removeChild(t), "bs3") : i && parseFloat(i) ? (Zt.document.head.removeChild(t), "bs4") : (Zt.document.head.removeChild(t), "bs5") }

            function xt() { return void 0 === Zt || (void 0 === Zt.__theme ? (At || (At = Ct()), "bs3" === At) : "bs3" === Zt.__theme) }

            function Tt() { return !xt() && (At || (At = Ct()), "bs4" === At) }
            class St { constructor() { this.length = 0, this.asArray = [] }
                get(t) { if (0 === this.length || t < 0 || t >= this.length) return; let e = this.head; for (let i = 0; i < t; i++) e = e ? .next; return e ? .value }
                add(t, e = this.length) { if (e < 0 || e > this.length) throw new Error("Position is out of the list"); const i = { value: t, next: void 0, previous: void 0 }; if (0 === this.length) this.head = i, this.tail = i, this.current = i;
                    else if (0 === e && this.head) i.next = this.head, this.head.previous = i, this.head = i;
                    else if (e === this.length && this.tail) this.tail.next = i, i.previous = this.tail, this.tail = i;
                    else { const t = this.getNode(e - 1),
                            s = t ? .next;
                        t && s && (t.next = i, s.previous = i, i.previous = t, i.next = s) }
                    this.length++, this.createInternalArrayRepresentation() }
                remove(t = 0) { if (0 === this.length || t < 0 || t >= this.length) throw new Error("Position is out of the list"); if (0 === t && this.head) this.head = this.head.next, this.head ? this.head.previous = void 0 : this.tail = void 0;
                    else if (t === this.length - 1 && this.tail ? .previous) this.tail = this.tail.previous, this.tail.next = void 0;
                    else { const e = this.getNode(t);
                        e ? .next && e.previous && (e.next.previous = e.previous, e.previous.next = e.next) }
                    this.length--, this.createInternalArrayRepresentation() }
                set(t, e) { if (0 === this.length || t < 0 || t >= this.length) throw new Error("Position is out of the list"); const i = this.getNode(t);
                    i && (i.value = e, this.createInternalArrayRepresentation()) }
                toArray() { return this.asArray }
                findAll(t) { let e = this.head; const i = []; if (!e) return i; for (let s = 0; s < this.length; s++) { if (!e) return i;
                        t(e.value, s) && i.push({ index: s, value: e.value }), e = e.next } return i }
                push(...t) { return t.forEach(t => { this.add(t) }), this.length }
                pop() { if (0 === this.length) return; const t = this.tail; return this.remove(this.length - 1), t ? .value }
                unshift(...t) { return t.reverse(), t.forEach(t => { this.add(t, 0) }), this.length }
                shift() { if (0 === this.length) return; const t = this.head ? .value; return this.remove(), t }
                forEach(t) { let e = this.head; for (let i = 0; i < this.length; i++) { if (!e) return;
                        t(e.value, i), e = e.next } }
                indexOf(t) { let e = this.head,
                        i = -1; for (let s = 0; s < this.length; s++) { if (!e) return i; if (e.value === t) { i = s; break }
                        e = e.next } return i }
                some(t) { let e = this.head,
                        i = !1; for (; e && !i;) { if (t(e.value)) { i = !0; break }
                        e = e.next } return i }
                every(t) { let e = this.head,
                        i = !0; for (; e && i;) t(e.value) || (i = !1), e = e.next; return i }
                toString() { return "[Linked List]" }
                find(t) { let e = this.head; for (let i = 0; i < this.length; i++) { if (!e) return; if (t(e.value, i)) return e.value;
                        e = e.next } }
                findIndex(t) { let e = this.head; for (let i = 0; i < this.length; i++) { if (!e) return -1; if (t(e.value, i)) return i;
                        e = e.next } return -1 }
                getNode(t) { if (0 === this.length || t < 0 || t >= this.length) throw new Error("Position is out of the list"); let e = this.head; for (let i = 0; i < t; i++) e = e ? .next; return e }
                createInternalArrayRepresentation() { const t = []; let e = this.head; for (; e;) t.push(e.value), e = e.next;
                    this.asArray = t } }

            function Ot(t, e) { if (1 & t) { const t = n.EpF();
                    n.TgZ(0, "li", 7), n.NdJ("click", function() { const e = n.CHM(t).index; return n.oxw(2).selectSlide(e) }), n.qZA() }
                2 & t && n.ekj("active", !0 === e.$implicit.active) }

            function wt(t, e) { if (1 & t && (n.ynx(0), n.TgZ(1, "ol", 5), n.YNc(2, Ot, 1, 2, "li", 6), n.qZA(), n.BQk()), 2 & t) { const t = n.oxw();
                    n.xp6(2), n.Q6J("ngForOf", t.indicatorsSlides()) } }

            function Pt(t, e) { if (1 & t) { const t = n.EpF();
                    n.TgZ(0, "button", 9), n.NdJ("click", function() { const e = n.CHM(t).index; return n.oxw(2).selectSlide(e) }), n.qZA() } if (2 & t) { const t = e.$implicit,
                        i = e.index,
                        s = n.oxw(2);
                    n.ekj("active", !0 === t.active), n.uIk("data-bs-target", "#" + s.currentId)("data-bs-slide-to", i) } }

            function Vt(t, e) { if (1 & t && (n.ynx(0), n.TgZ(1, "div", 5), n.YNc(2, Pt, 1, 4, "button", 8), n.qZA(), n.BQk()), 2 & t) { const t = n.oxw();
                    n.xp6(2), n.Q6J("ngForOf", t.indicatorsSlides()) } }

            function It(t, e) { 1 & t && (n.TgZ(0, "span", 13), n._uU(1, "Previous"), n.qZA()) }

            function kt(t, e) { if (1 & t) { const t = n.EpF();
                    n.TgZ(0, "a", 10), n.NdJ("click", function() { return n.CHM(t), n.oxw().previousSlide() }), n._UZ(1, "span", 11), n.YNc(2, It, 2, 0, "span", 12), n.qZA() } if (2 & t) { const t = n.oxw();
                    n.ekj("disabled", t.checkDisabledClass("prev")), n.uIk("data-bs-target", "#" + t.currentId), n.xp6(2), n.Q6J("ngIf", t.isBs4) } }

            function qt(t, e) { if (1 & t) { const t = n.EpF();
                    n.TgZ(0, "a", 14), n.NdJ("click", function() { return n.CHM(t), n.oxw().nextSlide() }), n._UZ(1, "span", 15), n.TgZ(2, "span", 13), n._uU(3, "Next"), n.qZA(), n.qZA() } if (2 & t) { const t = n.oxw();
                    n.ekj("disabled", t.checkDisabledClass("next")), n.uIk("data-bs-target", "#" + t.currentId) } } "undefined" == typeof console || console; const Mt = function(t) { return { display: t } },
                Et = ["*"]; let Nt = (() => { class t { constructor() { this.interval = 5e3, this.noPause = !1, this.noWrap = !1, this.showIndicators = !0, this.pauseOnFocus = !1, this.indicatorsByChunk = !1, this.itemsPerSlide = 1, this.singleSlideOffset = !1 } } return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275prov = n.Yz7({ token: t, factory: t.\u0275fac, providedIn: "root" }), t })(); var Ut = (() => (function(t) { t[t.UNKNOWN = 0] = "UNKNOWN", t[t.NEXT = 1] = "NEXT", t[t.PREV = 2] = "PREV" }(Ut || (Ut = {})), Ut))(); let Dt = 1,
                Ft = (() => { class t { constructor(t, e) { this.ngZone = e, this.noWrap = !1, this.noPause = !1, this.showIndicators = !0, this.pauseOnFocus = !1, this.indicatorsByChunk = !1, this.itemsPerSlide = 1, this.singleSlideOffset = !1, this.isAnimated = !1, this.activeSlideChange = new n.vpe(!1), this.slideRangeChange = new n.vpe, this.startFromIndex = 0, this._interval = 5e3, this._slides = new St, this._currentVisibleSlidesIndex = 0, this.isPlaying = !1, this.destroyed = !1, this.currentId = 0, this.getActive = t => t.active, this.makeSlidesConsistent = t => { t.forEach((t, e) => t.item.order = e) }, Object.assign(this, t), this.currentId = Dt++ }
                        set activeSlide(t) { var e;
                            this.multilist || (("number" == typeof(e = t) || "[object Number]" === Object.prototype.toString.call(e)) && (this.customActiveSlide = t), this._slides.length && t !== this._currentActiveSlide && this._select(t)) }
                        get activeSlide() { return this._currentActiveSlide || 0 }
                        get interval() { return this._interval }
                        set interval(t) { this._interval = t, this.restartTimer() }
                        get slides() { return this._slides.toArray() }
                        get isFirstSlideVisible() { const t = this.getVisibleIndexes(); return !(!t || t instanceof Array && !t.length) && t.includes(0) }
                        get isLastSlideVisible() { const t = this.getVisibleIndexes(); return !(!t || t instanceof Array && !t.length) && t.includes(this._slides.length - 1) }
                        get isBs4() { return !xt() }
                        get _bsVer() { return { isBs3: xt(), isBs4: Tt(), isBs5: !xt() && !Tt() && (At || (At = Ct()), "bs5" === At) } }
                        ngAfterViewInit() { setTimeout(() => { this.singleSlideOffset && (this.indicatorsByChunk = !1), this.multilist && (this._chunkedSlides = function(t, e) { const i = [],
                                        s = Math.ceil(t.length / e); let n = 0; for (; n < s;) { const o = t.splice(0, n === s - 1 && e < t.length ? t.length : e);
                                        i.push(o), n++ } return i }(this.mapSlidesAndIndexes(), this.itemsPerSlide), this.selectInitialSlides()), this.customActiveSlide && !this.multilist && this._select(this.customActiveSlide) }, 0) }
                        ngOnDestroy() { this.destroyed = !0 }
                        addSlide(t) { this._slides.add(t), this.multilist && this._slides.length <= this.itemsPerSlide && (t.active = !0), !this.multilist && this.isAnimated && (t.isAnimated = !0), this.multilist || 1 !== this._slides.length || (this._currentActiveSlide = void 0, this.customActiveSlide || (this.activeSlide = 0), this.play()), this.multilist && this._slides.length > this.itemsPerSlide && this.play() }
                        removeSlide(t) { const e = this._slides.indexOf(t); if (this._currentActiveSlide === e) { let t;
                                this._slides.length > 1 && (t = this.isLast(e) ? this.noWrap ? e - 1 : 0 : e), this._slides.remove(e), setTimeout(() => { this._select(t) }, 0) } else { this._slides.remove(e); const t = this.getCurrentSlideIndex();
                                setTimeout(() => { this._currentActiveSlide = t, this.activeSlideChange.emit(this._currentActiveSlide) }, 0) } }
                        nextSlideFromInterval(t = !1) { this.move(Ut.NEXT, t) }
                        nextSlide(t = !1) { this.isPlaying && this.restartTimer(), this.move(Ut.NEXT, t) }
                        previousSlide(t = !1) { this.isPlaying && this.restartTimer(), this.move(Ut.PREV, t) }
                        getFirstVisibleIndex() { return this.slides.findIndex(this.getActive) }
                        getLastVisibleIndex() { return function(t, e) { let i = t.length; for (; i--;)
                                    if (e(t[i], i, t)) return i;
                                return -1 }(this.slides, this.getActive) }
                        move(t, e = !1) { const i = this.getFirstVisibleIndex(),
                                s = this.getLastVisibleIndex();
                            this.noWrap && (t === Ut.NEXT && this.isLast(s) || t === Ut.PREV && 0 === i) || (this.multilist ? this.moveMultilist(t) : this.activeSlide = this.findNextSlideIndex(t, e) || 0) }
                        keydownPress(t) { if (13 === t.keyCode || "Enter" === t.key || 32 === t.keyCode || "Space" === t.key) return this.nextSlide(), void t.preventDefault();
                            37 !== t.keyCode && "LeftArrow" !== t.key ? 39 !== t.keyCode && "RightArrow" !== t.key || this.nextSlide() : this.previousSlide() }
                        onMouseLeave() { this.pauseOnFocus || this.play() }
                        onMouseUp() { this.pauseOnFocus || this.play() }
                        pauseFocusIn() { this.pauseOnFocus && (this.isPlaying = !1, this.resetTimer()) }
                        pauseFocusOut() { this.play() }
                        selectSlide(t) { this.isPlaying && this.restartTimer(), this.multilist ? this.selectSlideRange(this.indicatorsByChunk ? t * this.itemsPerSlide : t) : this.activeSlide = this.indicatorsByChunk ? t * this.itemsPerSlide : t }
                        play() { this.isPlaying || (this.isPlaying = !0, this.restartTimer()) }
                        pause() { this.noPause || (this.isPlaying = !1, this.resetTimer()) }
                        getCurrentSlideIndex() { return this._slides.findIndex(this.getActive) }
                        isLast(t) { return t + 1 >= this._slides.length }
                        isFirst(t) { return 0 === t }
                        indicatorsSlides() { return this.slides.filter((t, e) => !this.indicatorsByChunk || e % this.itemsPerSlide == 0) }
                        selectInitialSlides() { const t = this.startFromIndex <= this._slides.length ? this.startFromIndex : 0; if (this.hideSlides(), this.singleSlideOffset) { if (this._slidesWithIndexes = this.mapSlidesAndIndexes(), this._slides.length - t < this.itemsPerSlide) { const e = this._slidesWithIndexes.slice(0, t);
                                    this._slidesWithIndexes = [...this._slidesWithIndexes, ...e].slice(e.length).slice(0, this.itemsPerSlide) } else this._slidesWithIndexes = this._slidesWithIndexes.slice(t, t + this.itemsPerSlide);
                                this._slidesWithIndexes.forEach(t => t.item.active = !0), this.makeSlidesConsistent(this._slidesWithIndexes) } else this.selectRangeByNestedIndex(t);
                            this.slideRangeChange.emit(this.getVisibleIndexes()) }
                        findNextSlideIndex(t, e) { let i = 0; if (e || !this.isLast(this.activeSlide) || t === Ut.PREV || !this.noWrap) { switch (t) {
                                    case Ut.NEXT:
                                        if (void 0 === this._currentActiveSlide) { i = 0; break } if (!this.isLast(this._currentActiveSlide)) { i = this._currentActiveSlide + 1; break }
                                        i = !e && this.noWrap ? this._currentActiveSlide : 0; break;
                                    case Ut.PREV:
                                        if (void 0 === this._currentActiveSlide) { i = 0; break } if (this._currentActiveSlide > 0) { i = this._currentActiveSlide - 1; break } if (!e && this.noWrap) { i = this._currentActiveSlide; break }
                                        i = this._slides.length - 1; break;
                                    default:
                                        throw new Error("Unknown direction") } return i } }
                        mapSlidesAndIndexes() { return this.slides.slice().map((t, e) => ({ index: e, item: t })) }
                        selectSlideRange(t) { if (!this.isIndexInRange(t)) { if (this.hideSlides(), this.singleSlideOffset) { const e = this.isIndexOnTheEdges(t) ? t : t - this.itemsPerSlide + 1,
                                        i = this.isIndexOnTheEdges(t) ? t + this.itemsPerSlide : t + 1;
                                    this._slidesWithIndexes = this.mapSlidesAndIndexes().slice(e, i), this.makeSlidesConsistent(this._slidesWithIndexes), this._slidesWithIndexes.forEach(t => t.item.active = !0) } else this.selectRangeByNestedIndex(t);
                                this.slideRangeChange.emit(this.getVisibleIndexes()) } }
                        selectRangeByNestedIndex(t) { if (!this._chunkedSlides) return; const e = this._chunkedSlides.map((t, e) => ({ index: e, list: t })).find(e => void 0 !== e.list.find(e => e.index === t));
                            e && (this._currentVisibleSlidesIndex = e.index, this._chunkedSlides[e.index].forEach(t => { t.item.active = !0 })) }
                        isIndexOnTheEdges(t) { return t + 1 - this.itemsPerSlide <= 0 || t + this.itemsPerSlide <= this._slides.length }
                        isIndexInRange(t) { return this.singleSlideOffset && this._slidesWithIndexes ? this._slidesWithIndexes.map(t => t.index).indexOf(t) >= 0 : t <= this.getLastVisibleIndex() && t >= this.getFirstVisibleIndex() }
                        hideSlides() { this.slides.forEach(t => t.active = !1) }
                        isVisibleSlideListLast() { return !!this._chunkedSlides && this._currentVisibleSlidesIndex === this._chunkedSlides.length - 1 }
                        isVisibleSlideListFirst() { return 0 === this._currentVisibleSlidesIndex }
                        moveSliderByOneItem(t) { let e, i, s, n, o; if (this.noWrap) { e = this.getFirstVisibleIndex(), i = this.getLastVisibleIndex(), s = t === Ut.NEXT ? e : i, n = t !== Ut.NEXT ? e - 1 : this.isLast(i) ? 0 : i + 1; const o = this._slides.get(s);
                                o && (o.active = !1); const r = this._slides.get(n);
                                r && (r.active = !0); const a = this.mapSlidesAndIndexes().filter(t => t.item.active); return this.makeSlidesConsistent(a), this.singleSlideOffset && (this._slidesWithIndexes = a), void this.slideRangeChange.emit(this.getVisibleIndexes()) } if (this._slidesWithIndexes && this._slidesWithIndexes[0]) { if (e = this._slidesWithIndexes[0].index, i = this._slidesWithIndexes[this._slidesWithIndexes.length - 1].index, t === Ut.NEXT) { this._slidesWithIndexes.shift(), o = this.isLast(i) ? 0 : i + 1; const t = this._slides.get(o);
                                    t && this._slidesWithIndexes.push({ index: o, item: t }) } else { this._slidesWithIndexes.pop(), o = this.isFirst(e) ? this._slides.length - 1 : e - 1; const t = this._slides.get(o);
                                    t && (this._slidesWithIndexes = [{ index: o, item: t }, ...this._slidesWithIndexes]) }
                                this.hideSlides(), this._slidesWithIndexes.forEach(t => t.item.active = !0), this.makeSlidesConsistent(this._slidesWithIndexes), this.slideRangeChange.emit(this._slidesWithIndexes.map(t => t.index)) } }
                        moveMultilist(t) { this.singleSlideOffset ? this.moveSliderByOneItem(t) : (this.hideSlides(), this._currentVisibleSlidesIndex = this.noWrap ? t === Ut.NEXT ? this._currentVisibleSlidesIndex + 1 : this._currentVisibleSlidesIndex - 1 : t === Ut.NEXT ? this.isVisibleSlideListLast() ? 0 : this._currentVisibleSlidesIndex + 1 : this.isVisibleSlideListFirst() ? this._chunkedSlides ? this._chunkedSlides.length - 1 : 0 : this._currentVisibleSlidesIndex - 1, this._chunkedSlides && this._chunkedSlides[this._currentVisibleSlidesIndex].forEach(t => t.item.active = !0), this.slideRangeChange.emit(this.getVisibleIndexes())) }
                        getVisibleIndexes() { return !this.singleSlideOffset && this._chunkedSlides ? this._chunkedSlides[this._currentVisibleSlidesIndex].map(t => t.index) : this._slidesWithIndexes ? this._slidesWithIndexes.map(t => t.index) : void 0 }
                        _select(t) { if (isNaN(t)) return void this.pause(); if (!this.multilist && void 0 !== this._currentActiveSlide) { const t = this._slides.get(this._currentActiveSlide);
                                void 0 !== t && (t.active = !1) } const e = this._slides.get(t);
                            void 0 !== e && (this._currentActiveSlide = t, e.active = !0, this.activeSlide = t, this.activeSlideChange.emit(t)) }
                        restartTimer() { this.resetTimer(); const t = +this.interval;!isNaN(t) && t > 0 && (this.currentInterval = this.ngZone.runOutsideAngular(() => window.setInterval(() => { const t = +this.interval;
                                this.ngZone.run(() => { this.isPlaying && !isNaN(this.interval) && t > 0 && this.slides.length ? this.nextSlideFromInterval() : this.pause() }) }, t))) }
                        get multilist() { return this.itemsPerSlide > 1 }
                        resetTimer() { this.currentInterval && (clearInterval(this.currentInterval), this.currentInterval = void 0) }
                        checkDisabledClass(t) { return "prev" === t ? 0 === this.activeSlide && this.noWrap && !this.multilist || this.isFirstSlideVisible && this.noWrap && this.multilist : this.isLast(this.activeSlide) && this.noWrap && !this.multilist || this.isLastSlideVisible && this.noWrap && this.multilist } } return t.\u0275fac = function(e) { return new(e || t)(n.Y36(Nt), n.Y36(n.R0b)) }, t.\u0275cmp = n.Xpm({ type: t, selectors: [
                            ["carousel"]
                        ], inputs: { noWrap: "noWrap", noPause: "noPause", showIndicators: "showIndicators", pauseOnFocus: "pauseOnFocus", indicatorsByChunk: "indicatorsByChunk", itemsPerSlide: "itemsPerSlide", singleSlideOffset: "singleSlideOffset", isAnimated: "isAnimated", activeSlide: "activeSlide", startFromIndex: "startFromIndex", interval: "interval" }, outputs: { activeSlideChange: "activeSlideChange", slideRangeChange: "slideRangeChange" }, ngContentSelectors: Et, decls: 7, vars: 8, consts: [
                            ["tabindex", "0", 1, "carousel", "slide", 3, "id", "mouseenter", "mouseleave", "mouseup", "keydown", "focusin", "focusout"],
                            [4, "ngIf"],
                            [1, "carousel-inner", 3, "ngStyle"],
                            ["class", "left carousel-control carousel-control-prev", "tabindex", "0", "role", "button", 3, "disabled", "click", 4, "ngIf"],
                            ["class", "right carousel-control carousel-control-next", "tabindex", "0", "role", "button", 3, "disabled", "click", 4, "ngIf"],
                            [1, "carousel-indicators"],
                            [3, "active", "click", 4, "ngFor", "ngForOf"],
                            [3, "click"],
                            ["type", "button", "aria-current", "true", 3, "active", "click", 4, "ngFor", "ngForOf"],
                            ["type", "button", "aria-current", "true", 3, "click"],
                            ["tabindex", "0", "role", "button", 1, "left", "carousel-control", "carousel-control-prev", 3, "click"],
                            ["aria-hidden", "true", 1, "icon-prev", "carousel-control-prev-icon"],
                            ["class", "sr-only visually-hidden", 4, "ngIf"],
                            [1, "sr-only", "visually-hidden"],
                            ["tabindex", "0", "role", "button", 1, "right", "carousel-control", "carousel-control-next", 3, "click"],
                            ["aria-hidden", "true", 1, "icon-next", "carousel-control-next-icon"]
                        ], template: function(t, e) { 1 & t && (n.F$t(), n.TgZ(0, "div", 0), n.NdJ("mouseenter", function() { return e.pause() })("mouseleave", function() { return e.onMouseLeave() })("mouseup", function() { return e.onMouseUp() })("keydown", function(t) { return e.keydownPress(t) })("focusin", function() { return e.pauseFocusIn() })("focusout", function() { return e.pauseFocusOut() }), n.YNc(1, wt, 3, 1, "ng-container", 1), n.YNc(2, Vt, 3, 1, "ng-container", 1), n.TgZ(3, "div", 2), n.Hsn(4), n.qZA(), n.YNc(5, kt, 3, 4, "a", 3), n.YNc(6, qt, 4, 3, "a", 4), n.qZA()), 2 & t && (n.Q6J("id", e.currentId), n.xp6(1), n.Q6J("ngIf", !e._bsVer.isBs5 && e.showIndicators && e.slides.length > 1), n.xp6(1), n.Q6J("ngIf", e._bsVer.isBs5 && e.showIndicators && e.slides.length > 1), n.xp6(1), n.Q6J("ngStyle", n.VKq(6, Mt, e.multilist ? "flex" : "block")), n.xp6(2), n.Q6J("ngIf", e.slides.length > 1), n.xp6(1), n.Q6J("ngIf", e.slides.length > 1)) }, directives: [s.O5, s.PC, s.sg], encapsulation: 2 }), t })(),
                Lt = (() => { class t { constructor(t) { this.active = !1, this.itemWidth = "100%", this.order = 0, this.isAnimated = !1, this.addClass = !0, this.multilist = !1, this.carousel = t }
                        ngOnInit() { this.carousel.addSlide(this), this.itemWidth = 100 / this.carousel.itemsPerSlide + "%", this.multilist = this.carousel ? .itemsPerSlide > 1 }
                        ngOnDestroy() { this.carousel.removeSlide(this) } } return t.\u0275fac = function(e) { return new(e || t)(n.Y36(Ft)) }, t.\u0275cmp = n.Xpm({ type: t, selectors: [
                            ["slide"]
                        ], hostVars: 15, hostBindings: function(t, e) { 2 & t && (n.uIk("aria-hidden", !e.active), n.Udp("width", e.itemWidth)("order", e.order), n.ekj("multilist-margin", e.multilist)("active", e.active)("carousel-animation", e.isAnimated)("item", e.addClass)("carousel-item", e.addClass)) }, inputs: { active: "active" }, ngContentSelectors: Et, decls: 2, vars: 2, consts: [
                            [1, "item"]
                        ], template: function(t, e) { 1 & t && (n.F$t(), n.TgZ(0, "div", 0), n.Hsn(1), n.qZA()), 2 & t && n.ekj("active", e.active) }, styles: [".carousel-animation[_nghost-%COMP%]{transition:opacity .6s ease,visibility .6s ease;float:left}.carousel-animation.active[_nghost-%COMP%]{opacity:1;visibility:visible}.carousel-animation[_nghost-%COMP%]:not(.active){display:block;position:absolute;opacity:0;visibility:hidden}.multilist-margin[_nghost-%COMP%]{margin-right:auto}.carousel-item[_nghost-%COMP%]{perspective:1000px}"] }), t })(),
                Rt = (() => { class t { static forRoot() { return { ngModule: t, providers: [] } } } return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275mod = n.oAB({ type: t }), t.\u0275inj = n.cJS({ imports: [
                            [s.ez]
                        ] }), t })(),
                Jt = (() => { class t { constructor() {}
                        ngOnInit() {} } return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275cmp = n.Xpm({ type: t, selectors: [
                            ["app-banner"]
                        ], decls: 24, vars: 1, consts: [
                            [1, "banner", "mb-3"],
                            [1, "banner-body"],
                            [3, "isAnimated"],
                            ["src", "https://th.bing.com/th/id/OIP.RVi3VgE9dH_PTT8OZLZaRwHaE7?pid=ImgDet&rs=1", "alt", "", 1, "img-fluid", "w-100"],
                            [1, "carousel-caption", "d-none", "d-md-block"],
                            ["src", "https://th.bing.com/th/id/OIP.FGnZeH8uWxsO6bAMsg8FzwHaE3?pid=ImgDet&rs=1", "alt", "", 1, "img-fluid", "w-100"],
                            ["src", "https://ftw.usatoday.com/wp-content/uploads/sites/90/2016/07/afp_552086019_81997609.jpg", "alt", "", 1, "img-fluid", "w-100"]
                        ], template: function(t, e) { 1 & t && (n.TgZ(0, "div", 0), n.TgZ(1, "div", 1), n.TgZ(2, "carousel", 2), n.TgZ(3, "slide"), n._UZ(4, "img", 3), n.TgZ(5, "div", 4), n.TgZ(6, "h3"), n._uU(7, "First slide label"), n.qZA(), n.TgZ(8, "p"), n._uU(9, "Nulla vitae elit libero, a pharetra augue mollis interdum."), n.qZA(), n.qZA(), n.qZA(), n.TgZ(10, "slide"), n._UZ(11, "img", 5), n.TgZ(12, "div", 4), n.TgZ(13, "h3"), n._uU(14, "First slide label"), n.qZA(), n.TgZ(15, "p"), n._uU(16, "Nulla vitae elit libero, a pharetra augue mollis interdum."), n.qZA(), n.qZA(), n.qZA(), n.TgZ(17, "slide"), n._UZ(18, "img", 6), n.TgZ(19, "div", 4), n.TgZ(20, "h3"), n._uU(21, "First slide label"), n.qZA(), n.TgZ(22, "p"), n._uU(23, "Nulla vitae elit libero, a pharetra augue mollis interdum."), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.qZA()), 2 & t && (n.xp6(2), n.Q6J("isAnimated", !0)) }, directives: [Ft, Lt], styles: [""] }), t })(); const Bt = function(t) { return { active: t } };

            function jt(t, e) { if (1 & t) { const t = n.EpF();
                    n.TgZ(0, "div", 19), n.NdJ("click", function() { const e = n.CHM(t).$implicit; return n.oxw().getRounds(e.SeriesID, e.SeriesGUID) }), n.TgZ(1, "div", 20), n._UZ(2, "img", 21), n.qZA(), n.TgZ(3, "div", 22), n._uU(4), n.qZA(), n.qZA() } if (2 & t) { const t = e.$implicit,
                        i = n.oxw();
                    n.xp6(2), n.Q6J("src", t.SeriesFlag, n.LSH), n.xp6(1), n.Q6J("ngClass", n.VKq(3, Bt, i.selectedSeriesId == t.SeriesID)), n.xp6(1), n.Oqu(t.SeriesName) } }

            function Wt(t, e) { if (1 & t) { const t = n.EpF();
                    n.TgZ(0, "div", 19), n.NdJ("click", function() { const e = n.CHM(t).$implicit; return n.oxw().getMatches(e.RoundID) }), n.TgZ(1, "div", 22), n.TgZ(2, "h5"), n._uU(3), n.qZA(), n.TgZ(4, "p"), n._uU(5), n.ALo(6, "date"), n.ALo(7, "date"), n.qZA(), n.qZA(), n.qZA() } if (2 & t) { const t = e.$implicit,
                        i = n.oxw();
                    n.xp6(1), n.Q6J("ngClass", n.VKq(10, Bt, i.selectedRoundsId == t.RoundID)), n.xp6(2), n.Oqu(t.RoundName), n.xp6(2), n.AsE("", n.xi3(6, 4, t.SeriesStartDateUTC, "dd-MM-yyy"), " - ", n.xi3(7, 7, t.SeriesEndDateUTC, "dd-MM-yyy"), " ") } }

            function Qt(t, e) { 1 & t && (n.TgZ(0, "div", 23), n.TgZ(1, "div", 24), n.TgZ(2, "span", 25), n._uU(3, "Make your predictions"), n.qZA(), n.TgZ(4, "span"), n._uU(5, "Monday, 19 July 2021"), n.qZA(), n.qZA(), n.qZA()) }

            function Yt(t, e) { if (1 & t && (n.TgZ(0, "div", 26), n.TgZ(1, "div", 27), n.TgZ(2, "div", 28), n.TgZ(3, "a", 29), n._UZ(4, "i", 30), n.qZA(), n.qZA(), n.TgZ(5, "div", 4), n.TgZ(6, "div", 31), n._UZ(7, "img", 32), n.TgZ(8, "p"), n._uU(9), n.qZA(), n.qZA(), n.TgZ(10, "div", 31), n.TgZ(11, "p", 33), n._uU(12), n.ALo(13, "date"), n.qZA(), n.qZA(), n.TgZ(14, "div", 31), n._UZ(15, "img", 32), n.TgZ(16, "p"), n._uU(17), n.qZA(), n.qZA(), n.qZA(), n.TgZ(18, "div", 4), n.TgZ(19, "div", 34), n.TgZ(20, "button", 35), n._uU(21, "WIN"), n.qZA(), n.qZA(), n.TgZ(22, "div", 34), n.TgZ(23, "button", 35), n._uU(24, "TIE"), n.qZA(), n.qZA(), n.TgZ(25, "div", 34), n.TgZ(26, "button", 35), n._uU(27, "WIN"), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.qZA()), 2 & t) { const t = e.$implicit;
                    n.xp6(7), n.Q6J("src", t.TeamFlagLocal, n.LSH), n.xp6(2), n.Oqu(t.MatchScoreDetails.TeamScoreLocal.Name), n.xp6(3), n.Oqu(n.xi3(13, 5, t.MatchStartDateTimeUTC, "hh:mm a")), n.xp6(3), n.Q6J("src", t.TeamFlagVisitor, n.LSH), n.xp6(2), n.Oqu(t.MatchScoreDetails.TeamScoreVisitor.Name) } }

            function Gt(t, e) { 1 & t && (n.TgZ(0, "div", 36), n.TgZ(1, "div", 37), n.TgZ(2, "button", 38), n._uU(3, "My Prediction"), n.qZA(), n.TgZ(4, "button", 38), n._uU(5, "Results"), n.qZA(), n.TgZ(6, "button", 38), n._uU(7, "Our prizes"), n.qZA(), n.qZA(), n.qZA()) }

            function Ht(t, e) { if (1 & t && (n.TgZ(0, "div", 41), n._UZ(1, "img", 42), n.TgZ(2, "small", 43), n._uU(3), n.ALo(4, "date"), n.qZA(), n.TgZ(5, "h2", 44), n._uU(6), n.qZA(), n.TgZ(7, "span"), n.TgZ(8, "a", 29), n._uU(9), n.qZA(), n.qZA(), n.qZA()), 2 & t) { const t = e.$implicit;
                    n.xp6(1), n.Q6J("src", t.media_url, n.LSH), n.xp6(2), n.Oqu(n.xi3(4, 4, t.news_date, "dd MMM yyyy")), n.xp6(3), n.Oqu(t.title), n.xp6(3), n.Oqu(t.source_name) } } const zt = function(t) { return { "show active news-tab": t } };

            function $t(t, e) { if (1 & t && (n.TgZ(0, "div", 39), n.YNc(1, Ht, 10, 7, "div", 40), n.qZA()), 2 & t) { const t = n.oxw();
                    n.Q6J("ngClass", n.VKq(2, zt, "News" == t.tab)), n.xp6(1), n.Q6J("ngForOf", t.newsList.slice(0, 10)) } }

            function Xt(t, e) { if (1 & t && (n.TgZ(0, "div", 52), n.TgZ(1, "div", 4), n.TgZ(2, "div", 53), n._UZ(3, "img", 54), n._uU(4), n.qZA(), n.TgZ(5, "div", 55), n._uU(6), n.ALo(7, "date"), n.qZA(), n.TgZ(8, "div", 56), n._uU(9), n._UZ(10, "img", 54), n.qZA(), n.qZA(), n.qZA()), 2 & t) { const t = e.$implicit;
                    n.xp6(3), n.Q6J("src", t.TeamFlagLocal, n.LSH), n.xp6(1), n.hij(" ", t.MatchScoreDetails.TeamScoreLocal.Name, " "), n.xp6(2), n.hij(" ", n.xi3(7, 5, t.MatchStartDateTimeUTC, "hh:mm a"), " "), n.xp6(3), n.hij(" ", t.MatchScoreDetails.TeamScoreVisitor.Name, " "), n.xp6(1), n.Q6J("src", t.TeamFlagVisitor, n.LSH) } }

            function Kt(t, e) { if (1 & t && (n.TgZ(0, "div", 45), n.TgZ(1, "div", 46), n.TgZ(2, "div", 47), n.TgZ(3, "p"), n.TgZ(4, "strong"), n._uU(5, "Yesterday"), n.qZA(), n.qZA(), n.TgZ(6, "span"), n._uU(7, "19-02-2021"), n.qZA(), n.qZA(), n.TgZ(8, "div", 47), n.TgZ(9, "p"), n.TgZ(10, "strong"), n._uU(11, "Today"), n.qZA(), n.qZA(), n.TgZ(12, "span"), n._uU(13, "19-02-2021"), n.qZA(), n.qZA(), n.TgZ(14, "div", 47), n.TgZ(15, "p"), n.TgZ(16, "strong"), n._uU(17, "Tomorrow"), n.qZA(), n.qZA(), n.TgZ(18, "span"), n._uU(19, "19-02-2021"), n.qZA(), n.qZA(), n.qZA(), n.TgZ(20, "div", 48), n.TgZ(21, "div", 49), n.TgZ(22, "h4"), n._uU(23, "Saudi Professional League"), n.qZA(), n.qZA(), n.TgZ(24, "div", 50), n.YNc(25, Xt, 11, 8, "div", 51), n.qZA(), n.qZA(), n.qZA()), 2 & t) { const t = n.oxw();
                    n.Q6J("ngClass", n.VKq(2, zt, "All Matches" == t.tab)), n.xp6(25), n.Q6J("ngForOf", t.matchesByRounds) } } let te = (() => { class t { constructor(t, e) { this.newsService = t, this.seriesService = e, this.tab = "News", this.imagePlaceholderUrl = X.N.imagePlaceholderUrl, this.selectedSeriesId = "", this.selectedRoundsId = "", this.newsList = [], this.seriesList = [], this.roundsBySeries = [], this.matchesByRounds = [] }
                        ngOnInit() { this.getSeries(), this.getNews() }
                        changeTab(t) { this.tab = t, "All Matches" == t && this.getMatches("") }
                        getNews() { this.newsService.getAllNews().subscribe(t => { this.newsList = t.Data.Records }) }
                        getSeries() { this.seriesService.getAllSeries().subscribe(t => { this.seriesList = t.Data.Records }) }
                        getRounds(t, e) { this.selectedSeriesId = t, this.selectedSeriesGUID = e, this.seriesService.getRoundsBySeries(e).subscribe(t => { this.roundsBySeries = t.Data.Records }) }
                        getMatches(t) { this.selectedRoundsId = t, this.seriesService.getMatchesByRounds(this.selectedSeriesGUID, t).subscribe(t => { this.matchesByRounds = t.Data.Records }) } } return t.\u0275fac = function(e) { return new(e || t)(n.Y36(bt), n.Y36(yt)) }, t.\u0275cmp = n.Xpm({ type: t, selectors: [
                            ["app-home"]
                        ], decls: 24, vars: 13, consts: [
                            [1, "wrapper"],
                            [1, "container"],
                            [1, "row", "mt-4", "mb-4"],
                            [1, "col-lg-8"],
                            [1, "row"],
                            [1, "owl-carousel"],
                            ["class", "owl-item", 3, "click", 4, "ngFor", "ngForOf"],
                            [1, "owl-carousel", "mt-4"],
                            ["class", "col-md-12 mt-4", 4, "ngIf"],
                            ["class", "col-md-6", 4, "ngFor", "ngForOf"],
                            ["class", "row main-btn mt-4", 4, "ngIf"],
                            [1, "col-lg-4"],
                            ["id", "pills-tab", "role", "tablist", 1, "nav", "nav-pills", "mb-1", "aside-tab"],
                            ["role", "presentation", 1, "nav-item", 2, "cursor", "pointer", 3, "click"],
                            ["id", "pills-home-tab", "data-toggle", "pill", "role", "tab", "aria-controls", "pills-home", "aria-selected", "true", 1, "nav-link", 3, "ngClass"],
                            ["id", "pills-profile-tab", "data-toggle", "pill", "role", "tab", "aria-controls", "pills-profile", "aria-selected", "false", 1, "nav-link", 3, "ngClass"],
                            ["id", "pills-tabContent", 1, "tab-content", "rounded-bottom"],
                            ["class", "tab-pane fade", "id", "pills-home", "role", "tabpanel", "aria-labelledby", "pills-home-tab", 3, "ngClass", 4, "ngIf"],
                            ["class", "tab-panel fade", "id", "pills-profile", "role", "tabpanel", "aria-labelledby", "pills-profile-tab", 3, "ngClass", 4, "ngIf"],
                            [1, "owl-item", 3, "click"],
                            [1, "league-logo"],
                            ["alt", "goalak", 3, "src"],
                            [1, "league-name", 3, "ngClass"],
                            [1, "col-md-12", "mt-4"],
                            [1, "heading-box"],
                            [1, "black"],
                            [1, "col-md-6"],
                            [1, "matches-box"],
                            [1, "icon"],
                            ["href", "#"],
                            [1, "bi", "bi-share-fill"],
                            [1, "col-md-4", "eboxes"],
                            ["alt", "", 3, "src"],
                            [1, "time"],
                            [1, "col-md-4"],
                            ["type", "button", 1, "btn", "btn-secondary"],
                            [1, "row", "main-btn", "mt-4"],
                            [1, "col-md-12"],
                            ["type", "button", 1, "btn", "btn-success", "shadow-sm"],
                            ["id", "pills-home", "role", "tabpanel", "aria-labelledby", "pills-home-tab", 1, "tab-pane", "fade", 3, "ngClass"],
                            ["class", "news-box", 4, "ngFor", "ngForOf"],
                            [1, "news-box"],
                            ["onerror", "this.src='https://forodesaopaulo.org/wp-content/uploads/2019/10/image-placeholder-26-min-uai-1032x688.jpg'", "alt", "goalak", 1, "mb-2", 3, "src"],
                            [1, "text-muted"],
                            [1, "mt-2"],
                            ["id", "pills-profile", "role", "tabpanel", "aria-labelledby", "pills-profile-tab", 1, "tab-panel", "fade", 3, "ngClass"],
                            [1, "match-tab"],
                            [1, "col-md-4", "match-day-box"],
                            [1, "row", "match-box"],
                            [1, "col-12", "mt-4", "mb-3"],
                            [1, "col-12"],
                            ["class", "list mr-1 ml-1 p-2 mb-1", 4, "ngFor", "ngForOf"],
                            [1, "list", "mr-1", "ml-1", "p-2", "mb-1"],
                            [1, "col-4"],
                            ["width", "22", "alt", "goalak", 3, "src"],
                            [1, "col-4", "bg-white", "text-center", "pt-1"],
                            [1, "col-4", 2, "text-align", "right"]
                        ], template: function(t, e) { 1 & t && (n.TgZ(0, "div", 0), n.TgZ(1, "div", 1), n.TgZ(2, "div", 2), n.TgZ(3, "div", 3), n._UZ(4, "app-banner"), n.TgZ(5, "div", 4), n.TgZ(6, "div", 5), n.YNc(7, jt, 5, 5, "div", 6), n.qZA(), n.TgZ(8, "div", 7), n.YNc(9, Wt, 8, 12, "div", 6), n.qZA(), n.YNc(10, Qt, 6, 0, "div", 8), n.YNc(11, Yt, 28, 8, "div", 9), n.YNc(12, Gt, 8, 0, "div", 10), n.qZA(), n.qZA(), n.TgZ(13, "div", 11), n.TgZ(14, "ul", 12), n.TgZ(15, "li", 13), n.NdJ("click", function() { return e.changeTab("News") }), n.TgZ(16, "a", 14), n._uU(17, "News"), n.qZA(), n.qZA(), n.TgZ(18, "li", 13), n.NdJ("click", function() { return e.changeTab("All Matches") }), n.TgZ(19, "a", 15), n._uU(20, "All Matches"), n.qZA(), n.qZA(), n.qZA(), n.TgZ(21, "div", 16), n.YNc(22, $t, 2, 4, "div", 17), n.YNc(23, Kt, 26, 4, "div", 18), n.qZA(), n.qZA(), n.qZA(), n.qZA(), n.qZA()), 2 & t && (n.xp6(7), n.Q6J("ngForOf", e.seriesList), n.xp6(2), n.Q6J("ngForOf", e.roundsBySeries), n.xp6(1), n.Q6J("ngIf", (null == e.matchesByRounds ? null : e.matchesByRounds.length) > 0), n.xp6(1), n.Q6J("ngForOf", e.matchesByRounds), n.xp6(1), n.Q6J("ngIf", (null == e.matchesByRounds ? null : e.matchesByRounds.length) > 0), n.xp6(4), n.Q6J("ngClass", n.VKq(9, Bt, "News" == e.tab)), n.xp6(3), n.Q6J("ngClass", n.VKq(11, Bt, "All Matches" == e.tab)), n.xp6(3), n.Q6J("ngIf", "News" == e.tab), n.xp6(1), n.Q6J("ngIf", "All Matches" == e.tab)) }, directives: [Jt, s.sg, s.O5, s.mk], pipes: [s.uU], styles: [".owl-carousel[_ngcontent-%COMP%]{width:100%;overflow:auto;white-space:nowrap;display:flex;flex-direction:row;grid-gap:1rem;gap:1rem}.owl-carousel[_ngcontent-%COMP%]::-webkit-scrollbar{display:none!important;width:0!important;height:0!important;background:transparent!important}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]{text-align:center;cursor:pointer}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]:hover   .league-name[_ngcontent-%COMP%]{background-color:#ffd710}.owl-carousel.owl-drag[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]{background-color:#fff}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .league-logo[_ngcontent-%COMP%]{height:80px}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .league-logo[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:auto;margin-left:auto;margin-right:auto;padding:10px;height:80px}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .league-name[_ngcontent-%COMP%]{border-top:1px solid #f4f4f4;border-right:1px solid #f4f4f4;padding:5px 10px;font-size:12px;text-align:center;font-weight:600;border-radius:3px}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .league-name[_ngcontent-%COMP%]   h5[_ngcontent-%COMP%]{font-size:14px;font-weight:600}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .league-name[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{font-size:12px;margin-bottom:0}.owl-nav[_ngcontent-%COMP%]{display:none}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]{background-color:#ffd710}.heading-box[_ngcontent-%COMP%]{display:flex;justify-content:space-between;align-items:center;background-color:red;background-image:linear-gradient(90deg,#f4f4f4,#777);padding:7px 15px;color:#fff;font-weight:500}.heading-box[_ngcontent-%COMP%]   .black[_ngcontent-%COMP%]{color:#000}.matches-box[_ngcontent-%COMP%]{padding:15px;background:transparent;cursor:pointer;margin-top:15px;border-top:1px solid #f4f4f4;border-right:1px solid #f4f4f4}.matches-box[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%]{background-color:#eb5d6e;color:#fff}.matches-box[_ngcontent-%COMP%]   .eboxes[_ngcontent-%COMP%]{display:flex;flex-direction:column;align-items:center;justify-content:center}.matches-box[_ngcontent-%COMP%]   .eboxes[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:50px}.matches-box[_ngcontent-%COMP%]   .eboxes[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{margin-top:10px;font-weight:400;font-size:14px}.matches-box[_ngcontent-%COMP%]   .eboxes[_ngcontent-%COMP%]   .time[_ngcontent-%COMP%]{color:#eb5d6e}.btn-secondary[_ngcontent-%COMP%]{width:100%;font-size:10px}.btn-secondary[_ngcontent-%COMP%]:hover{background:#007e0d}.main-btn[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]{background-color:#eb5d6e;min-width:120px;border:none;border-radius:30px;font-size:14px;margin:10px}.nav-item[_ngcontent-%COMP%]{border:1px solid #0d6efd;border-radius:5px}.nav-item[_ngcontent-%COMP%]:hover   a[_ngcontent-%COMP%]{background-color:#0d6efd;color:#fff}.news-box[_ngcontent-%COMP%]{padding:10px;cursor:pointer;margin-top:15px;border-top:1px solid #f4f4f4;border-left:1px solid #f4f4f4}.match-tab[_ngcontent-%COMP%]{display:flex;margin-top:20px}.match-day-box[_ngcontent-%COMP%]{cursor:pointer;padding:10px;font-size:12px;font-weight:500;text-align:center;border-radius:5px}.match-day-box[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{font-size:12px;margin-bottom:0}.match-day-box[_ngcontent-%COMP%]:hover{background:#ffd710}"] }), t })(),
                ee = (() => { class t {} return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275mod = n.oAB({ type: t }), t.\u0275inj = n.cJS({ imports: [
                            [s.ez, z.Tx, $.ZX, G, Rt.forRoot(), H.Bz.forChild([{ path: "", component: vt, children: [{ path: "", redirectTo: "dashboard", pathMatch: "full" }, { path: "dashboard", component: te }] }])]
                        ] }), t })() } }
]);