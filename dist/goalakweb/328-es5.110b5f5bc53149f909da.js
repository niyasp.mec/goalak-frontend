! function() {
    function t(t) { return function(t) { if (Array.isArray(t)) return n(t) }(t) || function(t) { if ("undefined" != typeof Symbol && null != t[Symbol.iterator] || null != t["@@iterator"]) return Array.from(t) }(t) || e(t) || function() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.") }() }

    function e(t, e) { if (t) { if ("string" == typeof t) return n(t, e); var i = Object.prototype.toString.call(t).slice(8, -1); return "Object" === i && t.constructor && (i = t.constructor.name), "Map" === i || "Set" === i ? Array.from(t) : "Arguments" === i || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i) ? n(t, e) : void 0 } }

    function n(t, e) {
        (null == e || e > t.length) && (e = t.length); for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n]; return i }

    function i(t, e) { if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
        t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } }), Object.defineProperty(t, "prototype", { writable: !1 }), e && s(t, e) }

    function s(t, e) { return (s = Object.setPrototypeOf || function(t, e) { return t.__proto__ = e, t })(t, e) }

    function o(t) { var e = function() { if ("undefined" == typeof Reflect || !Reflect.construct) return !1; if (Reflect.construct.sham) return !1; if ("function" == typeof Proxy) return !0; try { return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function() {})), !0 } catch (t) { return !1 } }(); return function() { var n, i = a(t); if (e) { var s = a(this).constructor;
                n = Reflect.construct(i, arguments, s) } else n = i.apply(this, arguments); return r(this, n) } }

    function r(t, e) { if (e && ("object" == typeof e || "function" == typeof e)) return e; if (void 0 !== e) throw new TypeError("Derived constructors may only return object or undefined"); return function(t) { if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); return t }(t) }

    function a(t) { return (a = Object.setPrototypeOf ? Object.getPrototypeOf : function(t) { return t.__proto__ || Object.getPrototypeOf(t) })(t) }

    function l(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }

    function u(t, e) { for (var n = 0; n < e.length; n++) { var i = e[n];
            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i) } }

    function c(t, e, n) { return e && u(t.prototype, e), n && u(t, n), Object.defineProperty(t, "prototype", { writable: !1 }), t }(self.webpackChunkgoalakweb = self.webpackChunkgoalakweb || []).push([
        [328], { 9328: function(n, s, r) { "use strict";
                r.r(s), r.d(s, { PartialsModule: function() { return le } }); var a = r(8583),
                    u = r(3018),
                    d = r(4402),
                    h = r(7574),
                    f = r(9796),
                    g = r(8002),
                    v = r(1555);

                function p(t, e) { return new h.y(function(n) { var i = t.length; if (0 !== i)
                            for (var s = new Array(i), o = 0, r = 0, a = function(a) { var l = (0, d.D)(t[a]),
                                        u = !1;
                                    n.add(l.subscribe({ next: function(t) { u || (u = !0, r++), s[a] = t }, error: function(t) { return n.error(t) }, complete: function() {++o !== i && u || (r === i && n.next(e ? e.reduce(function(t, e, n) { return t[e] = s[n], t }, {}) : s), n.complete()) } })) }, l = 0; l < i; l++) a(l);
                        else n.complete() }) } var y = function() { var t = function() {
                            function t(e, n) { l(this, t), this._renderer = e, this._elementRef = n, this.onChange = function(t) {}, this.onTouched = function() {} } return c(t, [{ key: "setProperty", value: function(t, e) { this._renderer.setProperty(this._elementRef.nativeElement, t, e) } }, { key: "registerOnTouched", value: function(t) { this.onTouched = t } }, { key: "registerOnChange", value: function(t) { this.onChange = t } }, { key: "setDisabledState", value: function(t) { this.setProperty("disabled", t) } }]), t }(); return t.\u0275fac = function(e) { return new(e || t)(u.Y36(u.Qsj), u.Y36(u.SBq)) }, t.\u0275dir = u.lG2({ type: t }), t }(),
                    m = function() { var t, e = function(t) { i(n, t); var e = o(n);

                            function n() { return l(this, n), e.apply(this, arguments) } return c(n) }(y); return e.\u0275fac = function(n) { return (t || (t = u.n5z(e)))(n || e) }, e.\u0275dir = u.lG2({ type: e, features: [u.qOj] }), e }(),
                    _ = new u.OlP("NgValueAccessor"),
                    k = { provide: _, useExisting: (0, u.Gpc)(function() { return A }), multi: !0 },
                    b = new u.OlP("CompositionEventMode"),
                    A = function() { var t = function(t) { i(n, t); var e = o(n);

                            function n(t, i, s) { var o, r; return l(this, n), (o = e.call(this, t, i))._compositionMode = s, o._composing = !1, null == o._compositionMode && (o._compositionMode = (r = (0, a.q)() ? (0, a.q)().getUserAgent() : "", !/android (\d+)/.test(r.toLowerCase()))), o } return c(n, [{ key: "writeValue", value: function(t) { this.setProperty("value", null == t ? "" : t) } }, { key: "_handleInput", value: function(t) {
                                    (!this._compositionMode || this._compositionMode && !this._composing) && this.onChange(t) } }, { key: "_compositionStart", value: function() { this._composing = !0 } }, { key: "_compositionEnd", value: function(t) { this._composing = !1, this._compositionMode && this.onChange(t) } }]), n }(y); return t.\u0275fac = function(e) { return new(e || t)(u.Y36(u.Qsj), u.Y36(u.SBq), u.Y36(b, 8)) }, t.\u0275dir = u.lG2({ type: t, selectors: [
                                ["input", "formControlName", "", 3, "type", "checkbox"],
                                ["textarea", "formControlName", ""],
                                ["input", "formControl", "", 3, "type", "checkbox"],
                                ["textarea", "formControl", ""],
                                ["input", "ngModel", "", 3, "type", "checkbox"],
                                ["textarea", "ngModel", ""],
                                ["", "ngDefaultControl", ""]
                            ], hostBindings: function(t, e) { 1 & t && u.NdJ("input", function(t) { return e._handleInput(t.target.value) })("blur", function() { return e.onTouched() })("compositionstart", function() { return e._compositionStart() })("compositionend", function(t) { return e._compositionEnd(t.target.value) }) }, features: [u._Bn([k]), u.qOj] }), t }(),
                    Z = new u.OlP("NgValidators"),
                    C = new u.OlP("NgAsyncValidators");

                function x(t) { return null != t }

                function S(t) { var e = (0, u.QGY)(t) ? (0, d.D)(t) : t; return (0, u.CqO)(e), e }

                function T(t) { var e = {}; return t.forEach(function(t) { e = null != t ? Object.assign(Object.assign({}, e), t) : e }), 0 === Object.keys(e).length ? null : e }

                function O(t, e) { return e.map(function(e) { return e(t) }) }

                function w(t) { return t.map(function(t) { return function(t) { return !t.validate }(t) ? t : function(e) { return t.validate(e) } }) }

                function P(t) { return null != t ? function(t) { if (!t) return null; var e = t.filter(x); return 0 == e.length ? null : function(t) { return T(O(t, e)) } }(w(t)) : null }

                function I(e) { return null != e ? function(e) { if (!e) return null; var n = e.filter(x); return 0 == n.length ? null : function(e) { return function() { for (var e = arguments.length, n = new Array(e), i = 0; i < e; i++) n[i] = arguments[i]; if (1 === n.length) { var s = n[0]; if ((0, f.k)(s)) return p(s, null); if ((0, v.K)(s) && Object.getPrototypeOf(s) === Object.prototype) { var o = Object.keys(s); return p(o.map(function(t) { return s[t] }), o) } } if ("function" == typeof n[n.length - 1]) { var r = n.pop(); return p(n = 1 === n.length && (0, f.k)(n[0]) ? n[0] : n, null).pipe((0, g.U)(function(e) { return r.apply(void 0, t(e)) })) } return p(n, null) }(O(e, n).map(S)).pipe((0, g.U)(T)) } }(w(e)) : null }

                function V(e, n) { return null === e ? [n] : Array.isArray(e) ? [].concat(t(e), [n]) : [e, n] } var q = function() { var t = function() {
                            function t() { l(this, t), this._rawValidators = [], this._rawAsyncValidators = [], this._onDestroyCallbacks = [] } return c(t, [{ key: "value", get: function() { return this.control ? this.control.value : null } }, { key: "valid", get: function() { return this.control ? this.control.valid : null } }, { key: "invalid", get: function() { return this.control ? this.control.invalid : null } }, { key: "pending", get: function() { return this.control ? this.control.pending : null } }, { key: "disabled", get: function() { return this.control ? this.control.disabled : null } }, { key: "enabled", get: function() { return this.control ? this.control.enabled : null } }, { key: "errors", get: function() { return this.control ? this.control.errors : null } }, { key: "pristine", get: function() { return this.control ? this.control.pristine : null } }, { key: "dirty", get: function() { return this.control ? this.control.dirty : null } }, { key: "touched", get: function() { return this.control ? this.control.touched : null } }, { key: "status", get: function() { return this.control ? this.control.status : null } }, { key: "untouched", get: function() { return this.control ? this.control.untouched : null } }, { key: "statusChanges", get: function() { return this.control ? this.control.statusChanges : null } }, { key: "valueChanges", get: function() { return this.control ? this.control.valueChanges : null } }, { key: "path", get: function() { return null } }, { key: "_setValidators", value: function(t) { this._rawValidators = t || [], this._composedValidatorFn = P(this._rawValidators) } }, { key: "_setAsyncValidators", value: function(t) { this._rawAsyncValidators = t || [], this._composedAsyncValidatorFn = I(this._rawAsyncValidators) } }, { key: "validator", get: function() { return this._composedValidatorFn || null } }, { key: "asyncValidator", get: function() { return this._composedAsyncValidatorFn || null } }, { key: "_registerOnDestroy", value: function(t) { this._onDestroyCallbacks.push(t) } }, { key: "_invokeOnDestroyCallbacks", value: function() { this._onDestroyCallbacks.forEach(function(t) { return t() }), this._onDestroyCallbacks = [] } }, { key: "reset", value: function(t) { this.control && this.control.reset(t) } }, { key: "hasError", value: function(t, e) { return !!this.control && this.control.hasError(t, e) } }, { key: "getError", value: function(t, e) { return this.control ? this.control.getError(t, e) : null } }]), t }(); return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275dir = u.lG2({ type: t }), t }(),
                    M = function() { var t, e = function(t) { i(n, t); var e = o(n);

                            function n() { return l(this, n), e.apply(this, arguments) } return c(n, [{ key: "formDirective", get: function() { return null } }, { key: "path", get: function() { return null } }]), n }(q); return e.\u0275fac = function(n) { return (t || (t = u.n5z(e)))(n || e) }, e.\u0275dir = u.lG2({ type: e, features: [u.qOj] }), e }(),
                    E = function(t) { i(n, t); var e = o(n);

                        function n() { var t; return l(this, n), (t = e.apply(this, arguments))._parent = null, t.name = null, t.valueAccessor = null, t } return c(n) }(q),
                    N = function() { var t = function(t) { i(n, t); var e = o(n);

                            function n(t) { return l(this, n), e.call(this, t) } return c(n) }(function() {
                            function t(e) { l(this, t), this._cd = e } return c(t, [{ key: "is", value: function(t) { var e, n, i; return "submitted" === t ? !!(null === (e = this._cd) || void 0 === e ? void 0 : e.submitted) : !!(null === (i = null === (n = this._cd) || void 0 === n ? void 0 : n.control) || void 0 === i ? void 0 : i[t]) } }]), t }()); return t.\u0275fac = function(e) { return new(e || t)(u.Y36(E, 2)) }, t.\u0275dir = u.lG2({ type: t, selectors: [
                                ["", "formControlName", ""],
                                ["", "ngModel", ""],
                                ["", "formControl", ""]
                            ], hostVars: 14, hostBindings: function(t, e) { 2 & t && u.ekj("ng-untouched", e.is("untouched"))("ng-touched", e.is("touched"))("ng-pristine", e.is("pristine"))("ng-dirty", e.is("dirty"))("ng-valid", e.is("valid"))("ng-invalid", e.is("invalid"))("ng-pending", e.is("pending")) }, features: [u.qOj] }), t }();

                function U(t, e) { t.forEach(function(t) { t.registerOnValidatorChange && t.registerOnValidatorChange(e) }) }

                function D(t, e) { t._pendingDirty && t.markAsDirty(), t.setValue(t._pendingValue, { emitModelToViewChange: !1 }), e.viewToModelUpdate(t._pendingValue), t._pendingChange = !1 }

                function F(t, e) { var n = t.indexOf(e);
                    n > -1 && t.splice(n, 1) }

                function L(t) { return (J(t) ? t.validators : t) || null }

                function R(t) { return Array.isArray(t) ? P(t) : t || null }

                function j(t, e) { return (J(e) ? e.asyncValidators : t) || null }

                function B(t) { return Array.isArray(t) ? I(t) : t || null }

                function J(t) { return null != t && !Array.isArray(t) && "object" == typeof t } var W = function() {
                        function t(e, n) { l(this, t), this._hasOwnPendingAsyncValidator = !1, this._onCollectionChange = function() {}, this._parent = null, this.pristine = !0, this.touched = !1, this._onDisabledChange = [], this._rawValidators = e, this._rawAsyncValidators = n, this._composedValidatorFn = R(this._rawValidators), this._composedAsyncValidatorFn = B(this._rawAsyncValidators) } return c(t, [{ key: "validator", get: function() { return this._composedValidatorFn }, set: function(t) { this._rawValidators = this._composedValidatorFn = t } }, { key: "asyncValidator", get: function() { return this._composedAsyncValidatorFn }, set: function(t) { this._rawAsyncValidators = this._composedAsyncValidatorFn = t } }, { key: "parent", get: function() { return this._parent } }, { key: "valid", get: function() { return "VALID" === this.status } }, { key: "invalid", get: function() { return "INVALID" === this.status } }, { key: "pending", get: function() { return "PENDING" == this.status } }, { key: "disabled", get: function() { return "DISABLED" === this.status } }, { key: "enabled", get: function() { return "DISABLED" !== this.status } }, { key: "dirty", get: function() { return !this.pristine } }, { key: "untouched", get: function() { return !this.touched } }, { key: "updateOn", get: function() { return this._updateOn ? this._updateOn : this.parent ? this.parent.updateOn : "change" } }, { key: "setValidators", value: function(t) { this._rawValidators = t, this._composedValidatorFn = R(t) } }, { key: "setAsyncValidators", value: function(t) { this._rawAsyncValidators = t, this._composedAsyncValidatorFn = B(t) } }, { key: "clearValidators", value: function() { this.validator = null } }, { key: "clearAsyncValidators", value: function() { this.asyncValidator = null } }, { key: "markAsTouched", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                this.touched = !0, this._parent && !t.onlySelf && this._parent.markAsTouched(t) } }, { key: "markAllAsTouched", value: function() { this.markAsTouched({ onlySelf: !0 }), this._forEachChild(function(t) { return t.markAllAsTouched() }) } }, { key: "markAsUntouched", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                this.touched = !1, this._pendingTouched = !1, this._forEachChild(function(t) { t.markAsUntouched({ onlySelf: !0 }) }), this._parent && !t.onlySelf && this._parent._updateTouched(t) } }, { key: "markAsDirty", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                this.pristine = !1, this._parent && !t.onlySelf && this._parent.markAsDirty(t) } }, { key: "markAsPristine", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                this.pristine = !0, this._pendingDirty = !1, this._forEachChild(function(t) { t.markAsPristine({ onlySelf: !0 }) }), this._parent && !t.onlySelf && this._parent._updatePristine(t) } }, { key: "markAsPending", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                this.status = "PENDING", !1 !== t.emitEvent && this.statusChanges.emit(this.status), this._parent && !t.onlySelf && this._parent.markAsPending(t) } }, { key: "disable", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    e = this._parentMarkedDirty(t.onlySelf);
                                this.status = "DISABLED", this.errors = null, this._forEachChild(function(e) { e.disable(Object.assign(Object.assign({}, t), { onlySelf: !0 })) }), this._updateValue(), !1 !== t.emitEvent && (this.valueChanges.emit(this.value), this.statusChanges.emit(this.status)), this._updateAncestors(Object.assign(Object.assign({}, t), { skipPristineCheck: e })), this._onDisabledChange.forEach(function(t) { return t(!0) }) } }, { key: "enable", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    e = this._parentMarkedDirty(t.onlySelf);
                                this.status = "VALID", this._forEachChild(function(e) { e.enable(Object.assign(Object.assign({}, t), { onlySelf: !0 })) }), this.updateValueAndValidity({ onlySelf: !0, emitEvent: t.emitEvent }), this._updateAncestors(Object.assign(Object.assign({}, t), { skipPristineCheck: e })), this._onDisabledChange.forEach(function(t) { return t(!1) }) } }, { key: "_updateAncestors", value: function(t) { this._parent && !t.onlySelf && (this._parent.updateValueAndValidity(t), t.skipPristineCheck || this._parent._updatePristine(), this._parent._updateTouched()) } }, { key: "setParent", value: function(t) { this._parent = t } }, { key: "updateValueAndValidity", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                this._setInitialStatus(), this._updateValue(), this.enabled && (this._cancelExistingSubscription(), this.errors = this._runValidator(), this.status = this._calculateStatus(), "VALID" !== this.status && "PENDING" !== this.status || this._runAsyncValidator(t.emitEvent)), !1 !== t.emitEvent && (this.valueChanges.emit(this.value), this.statusChanges.emit(this.status)), this._parent && !t.onlySelf && this._parent.updateValueAndValidity(t) } }, { key: "_updateTreeValidity", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : { emitEvent: !0 };
                                this._forEachChild(function(e) { return e._updateTreeValidity(t) }), this.updateValueAndValidity({ onlySelf: !0, emitEvent: t.emitEvent }) } }, { key: "_setInitialStatus", value: function() { this.status = this._allControlsDisabled() ? "DISABLED" : "VALID" } }, { key: "_runValidator", value: function() { return this.validator ? this.validator(this) : null } }, { key: "_runAsyncValidator", value: function(t) { var e = this; if (this.asyncValidator) { this.status = "PENDING", this._hasOwnPendingAsyncValidator = !0; var n = S(this.asyncValidator(this));
                                    this._asyncValidationSubscription = n.subscribe(function(n) { e._hasOwnPendingAsyncValidator = !1, e.setErrors(n, { emitEvent: t }) }) } } }, { key: "_cancelExistingSubscription", value: function() { this._asyncValidationSubscription && (this._asyncValidationSubscription.unsubscribe(), this._hasOwnPendingAsyncValidator = !1) } }, { key: "setErrors", value: function(t) { var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this.errors = t, this._updateControlsErrors(!1 !== e.emitEvent) } }, { key: "get", value: function(t) { return function(t, e, n) { if (null == e) return null; if (Array.isArray(e) || (e = e.split(".")), Array.isArray(e) && 0 === e.length) return null; var i = t; return e.forEach(function(t) { i = i instanceof Y ? i.controls.hasOwnProperty(t) ? i.controls[t] : null : i instanceof G && i.at(t) || null }), i }(this, t) } }, { key: "getError", value: function(t, e) { var n = e ? this.get(e) : this; return n && n.errors ? n.errors[t] : null } }, { key: "hasError", value: function(t, e) { return !!this.getError(t, e) } }, { key: "root", get: function() { for (var t = this; t._parent;) t = t._parent; return t } }, { key: "_updateControlsErrors", value: function(t) { this.status = this._calculateStatus(), t && this.statusChanges.emit(this.status), this._parent && this._parent._updateControlsErrors(t) } }, { key: "_initObservables", value: function() { this.valueChanges = new u.vpe, this.statusChanges = new u.vpe } }, { key: "_calculateStatus", value: function() { return this._allControlsDisabled() ? "DISABLED" : this.errors ? "INVALID" : this._hasOwnPendingAsyncValidator || this._anyControlsHaveStatus("PENDING") ? "PENDING" : this._anyControlsHaveStatus("INVALID") ? "INVALID" : "VALID" } }, { key: "_anyControlsHaveStatus", value: function(t) { return this._anyControls(function(e) { return e.status === t }) } }, { key: "_anyControlsDirty", value: function() { return this._anyControls(function(t) { return t.dirty }) } }, { key: "_anyControlsTouched", value: function() { return this._anyControls(function(t) { return t.touched }) } }, { key: "_updatePristine", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                this.pristine = !this._anyControlsDirty(), this._parent && !t.onlySelf && this._parent._updatePristine(t) } }, { key: "_updateTouched", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                this.touched = this._anyControlsTouched(), this._parent && !t.onlySelf && this._parent._updateTouched(t) } }, { key: "_isBoxedValue", value: function(t) { return "object" == typeof t && null !== t && 2 === Object.keys(t).length && "value" in t && "disabled" in t } }, { key: "_registerOnCollectionChange", value: function(t) { this._onCollectionChange = t } }, { key: "_setUpdateStrategy", value: function(t) { J(t) && null != t.updateOn && (this._updateOn = t.updateOn) } }, { key: "_parentMarkedDirty", value: function(t) { return !t && !(!this._parent || !this._parent.dirty) && !this._parent._anyControlsDirty() } }]), t }(),
                    Q = function(t) { i(n, t); var e = o(n);

                        function n() { var t, i = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
                                s = arguments.length > 1 ? arguments[1] : void 0,
                                o = arguments.length > 2 ? arguments[2] : void 0; return l(this, n), (t = e.call(this, L(s), j(o, s)))._onChange = [], t._applyFormState(i), t._setUpdateStrategy(s), t._initObservables(), t.updateValueAndValidity({ onlySelf: !0, emitEvent: !!t.asyncValidator }), t } return c(n, [{ key: "setValue", value: function(t) { var e = this,
                                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this.value = this._pendingValue = t, this._onChange.length && !1 !== n.emitModelToViewChange && this._onChange.forEach(function(t) { return t(e.value, !1 !== n.emitViewToModelChange) }), this.updateValueAndValidity(n) } }, { key: "patchValue", value: function(t) { var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this.setValue(t, e) } }, { key: "reset", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
                                    e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this._applyFormState(t), this.markAsPristine(e), this.markAsUntouched(e), this.setValue(this.value, e), this._pendingChange = !1 } }, { key: "_updateValue", value: function() {} }, { key: "_anyControls", value: function(t) { return !1 } }, { key: "_allControlsDisabled", value: function() { return this.disabled } }, { key: "registerOnChange", value: function(t) { this._onChange.push(t) } }, { key: "_unregisterOnChange", value: function(t) { F(this._onChange, t) } }, { key: "registerOnDisabledChange", value: function(t) { this._onDisabledChange.push(t) } }, { key: "_unregisterOnDisabledChange", value: function(t) { F(this._onDisabledChange, t) } }, { key: "_forEachChild", value: function(t) {} }, { key: "_syncPendingControls", value: function() { return !("submit" !== this.updateOn || (this._pendingDirty && this.markAsDirty(), this._pendingTouched && this.markAsTouched(), !this._pendingChange) || (this.setValue(this._pendingValue, { onlySelf: !0, emitModelToViewChange: !1 }), 0)) } }, { key: "_applyFormState", value: function(t) { this._isBoxedValue(t) ? (this.value = this._pendingValue = t.value, t.disabled ? this.disable({ onlySelf: !0, emitEvent: !1 }) : this.enable({ onlySelf: !0, emitEvent: !1 })) : this.value = this._pendingValue = t } }]), n }(W),
                    Y = function(t) { i(n, t); var e = o(n);

                        function n(t, i, s) { var o; return l(this, n), (o = e.call(this, L(i), j(s, i))).controls = t, o._initObservables(), o._setUpdateStrategy(i), o._setUpControls(), o.updateValueAndValidity({ onlySelf: !0, emitEvent: !!o.asyncValidator }), o } return c(n, [{ key: "registerControl", value: function(t, e) { return this.controls[t] ? this.controls[t] : (this.controls[t] = e, e.setParent(this), e._registerOnCollectionChange(this._onCollectionChange), e) } }, { key: "addControl", value: function(t, e) { var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
                                this.registerControl(t, e), this.updateValueAndValidity({ emitEvent: n.emitEvent }), this._onCollectionChange() } }, { key: "removeControl", value: function(t) { var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this.controls[t] && this.controls[t]._registerOnCollectionChange(function() {}), delete this.controls[t], this.updateValueAndValidity({ emitEvent: e.emitEvent }), this._onCollectionChange() } }, { key: "setControl", value: function(t, e) { var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
                                this.controls[t] && this.controls[t]._registerOnCollectionChange(function() {}), delete this.controls[t], e && this.registerControl(t, e), this.updateValueAndValidity({ emitEvent: n.emitEvent }), this._onCollectionChange() } }, { key: "contains", value: function(t) { return this.controls.hasOwnProperty(t) && this.controls[t].enabled } }, { key: "setValue", value: function(t) { var e = this,
                                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this._checkAllValuesPresent(t), Object.keys(t).forEach(function(i) { e._throwIfControlMissing(i), e.controls[i].setValue(t[i], { onlySelf: !0, emitEvent: n.emitEvent }) }), this.updateValueAndValidity(n) } }, { key: "patchValue", value: function(t) { var e = this,
                                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                null != t && (Object.keys(t).forEach(function(i) { e.controls[i] && e.controls[i].patchValue(t[i], { onlySelf: !0, emitEvent: n.emitEvent }) }), this.updateValueAndValidity(n)) } }, { key: "reset", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this._forEachChild(function(n, i) { n.reset(t[i], { onlySelf: !0, emitEvent: e.emitEvent }) }), this._updatePristine(e), this._updateTouched(e), this.updateValueAndValidity(e) } }, { key: "getRawValue", value: function() { return this._reduceChildren({}, function(t, e, n) { return t[n] = e instanceof Q ? e.value : e.getRawValue(), t }) } }, { key: "_syncPendingControls", value: function() { var t = this._reduceChildren(!1, function(t, e) { return !!e._syncPendingControls() || t }); return t && this.updateValueAndValidity({ onlySelf: !0 }), t } }, { key: "_throwIfControlMissing", value: function(t) { if (!Object.keys(this.controls).length) throw new Error("\n        There are no form controls registered with this group yet. If you're using ngModel,\n        you may want to check next tick (e.g. use setTimeout).\n      "); if (!this.controls[t]) throw new Error("Cannot find form control with name: ".concat(t, ".")) } }, { key: "_forEachChild", value: function(t) { var e = this;
                                Object.keys(this.controls).forEach(function(n) { var i = e.controls[n];
                                    i && t(i, n) }) } }, { key: "_setUpControls", value: function() { var t = this;
                                this._forEachChild(function(e) { e.setParent(t), e._registerOnCollectionChange(t._onCollectionChange) }) } }, { key: "_updateValue", value: function() { this.value = this._reduceValue() } }, { key: "_anyControls", value: function(t) { for (var e = 0, n = Object.keys(this.controls); e < n.length; e++) { var i = n[e],
                                        s = this.controls[i]; if (this.contains(i) && t(s)) return !0 } return !1 } }, { key: "_reduceValue", value: function() { var t = this; return this._reduceChildren({}, function(e, n, i) { return (n.enabled || t.disabled) && (e[i] = n.value), e }) } }, { key: "_reduceChildren", value: function(t, e) { var n = t; return this._forEachChild(function(t, i) { n = e(n, t, i) }), n } }, { key: "_allControlsDisabled", value: function() { for (var t = 0, e = Object.keys(this.controls); t < e.length; t++) { var n = e[t]; if (this.controls[n].enabled) return !1 } return Object.keys(this.controls).length > 0 || this.disabled } }, { key: "_checkAllValuesPresent", value: function(t) { this._forEachChild(function(e, n) { if (void 0 === t[n]) throw new Error("Must supply a value for form control with name: '".concat(n, "'.")) }) } }]), n }(W),
                    G = function(t) { i(s, t); var n = o(s);

                        function s(t, e, i) { var o; return l(this, s), (o = n.call(this, L(e), j(i, e))).controls = t, o._initObservables(), o._setUpdateStrategy(e), o._setUpControls(), o.updateValueAndValidity({ onlySelf: !0, emitEvent: !!o.asyncValidator }), o } return c(s, [{ key: "at", value: function(t) { return this.controls[t] } }, { key: "push", value: function(t) { var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this.controls.push(t), this._registerControl(t), this.updateValueAndValidity({ emitEvent: e.emitEvent }), this._onCollectionChange() } }, { key: "insert", value: function(t, e) { var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
                                this.controls.splice(t, 0, e), this._registerControl(e), this.updateValueAndValidity({ emitEvent: n.emitEvent }) } }, { key: "removeAt", value: function(t) { var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this.controls[t] && this.controls[t]._registerOnCollectionChange(function() {}), this.controls.splice(t, 1), this.updateValueAndValidity({ emitEvent: e.emitEvent }) } }, { key: "setControl", value: function(t, e) { var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
                                this.controls[t] && this.controls[t]._registerOnCollectionChange(function() {}), this.controls.splice(t, 1), e && (this.controls.splice(t, 0, e), this._registerControl(e)), this.updateValueAndValidity({ emitEvent: n.emitEvent }), this._onCollectionChange() } }, { key: "length", get: function() { return this.controls.length } }, { key: "setValue", value: function(t) { var e = this,
                                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this._checkAllValuesPresent(t), t.forEach(function(t, i) { e._throwIfControlMissing(i), e.at(i).setValue(t, { onlySelf: !0, emitEvent: n.emitEvent }) }), this.updateValueAndValidity(n) } }, { key: "patchValue", value: function(t) { var e = this,
                                    n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                null != t && (t.forEach(function(t, i) { e.at(i) && e.at(i).patchValue(t, { onlySelf: !0, emitEvent: n.emitEvent }) }), this.updateValueAndValidity(n)) } }, { key: "reset", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [],
                                    e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                this._forEachChild(function(n, i) { n.reset(t[i], { onlySelf: !0, emitEvent: e.emitEvent }) }), this._updatePristine(e), this._updateTouched(e), this.updateValueAndValidity(e) } }, { key: "getRawValue", value: function() { return this.controls.map(function(t) { return t instanceof Q ? t.value : t.getRawValue() }) } }, { key: "clear", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                this.controls.length < 1 || (this._forEachChild(function(t) { return t._registerOnCollectionChange(function() {}) }), this.controls.splice(0), this.updateValueAndValidity({ emitEvent: t.emitEvent })) } }, { key: "_syncPendingControls", value: function() { var t = this.controls.reduce(function(t, e) { return !!e._syncPendingControls() || t }, !1); return t && this.updateValueAndValidity({ onlySelf: !0 }), t } }, { key: "_throwIfControlMissing", value: function(t) { if (!this.controls.length) throw new Error("\n        There are no form controls registered with this array yet. If you're using ngModel,\n        you may want to check next tick (e.g. use setTimeout).\n      "); if (!this.at(t)) throw new Error("Cannot find form control at index ".concat(t)) } }, { key: "_forEachChild", value: function(t) { this.controls.forEach(function(e, n) { t(e, n) }) } }, { key: "_updateValue", value: function() { var t = this;
                                this.value = this.controls.filter(function(e) { return e.enabled || t.disabled }).map(function(t) { return t.value }) } }, { key: "_anyControls", value: function(t) { return this.controls.some(function(e) { return e.enabled && t(e) }) } }, { key: "_setUpControls", value: function() { var t = this;
                                this._forEachChild(function(e) { return t._registerControl(e) }) } }, { key: "_checkAllValuesPresent", value: function(t) { this._forEachChild(function(e, n) { if (void 0 === t[n]) throw new Error("Must supply a value for form control at index: ".concat(n, ".")) }) } }, { key: "_allControlsDisabled", value: function() { var t, n = function(t, n) { var i = "undefined" != typeof Symbol && t[Symbol.iterator] || t["@@iterator"]; if (!i) { if (Array.isArray(t) || (i = e(t)) || n && t && "number" == typeof t.length) { i && (t = i); var s = 0,
                                                o = function() {}; return { s: o, n: function() { return s >= t.length ? { done: !0 } : { done: !1, value: t[s++] } }, e: function(t) { throw t }, f: o } } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.") } var r, a = !0,
                                        l = !1; return { s: function() { i = i.call(t) }, n: function() { var t = i.next(); return a = t.done, t }, e: function(t) { l = !0, r = t }, f: function() { try { a || null == i.return || i.return() } finally { if (l) throw r } } } }(this.controls); try { for (n.s(); !(t = n.n()).done;) { if (t.value.enabled) return !1 } } catch (i) { n.e(i) } finally { n.f() } return this.controls.length > 0 || this.disabled } }, { key: "_registerControl", value: function(t) { t.setParent(this), t._registerOnCollectionChange(this._onCollectionChange) } }]), s }(W),
                    H = { provide: E, useExisting: (0, u.Gpc)(function() { return X }) },
                    z = Promise.resolve(null),
                    X = function() { var e = function(e) { i(s, e); var n = o(s);

                            function s(t, e, i, o) { var r; return l(this, s), (r = n.call(this)).control = new Q, r._registered = !1, r.update = new u.vpe, r._parent = t, r._setValidators(e), r._setAsyncValidators(i), r.valueAccessor = function(t, e) { return e ? (Array.isArray(e), e.forEach(function(t) { t.constructor === A ? n = t : Object.getPrototypeOf(t.constructor) === m ? i = t : s = t }), s || i || n || null) : null; var n, i, s }(0, o), r } return c(s, [{ key: "ngOnChanges", value: function(t) { this._checkForErrors(), this._registered || this._setUpControl(), "isDisabled" in t && this._updateDisabled(t),
                                        function(t, e) { if (!t.hasOwnProperty("model")) return !1; var n = t.model; return !!n.isFirstChange() || !Object.is(e, n.currentValue) }(t, this.viewModel) && (this._updateValue(this.model), this.viewModel = this.model) } }, { key: "ngOnDestroy", value: function() { this.formDirective && this.formDirective.removeControl(this) } }, { key: "path", get: function() { return this._parent ? [].concat(t(this._parent.path), [this.name]) : [this.name] } }, { key: "formDirective", get: function() { return this._parent ? this._parent.formDirective : null } }, { key: "viewToModelUpdate", value: function(t) { this.viewModel = t, this.update.emit(t) } }, { key: "_setUpControl", value: function() { this._setUpdateStrategy(), this._isStandalone() ? this._setUpStandalone() : this.formDirective.addControl(this), this._registered = !0 } }, { key: "_setUpdateStrategy", value: function() { this.options && null != this.options.updateOn && (this.control._updateOn = this.options.updateOn) } }, { key: "_isStandalone", value: function() { return !this._parent || !(!this.options || !this.options.standalone) } }, { key: "_setUpStandalone", value: function() { var t, e;
                                    (function(t, e) { var n = function(t) { return t._rawValidators }(t);
                                        null !== e.validator ? t.setValidators(V(n, e.validator)) : "function" == typeof n && t.setValidators([n]); var i = function(t) { return t._rawAsyncValidators }(t);
                                        null !== e.asyncValidator ? t.setAsyncValidators(V(i, e.asyncValidator)) : "function" == typeof i && t.setAsyncValidators([i]); var s = function() { return t.updateValueAndValidity() };
                                        U(e._rawValidators, s), U(e._rawAsyncValidators, s) })(t = this.control, e = this), e.valueAccessor.writeValue(t.value),
                                        function(t, e) { e.valueAccessor.registerOnChange(function(n) { t._pendingValue = n, t._pendingChange = !0, t._pendingDirty = !0, "change" === t.updateOn && D(t, e) }) }(t, e),
                                        function(t, e) { var n = function(t, n) { e.valueAccessor.writeValue(t), n && e.viewToModelUpdate(t) };
                                            t.registerOnChange(n), e._registerOnDestroy(function() { t._unregisterOnChange(n) }) }(t, e),
                                        function(t, e) { e.valueAccessor.registerOnTouched(function() { t._pendingTouched = !0, "blur" === t.updateOn && t._pendingChange && D(t, e), "submit" !== t.updateOn && t.markAsTouched() }) }(t, e),
                                        function(t, e) { if (e.valueAccessor.setDisabledState) { var n = function(t) { e.valueAccessor.setDisabledState(t) };
                                                t.registerOnDisabledChange(n), e._registerOnDestroy(function() { t._unregisterOnDisabledChange(n) }) } }(t, e), this.control.updateValueAndValidity({ emitEvent: !1 }) } }, { key: "_checkForErrors", value: function() { this._isStandalone() || this._checkParentType(), this._checkName() } }, { key: "_checkParentType", value: function() {} }, { key: "_checkName", value: function() { this.options && this.options.name && (this.name = this.options.name), this._isStandalone() } }, { key: "_updateValue", value: function(t) { var e = this;
                                    z.then(function() { e.control.setValue(t, { emitViewToModelChange: !1 }) }) } }, { key: "_updateDisabled", value: function(t) { var e = this,
                                        n = t.isDisabled.currentValue,
                                        i = "" === n || n && "false" !== n;
                                    z.then(function() { i && !e.control.disabled ? e.control.disable() : !i && e.control.disabled && e.control.enable() }) } }]), s }(E); return e.\u0275fac = function(t) { return new(t || e)(u.Y36(M, 9), u.Y36(Z, 10), u.Y36(C, 10), u.Y36(_, 10)) }, e.\u0275dir = u.lG2({ type: e, selectors: [
                                ["", "ngModel", "", 3, "formControlName", "", 3, "formControl", ""]
                            ], inputs: { name: "name", isDisabled: ["disabled", "isDisabled"], model: ["ngModel", "model"], options: ["ngModelOptions", "options"] }, outputs: { update: "ngModelChange" }, exportAs: ["ngModel"], features: [u._Bn([H]), u.qOj, u.TTD] }), e }(),
                    K = function() { var t = c(function t() { l(this, t) }); return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275mod = u.oAB({ type: t }), t.\u0275inj = u.cJS({}), t }(),
                    $ = function() { var t = c(function t() { l(this, t) }); return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275mod = u.oAB({ type: t }), t.\u0275inj = u.cJS({ imports: [
                                [K]
                            ] }), t }(),
                    tt = function() { var t = c(function t() { l(this, t) }); return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275mod = u.oAB({ type: t }), t.\u0275inj = u.cJS({ imports: [$] }), t }(),
                    et = r(7957),
                    nt = r(5384),
                    it = r(3484),
                    st = r(2340),
                    ot = r(1841),
                    rt = r(3342),
                    at = r(5304),
                    lt = r(205),
                    ut = function() { var t = function() {
                            function t(e) { l(this, t), this.http = e } return c(t, [{ key: "makeGetRequest", value: function(t) { return this.http.get(t).pipe((0, rt.b)(function(t) {}), (0, at.K)(function(t) { return (0, lt._)(t) })) } }, { key: "makePostRequest", value: function(t, e) { return this.http.post(t, e).pipe((0, rt.b)(function(t) {}), (0, at.K)(function(t) { return (0, lt._)(t) })) } }]), t }(); return t.\u0275fac = function(e) { return new(e || t)(u.LFG(ot.eN)) }, t.\u0275prov = u.Yz7({ token: t, factory: t.\u0275fac, providedIn: "root" }), t }(),
                    ct = function() { var t = function() {
                            function t(e, n) { l(this, t), this.http = e, this.rest = n } return c(t, [{ key: "generateOTP", value: function(t) { return this.rest.makePostRequest("".concat(st.N.apiUrl, "/signin/OtpSignIn"), { CountryPhoneCode: "91", PhoneNumber: t, Source: "Otp" }) } }, { key: "signIn", value: function(t, e) { return this.rest.makePostRequest("".concat(st.N.apiUrl, "/signin"), { PhoneNumber: t, OTP: e, Source: "Otp", DeviceType: "iPhone" }) } }]), t }(); return t.\u0275fac = function(e) { return new(e || t)(u.LFG(ot.eN), u.LFG(ut)) }, t.\u0275prov = u.Yz7({ token: t, factory: t.\u0275fac, providedIn: "root" }), t }();

                function dt(t, e) { if (1 & t) { var n = u.EpF();
                        u.TgZ(0, "div", 3), u.TgZ(1, "h6"), u._uU(2, "Enter Mobile Number "), u.TgZ(3, "span"), u._uU(4, "*"), u.qZA(), u.qZA(), u.TgZ(5, "div", 4), u.TgZ(6, "div", 5), u._UZ(7, "input", 6), u.qZA(), u.TgZ(8, "div", 7), u.TgZ(9, "input", 8), u.NdJ("ngModelChange", function(t) { return u.CHM(n), u.oxw().mobileNo = t }), u.qZA(), u.qZA(), u.qZA(), u.TgZ(10, "div", 9), u.TgZ(11, "button", 10), u.NdJ("click", function() { return u.CHM(n), u.oxw().generateOTP() }), u._uU(12, "Send OTP"), u.qZA(), u.TgZ(13, "button", 11), u._uU(14, "Sign Up"), u.qZA(), u.qZA(), u.qZA() } if (2 & t) { var i = u.oxw();
                        u.xp6(9), u.Q6J("ngModel", i.mobileNo) } }

                function ht(t, e) { if (1 & t) { var n = u.EpF();
                        u.TgZ(0, "div", 12), u.TgZ(1, "h6"), u._uU(2, "Enter OTP"), u.qZA(), u.TgZ(3, "div", 13), u.TgZ(4, "div", 14), u._UZ(5, "i", 15), u.TgZ(6, "input", 16), u.NdJ("ngModelChange", function(t) { return u.CHM(n), u.oxw().otp = t }), u.qZA(), u.qZA(), u.qZA(), u.TgZ(7, "div", 9), u.TgZ(8, "button", 10), u.NdJ("click", function() { return u.CHM(n), u.oxw().signIn() }), u._uU(9, "Login"), u.qZA(), u.TgZ(10, "button", 11), u._uU(11, "Sign Up"), u.qZA(), u.qZA(), u.qZA() } if (2 & t) { var i = u.oxw();
                        u.xp6(6), u.Q6J("ngModel", i.otp) } } var ft = function() { var t = function() {
                            function t(e, n) { l(this, t), this.authService = e, this._snackBar = n, this.closeMenuEvent = new u.vpe, this.formType = "login", this.mobileNo = "", this.otp = "" } return c(t, [{ key: "ngOnInit", value: function() {} }, { key: "clearAll", value: function(t) { this.formType = "login", this.mobileNo = "", this.otp = "", t && this.closeMenuEvent.emit("closeMenu") } }, { key: "generateOTP", value: function() { var t = this;
                                    this.mobileNo ? this.authService.generateOTP(this.mobileNo).subscribe(function(e) { var n;
                                        t.formType = "otp", t._snackBar.open("Your OTP: " + (null === (n = null == e ? void 0 : e.Data) || void 0 === n ? void 0 : n.Token), "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }) }, function(e) { t._snackBar.open("Sorry, Something went wrong. Please try again after some time!", "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }), t.clearAll(!0) }) : this._snackBar.open("Mobile number should not be empty!", "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }) } }, { key: "signIn", value: function() { var t = this;
                                    this.otp ? this.authService.signIn(this.mobileNo, this.otp).subscribe(function(e) { "200" == e.ResponseCode ? (localStorage.setItem("userProfile", JSON.stringify(e)), t._snackBar.open("Login Successful.", "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 })) : t._snackBar.open(e.Message, "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }), t.clearAll(!0) }, function(e) { t._snackBar.open("Sorry, Something went wrong. Please try again after some time!", "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }), t.clearAll(!0) }) : this._snackBar.open("OTP should not be empty!", "", { horizontalPosition: "end", verticalPosition: "bottom", duration: 2e3 }) } }, { key: "ngOnDestroy", value: function() { this.clearAll(!0) } }]), t }(); return t.\u0275fac = function(e) { return new(e || t)(u.Y36(ct), u.Y36(it.ux)) }, t.\u0275cmp = u.Xpm({ type: t, selectors: [
                                ["app-login"]
                            ], inputs: { formType: "formType" }, outputs: { closeMenuEvent: "closeMenuEvent" }, decls: 3, vars: 2, consts: [
                                [1, "dialog-container", 3, "click", "keydown"],
                                ["class", "login-section", 4, "ngIf"],
                                ["class", "otp-section", 4, "ngIf"],
                                [1, "login-section"],
                                [1, "login-form"],
                                [1, "otp-sec"],
                                ["type", "text", "value", "+91 (IN)", "readonly", "", 1, "line-input-field"],
                                [1, "mobile-sec"],
                                ["type", "text", "placeholder", "Ex. 9685745263", 1, "line-input-field", 3, "ngModel", "ngModelChange"],
                                [1, "buttons"],
                                [1, "btn-normal", 3, "click"],
                                [1, "btn-default"],
                                [1, "otp-section"],
                                [1, "otp-form"],
                                [1, "input-group"],
                                [1, "fa", "fa-pen"],
                                ["type", "text", 1, "line-input-field", 3, "ngModel", "ngModelChange"]
                            ], template: function(t, e) { 1 & t && (u.TgZ(0, "div", 0), u.NdJ("click", function(t) { return t.stopPropagation() })("keydown", function(t) { return t.stopPropagation() }), u.YNc(1, dt, 15, 1, "div", 1), u.YNc(2, ht, 12, 1, "div", 2), u.qZA()), 2 & t && (u.xp6(1), u.Q6J("ngIf", "login" == e.formType), u.xp6(1), u.Q6J("ngIf", "otp" == e.formType)) }, directives: [a.O5, A, N, X], styles: [".mat-menu-content{padding:0!important} .mat-menu-panel{max-width:none!important}.dialog-container[_ngcontent-%COMP%]{display:flex;flex-direction:column;justify-content:center}.dialog-container[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{font-size:medium;font-weight:500}.dialog-container[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{color:#eb5d72}.dialog-container[_ngcontent-%COMP%]   .login-form[_ngcontent-%COMP%]{margin-top:30px;width:100%;display:flex;align-items:center}.dialog-container[_ngcontent-%COMP%]   .login-form[_ngcontent-%COMP%]   .otp-sec[_ngcontent-%COMP%]{width:25%;margin-right:10px}.dialog-container[_ngcontent-%COMP%]   .login-form[_ngcontent-%COMP%]   .mobile-sec[_ngcontent-%COMP%]{width:70%;margin-left:10px}.dialog-container[_ngcontent-%COMP%]   .buttons[_ngcontent-%COMP%]{margin-top:40px;width:100%;display:flex;align-items:center;justify-content:space-around}"] }), t }(),
                    gt = ["menuTrigger"],
                    vt = ["loginPage"];

                function pt(t, e) { 1 & t && (u.TgZ(0, "li", 10), u.TgZ(1, "a", 11), u._uU(2, "My Prediction"), u.qZA(), u.qZA()) }

                function yt(t, e) { 1 & t && (u.TgZ(0, "li", 10), u.TgZ(1, "a", 11), u._uU(2, "Results"), u.qZA(), u.qZA()) }

                function mt(t, e) { 1 & t && (u.TgZ(0, "li", 10), u.TgZ(1, "a", 11), u._uU(2, "Winner's Name"), u.qZA(), u.qZA()) }

                function _t(t, e) { 1 & t && (u.TgZ(0, "li", 10), u.TgZ(1, "a", 11), u._uU(2, "Our Prices"), u.qZA(), u.qZA()) }

                function kt(t, e) { if (1 & t && (u.TgZ(0, "li", 20), u._UZ(1, "img", 21), u.qZA()), 2 & t) { u.oxw(); var n = u.MAs(18);
                        u.Q6J("matMenuTriggerFor", n) } }

                function bt(t, e) { if (1 & t) { var n = u.EpF();
                        u.TgZ(0, "li", 22, 23), u.NdJ("click", function() { return u.CHM(n), u.oxw().openLoginDialog() }), u.TgZ(2, "a", 24), u._uU(3, "Login"), u.qZA(), u.qZA() } if (2 & t) { u.oxw(); var i = u.MAs(25);
                        u.Q6J("matMenuTriggerFor", i) } } var At, Zt = function() { var t = function() {
                            function t() { l(this, t), this.isCollapsed = !0, this.isLoggedIn = !1 } return c(t, [{ key: "ngOnInit", value: function() { this.checkLoggedInOrNot() } }, { key: "closeLoginDialog", value: function() { this.trigger.closeMenu(), this.checkLoggedInOrNot() } }, { key: "openLoginDialog", value: function() { this.trigger.openMenu(), this.loginPage.clearAll(!1) } }, { key: "checkLoggedInOrNot", value: function() { this.isLoggedIn = !!localStorage.getItem("userProfile") } }, { key: "logout", value: function() { localStorage.setItem("userProfile", ""), this.checkLoggedInOrNot() } }]), t }(); return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275cmp = u.Xpm({ type: t, selectors: [
                                ["app-header"]
                            ], viewQuery: function(t, e) { var n;
                                (1 & t && (u.Gf(gt, 5), u.Gf(vt, 5)), 2 & t) && (u.iGM(n = u.CRH()) && (e.trigger = n.first), u.iGM(n = u.CRH()) && (e.loginPage = n.first)) }, decls: 28, vars: 8, consts: [
                                [2, "position", "sticky", "top", "0", "left", "0", "z-index", "999"],
                                [1, "navbar", "navbar-expand-lg", "navbar-dark", "bg-primary", "mb-3"],
                                [1, "container-fluid"],
                                ["routerLink", "/", 1, "navbar-brand"],
                                ["src", "assets/images/Goalak-logo-w.png", "alt", "Goalak"],
                                ["type", "button", "data-bs-toggle", "collapse", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler", 3, "click"],
                                [1, "navbar-toggler-icon"],
                                [1, "collapse", "navbar-collapse"],
                                [1, "navbar-nav", "ms-auto", "mb-2", "mb-lg-0"],
                                ["class", "nav-item", 4, "ngIf"],
                                [1, "nav-item"],
                                ["href", "#", 1, "nav-link"],
                                ["class", "nav-item", 3, "matMenuTriggerFor", 4, "ngIf"],
                                ["profileMenu", "matMenu"],
                                ["mat-menu-item", ""],
                                ["mat-menu-item", "", 3, "click"],
                                ["class", "nav-item", 3, "matMenuTriggerFor", "click", 4, "ngIf"],
                                ["menu", "matMenu"],
                                [3, "formType", "closeMenuEvent"],
                                ["loginPage", ""],
                                [1, "nav-item", 3, "matMenuTriggerFor"],
                                ["src", "assets/images/profile.png", "alt", "", 1, "profile"],
                                [1, "nav-item", 3, "matMenuTriggerFor", "click"],
                                ["menuTrigger", "matMenuTrigger"],
                                [1, "nav-link"]
                            ], template: function(t, e) { 1 & t && (u.TgZ(0, "header", 0), u.TgZ(1, "nav", 1), u.TgZ(2, "div", 2), u.TgZ(3, "a", 3), u._UZ(4, "img", 4), u.qZA(), u.TgZ(5, "button", 5), u.NdJ("click", function() { return e.isCollapsed = !e.isCollapsed }), u._UZ(6, "span", 6), u.qZA(), u.TgZ(7, "div", 7), u.TgZ(8, "ul", 8), u.YNc(9, pt, 3, 0, "li", 9), u.YNc(10, yt, 3, 0, "li", 9), u.YNc(11, mt, 3, 0, "li", 9), u.YNc(12, _t, 3, 0, "li", 9), u.TgZ(13, "li", 10), u.TgZ(14, "a", 11), u._uU(15, "EN"), u.qZA(), u.qZA(), u.YNc(16, kt, 2, 1, "li", 12), u.TgZ(17, "mat-menu", null, 13), u.TgZ(19, "button", 14), u._uU(20, "My Account"), u.qZA(), u.TgZ(21, "button", 15), u.NdJ("click", function() { return e.logout() }), u._uU(22, "Log Out"), u.qZA(), u.qZA(), u.YNc(23, bt, 4, 1, "li", 16), u.TgZ(24, "mat-menu", null, 17), u.TgZ(26, "app-login", 18, 19), u.NdJ("closeMenuEvent", function() { return e.closeLoginDialog() }), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.qZA()), 2 & t && (u.xp6(5), u.uIk("aria-expanded", !e.isCollapsed), u.xp6(4), u.Q6J("ngIf", e.isLoggedIn), u.xp6(1), u.Q6J("ngIf", e.isLoggedIn), u.xp6(1), u.Q6J("ngIf", e.isLoggedIn), u.xp6(1), u.Q6J("ngIf", e.isLoggedIn), u.xp6(4), u.Q6J("ngIf", e.isLoggedIn), u.xp6(7), u.Q6J("ngIf", !e.isLoggedIn), u.xp6(3), u.Q6J("formType", "login")) }, directives: [et.yS, a.O5, nt.VK, nt.OP, ft, nt.p6], styles: [".nav-item[_ngcontent-%COMP%]{padding:3px 10px;cursor:pointer}.nav-item[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover{background-color:#ffd710;font-weight:500;color:#000!important;border-radius:2px}.nav-item[_ngcontent-%COMP%]   .profile[_ngcontent-%COMP%]{width:40px;-o-object-fit:cover;object-fit:cover}"] }), t }(),
                    Ct = function() { var t = function() {
                            function t() { l(this, t) } return c(t, [{ key: "ngOnInit", value: function() {} }]), t }(); return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275cmp = u.Xpm({ type: t, selectors: [
                                ["app-footer"]
                            ], decls: 65, vars: 0, consts: [
                                [1, "space-top-80"],
                                [1, "container-fluid", "bottom"],
                                [1, "container", "pt-5", "pb-5"],
                                [1, "row"],
                                [1, "col-lg-3"],
                                ["href", "#", "target", "_self"],
                                [1, "social", "mb-4"],
                                ["href", "#"],
                                ["src", "assets/images/whatsapp.png", "alt", "goalak", 1, "mr-2"],
                                ["src", "assets/images/facebook.png", "alt", "goalak", 1, "mr-2"],
                                ["src", "assets/images/insta.png", "alt", "goalak", 1, "mr-2"],
                                ["src", "assets/images/twitter.png", "alt", "goalak", 1, "mr-2"],
                                ["src", "assets/images/app.png", "alt", "goalak", 1, "app"],
                                [1, "footer", "text-center"]
                            ], template: function(t, e) { 1 & t && (u.TgZ(0, "footer", 0), u.TgZ(1, "div", 1), u.TgZ(2, "div", 2), u.TgZ(3, "div", 3), u.TgZ(4, "div", 4), u.TgZ(5, "h3"), u._uU(6, "COMPANY"), u.qZA(), u.TgZ(7, "ul"), u.TgZ(8, "li"), u.TgZ(9, "a", 5), u._uU(10, "About Us"), u.qZA(), u.qZA(), u.TgZ(11, "li"), u.TgZ(12, "a", 5), u._uU(13, "Blog"), u.qZA(), u.qZA(), u.TgZ(14, "li"), u.TgZ(15, "a", 5), u._uU(16, "Feedback"), u.qZA(), u.qZA(), u.TgZ(17, "li"), u.TgZ(18, "a", 5), u._uU(19, "Sitemap"), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.TgZ(20, "div", 4), u.TgZ(21, "h3"), u._uU(22, "ACCOUNT"), u.qZA(), u.TgZ(23, "ul"), u.TgZ(24, "li"), u.TgZ(25, "a", 5), u._uU(26, "Login"), u.qZA(), u.qZA(), u.TgZ(27, "li"), u.TgZ(28, "a", 5), u._uU(29, "Register"), u.qZA(), u.qZA(), u.TgZ(30, "li"), u.TgZ(31, "a", 5), u._uU(32, "Create Listing"), u.qZA(), u.qZA(), u.TgZ(33, "li"), u.TgZ(34, "a", 5), u._uU(35, "How It Works"), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.TgZ(36, "div", 4), u.TgZ(37, "h3"), u._uU(38, "HELP & CONTACT"), u.qZA(), u.TgZ(39, "ul"), u.TgZ(40, "li"), u.TgZ(41, "a", 5), u._uU(42, "Contact Us"), u.qZA(), u.qZA(), u.TgZ(43, "li"), u.TgZ(44, "a", 5), u._uU(45, "Frequently Asked Questions"), u.qZA(), u.qZA(), u.TgZ(46, "li"), u.TgZ(47, "a", 5), u._uU(48, "Terms & Conditions"), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.TgZ(49, "div", 4), u.TgZ(50, "h3"), u._uU(51, "FOLLOW US"), u.qZA(), u.TgZ(52, "div", 6), u.TgZ(53, "a", 7), u._UZ(54, "img", 8), u.qZA(), u.TgZ(55, "a", 7), u._UZ(56, "img", 9), u.qZA(), u.TgZ(57, "a", 7), u._UZ(58, "img", 10), u.qZA(), u.TgZ(59, "a", 7), u._UZ(60, "img", 11), u.qZA(), u.qZA(), u._UZ(61, "img", 12), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.TgZ(62, "div", 13), u.TgZ(63, "p"), u._uU(64, "Copyright\xa9 2022 Goalak. All rights reserved."), u.qZA(), u.qZA(), u.qZA()) }, styles: [""] }), t }(),
                    xt = function() { var t = function() {
                            function t() { l(this, t) } return c(t, [{ key: "ngOnInit", value: function() {} }]), t }(); return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275cmp = u.Xpm({ type: t, selectors: [
                                ["app-partials"]
                            ], decls: 3, vars: 0, template: function(t, e) { 1 & t && (u._UZ(0, "app-header"), u._UZ(1, "router-outlet"), u._UZ(2, "app-footer")) }, directives: [Zt, et.lC, Ct], styles: [""] }), t }(),
                    St = function() { var t = function() {
                            function t(e, n) { l(this, t), this.http = e, this.rest = n } return c(t, [{ key: "getAllNews", value: function() { return this.rest.makeGetRequest("".concat(st.N.apiUrl, "/football/sports/getNews")) } }]), t }(); return t.\u0275fac = function(e) { return new(e || t)(u.LFG(ot.eN), u.LFG(ut)) }, t.\u0275prov = u.Yz7({ token: t, factory: t.\u0275fac, providedIn: "root" }), t }(),
                    Tt = function() { var t = function() {
                            function t(e, n) { l(this, t), this.http = e, this.rest = n } return c(t, [{ key: "getAllSeries", value: function() { return this.rest.makePostRequest("".concat(st.N.apiUrl, "/football/sports/getSeries"), { Params: "Status,TotalMatches,SeriesFlag,SeriesID,hasStandings,seriesGUID", StatusID: [1, 2], OrderBy: "Status" }) } }, { key: "getRoundsBySeries", value: function(t) { return this.rest.makePostRequest("".concat(st.N.apiUrl, "/football/series/getRounds"), { SeriesGUID: t, Params: "TotalLevels,RoundID,StatusID,SeriesType,RoundFormat,SeriesIDLive,SeriesStartDate,SeriesEndDate,SeriesStartDateUTC,SeriesEndDateUTC,TotalMatches,SeriesMatchStartDate,Status", SessionKey: "08925584-3927-4517-30cf-c4a910cc4e4c" }) } }, { key: "getMatchesByRounds", value: function(t, e) { return this.rest.makePostRequest("".concat(st.N.apiUrl, "/football/sports/getMatches"), { Params: "isPredicted,TeamNameShortVisitor,TeamNameShortLocal,Status,TeamFlagLocal,TeamFlagVisitor,MatchStartDateTimeUTC,WinPoints,PredictionData,Statistics,MatchScoreDetails,SeriesName,SeriesID,StatusID,SeriesGUID", SeriesGUID: t, SessionKey: "08925584-3927-4517-30cf-c4a910cc4e4c", RoundID: e, Status: "Pending" }) } }]), t }(); return t.\u0275fac = function(e) { return new(e || t)(u.LFG(ot.eN), u.LFG(ut)) }, t.\u0275prov = u.Yz7({ token: t, factory: t.\u0275fac, providedIn: "root" }), t }(),
                    Ot = "undefined" != typeof window && window || {};

                function wt() { if (void 0 === Ot.document) return "bs4"; var t = Ot.document.createElement("span");
                    t.innerText = "testing bs version", t.classList.add("d-none"), t.classList.add("pl-1"), Ot.document.head.appendChild(t); var e = t.getBoundingClientRect(),
                        n = Ot.getComputedStyle(t).paddingLeft; return !e || e && 0 !== e.top ? (Ot.document.head.removeChild(t), "bs3") : n && parseFloat(n) ? (Ot.document.head.removeChild(t), "bs4") : (Ot.document.head.removeChild(t), "bs5") }

                function Pt() { return void 0 === Ot || (void 0 === Ot.__theme ? (At || (At = wt()), "bs3" === At) : "bs3" === Ot.__theme) }

                function It() { return !Pt() && (At || (At = wt()), "bs4" === At) } var Vt = function() {
                    function t() { l(this, t), this.length = 0, this.asArray = [] } return c(t, [{ key: "get", value: function(t) { var e; if (!(0 === this.length || t < 0 || t >= this.length)) { for (var n = this.head, i = 0; i < t; i++) { var s;
                                    n = null === (s = n) || void 0 === s ? void 0 : s.next } return null === (e = n) || void 0 === e ? void 0 : e.value } } }, { key: "add", value: function(t) { var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.length; if (e < 0 || e > this.length) throw new Error("Position is out of the list"); var n = { value: t, next: void 0, previous: void 0 }; if (0 === this.length) this.head = n, this.tail = n, this.current = n;
                            else if (0 === e && this.head) n.next = this.head, this.head.previous = n, this.head = n;
                            else if (e === this.length && this.tail) this.tail.next = n, n.previous = this.tail, this.tail = n;
                            else { var i = this.getNode(e - 1),
                                    s = null == i ? void 0 : i.next;
                                i && s && (i.next = n, s.previous = n, n.previous = i, n.next = s) }
                            this.length++, this.createInternalArrayRepresentation() } }, { key: "remove", value: function() { var t, e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0; if (0 === this.length || e < 0 || e >= this.length) throw new Error("Position is out of the list"); if (0 === e && this.head) this.head = this.head.next, this.head ? this.head.previous = void 0 : this.tail = void 0;
                            else if (e === this.length - 1 && null !== (t = this.tail) && void 0 !== t && t.previous) this.tail = this.tail.previous, this.tail.next = void 0;
                            else { var n = this.getNode(e);
                                (null == n ? void 0 : n.next) && n.previous && (n.next.previous = n.previous, n.previous.next = n.next) }
                            this.length--, this.createInternalArrayRepresentation() } }, { key: "set", value: function(t, e) { if (0 === this.length || t < 0 || t >= this.length) throw new Error("Position is out of the list"); var n = this.getNode(t);
                            n && (n.value = e, this.createInternalArrayRepresentation()) } }, { key: "toArray", value: function() { return this.asArray } }, { key: "findAll", value: function(t) { var e = this.head,
                                n = []; if (!e) return n; for (var i = 0; i < this.length; i++) { if (!e) return n;
                                t(e.value, i) && n.push({ index: i, value: e.value }), e = e.next } return n } }, { key: "push", value: function() { for (var t = this, e = arguments.length, n = new Array(e), i = 0; i < e; i++) n[i] = arguments[i]; return n.forEach(function(e) { t.add(e) }), this.length } }, { key: "pop", value: function() { if (0 !== this.length) { var t = this.tail; return this.remove(this.length - 1), null == t ? void 0 : t.value } } }, { key: "unshift", value: function() { for (var t = this, e = arguments.length, n = new Array(e), i = 0; i < e; i++) n[i] = arguments[i]; return n.reverse(), n.forEach(function(e) { t.add(e, 0) }), this.length } }, { key: "shift", value: function() { var t; if (0 !== this.length) { var e = null === (t = this.head) || void 0 === t ? void 0 : t.value; return this.remove(), e } } }, { key: "forEach", value: function(t) { for (var e = this.head, n = 0; n < this.length; n++) { if (!e) return;
                                t(e.value, n), e = e.next } } }, { key: "indexOf", value: function(t) { for (var e = this.head, n = -1, i = 0; i < this.length; i++) { if (!e) return n; if (e.value === t) { n = i; break }
                                e = e.next } return n } }, { key: "some", value: function(t) { for (var e = this.head, n = !1; e && !n;) { if (t(e.value)) { n = !0; break }
                                e = e.next } return n } }, { key: "every", value: function(t) { for (var e = this.head, n = !0; e && n;) t(e.value) || (n = !1), e = e.next; return n } }, { key: "toString", value: function() { return "[Linked List]" } }, { key: "find", value: function(t) { for (var e = this.head, n = 0; n < this.length; n++) { if (!e) return; if (t(e.value, n)) return e.value;
                                e = e.next } } }, { key: "findIndex", value: function(t) { for (var e = this.head, n = 0; n < this.length; n++) { if (!e) return -1; if (t(e.value, n)) return n;
                                e = e.next } return -1 } }, { key: "getNode", value: function(t) { if (0 === this.length || t < 0 || t >= this.length) throw new Error("Position is out of the list"); for (var e = this.head, n = 0; n < t; n++) { var i;
                                e = null === (i = e) || void 0 === i ? void 0 : i.next } return e } }, { key: "createInternalArrayRepresentation", value: function() { for (var t = [], e = this.head; e;) t.push(e.value), e = e.next;
                            this.asArray = t } }]), t }();

                function qt(t, e) { if (1 & t) { var n = u.EpF();
                        u.TgZ(0, "li", 7), u.NdJ("click", function() { var t = u.CHM(n).index; return u.oxw(2).selectSlide(t) }), u.qZA() }
                    2 & t && u.ekj("active", !0 === e.$implicit.active) }

                function Mt(t, e) { if (1 & t && (u.ynx(0), u.TgZ(1, "ol", 5), u.YNc(2, qt, 1, 2, "li", 6), u.qZA(), u.BQk()), 2 & t) { var n = u.oxw();
                        u.xp6(2), u.Q6J("ngForOf", n.indicatorsSlides()) } }

                function Et(t, e) { if (1 & t) { var n = u.EpF();
                        u.TgZ(0, "button", 9), u.NdJ("click", function() { var t = u.CHM(n).index; return u.oxw(2).selectSlide(t) }), u.qZA() } if (2 & t) { var i = e.$implicit,
                            s = e.index,
                            o = u.oxw(2);
                        u.ekj("active", !0 === i.active), u.uIk("data-bs-target", "#" + o.currentId)("data-bs-slide-to", s) } }

                function Nt(t, e) { if (1 & t && (u.ynx(0), u.TgZ(1, "div", 5), u.YNc(2, Et, 1, 4, "button", 8), u.qZA(), u.BQk()), 2 & t) { var n = u.oxw();
                        u.xp6(2), u.Q6J("ngForOf", n.indicatorsSlides()) } }

                function Ut(t, e) { 1 & t && (u.TgZ(0, "span", 13), u._uU(1, "Previous"), u.qZA()) }

                function Dt(t, e) { if (1 & t) { var n = u.EpF();
                        u.TgZ(0, "a", 10), u.NdJ("click", function() { return u.CHM(n), u.oxw().previousSlide() }), u._UZ(1, "span", 11), u.YNc(2, Ut, 2, 0, "span", 12), u.qZA() } if (2 & t) { var i = u.oxw();
                        u.ekj("disabled", i.checkDisabledClass("prev")), u.uIk("data-bs-target", "#" + i.currentId), u.xp6(2), u.Q6J("ngIf", i.isBs4) } }

                function Ft(t, e) { if (1 & t) { var n = u.EpF();
                        u.TgZ(0, "a", 14), u.NdJ("click", function() { return u.CHM(n), u.oxw().nextSlide() }), u._UZ(1, "span", 15), u.TgZ(2, "span", 13), u._uU(3, "Next"), u.qZA(), u.qZA() } if (2 & t) { var i = u.oxw();
                        u.ekj("disabled", i.checkDisabledClass("next")), u.uIk("data-bs-target", "#" + i.currentId) } } "undefined" == typeof console || console; var Lt = function(t) { return { display: t } },
                    Rt = ["*"],
                    jt = function() { var t = c(function t() { l(this, t), this.interval = 5e3, this.noPause = !1, this.noWrap = !1, this.showIndicators = !0, this.pauseOnFocus = !1, this.indicatorsByChunk = !1, this.itemsPerSlide = 1, this.singleSlideOffset = !1 }); return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275prov = u.Yz7({ token: t, factory: t.\u0275fac, providedIn: "root" }), t }(),
                    Bt = (function(t) { t[t.UNKNOWN = 0] = "UNKNOWN", t[t.NEXT = 1] = "NEXT", t[t.PREV = 2] = "PREV" }(Bt || (Bt = {})), Bt),
                    Jt = 1,
                    Wt = function() { var e = function() {
                            function e(t, n) { l(this, e), this.ngZone = n, this.noWrap = !1, this.noPause = !1, this.showIndicators = !0, this.pauseOnFocus = !1, this.indicatorsByChunk = !1, this.itemsPerSlide = 1, this.singleSlideOffset = !1, this.isAnimated = !1, this.activeSlideChange = new u.vpe(!1), this.slideRangeChange = new u.vpe, this.startFromIndex = 0, this._interval = 5e3, this._slides = new Vt, this._currentVisibleSlidesIndex = 0, this.isPlaying = !1, this.destroyed = !1, this.currentId = 0, this.getActive = function(t) { return t.active }, this.makeSlidesConsistent = function(t) { t.forEach(function(t, e) { return t.item.order = e }) }, Object.assign(this, t), this.currentId = Jt++ } return c(e, [{ key: "activeSlide", get: function() { return this._currentActiveSlide || 0 }, set: function(t) { var e;
                                    this.multilist || (("number" == typeof(e = t) || "[object Number]" === Object.prototype.toString.call(e)) && (this.customActiveSlide = t), this._slides.length && t !== this._currentActiveSlide && this._select(t)) } }, { key: "interval", get: function() { return this._interval }, set: function(t) { this._interval = t, this.restartTimer() } }, { key: "slides", get: function() { return this._slides.toArray() } }, { key: "isFirstSlideVisible", get: function() { var t = this.getVisibleIndexes(); return !(!t || t instanceof Array && !t.length) && t.includes(0) } }, { key: "isLastSlideVisible", get: function() { var t = this.getVisibleIndexes(); return !(!t || t instanceof Array && !t.length) && t.includes(this._slides.length - 1) } }, { key: "isBs4", get: function() { return !Pt() } }, { key: "_bsVer", get: function() { return { isBs3: Pt(), isBs4: It(), isBs5: !Pt() && !It() && (At || (At = wt()), "bs5" === At) } } }, { key: "ngAfterViewInit", value: function() { var t = this;
                                    setTimeout(function() { t.singleSlideOffset && (t.indicatorsByChunk = !1), t.multilist && (t._chunkedSlides = function(t, e) { for (var n = [], i = Math.ceil(t.length / e), s = 0; s < i;) { var o = t.splice(0, s === i - 1 && e < t.length ? t.length : e);
                                                n.push(o), s++ } return n }(t.mapSlidesAndIndexes(), t.itemsPerSlide), t.selectInitialSlides()), t.customActiveSlide && !t.multilist && t._select(t.customActiveSlide) }, 0) } }, { key: "ngOnDestroy", value: function() { this.destroyed = !0 } }, { key: "addSlide", value: function(t) { this._slides.add(t), this.multilist && this._slides.length <= this.itemsPerSlide && (t.active = !0), !this.multilist && this.isAnimated && (t.isAnimated = !0), this.multilist || 1 !== this._slides.length || (this._currentActiveSlide = void 0, this.customActiveSlide || (this.activeSlide = 0), this.play()), this.multilist && this._slides.length > this.itemsPerSlide && this.play() } }, { key: "removeSlide", value: function(t) { var e = this,
                                        n = this._slides.indexOf(t); if (this._currentActiveSlide === n) { var i;
                                        this._slides.length > 1 && (i = this.isLast(n) ? this.noWrap ? n - 1 : 0 : n), this._slides.remove(n), setTimeout(function() { e._select(i) }, 0) } else { this._slides.remove(n); var s = this.getCurrentSlideIndex();
                                        setTimeout(function() { e._currentActiveSlide = s, e.activeSlideChange.emit(e._currentActiveSlide) }, 0) } } }, { key: "nextSlideFromInterval", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                                    this.move(Bt.NEXT, t) } }, { key: "nextSlide", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                                    this.isPlaying && this.restartTimer(), this.move(Bt.NEXT, t) } }, { key: "previousSlide", value: function() { var t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                                    this.isPlaying && this.restartTimer(), this.move(Bt.PREV, t) } }, { key: "getFirstVisibleIndex", value: function() { return this.slides.findIndex(this.getActive) } }, { key: "getLastVisibleIndex", value: function() { return function(t, e) { for (var n = t.length; n--;)
                                            if (e(t[n], n, t)) return n;
                                        return -1 }(this.slides, this.getActive) } }, { key: "move", value: function(t) { var e = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                                        n = this.getFirstVisibleIndex(),
                                        i = this.getLastVisibleIndex();
                                    this.noWrap && (t === Bt.NEXT && this.isLast(i) || t === Bt.PREV && 0 === n) || (this.multilist ? this.moveMultilist(t) : this.activeSlide = this.findNextSlideIndex(t, e) || 0) } }, { key: "keydownPress", value: function(t) { if (13 === t.keyCode || "Enter" === t.key || 32 === t.keyCode || "Space" === t.key) return this.nextSlide(), void t.preventDefault();
                                    37 !== t.keyCode && "LeftArrow" !== t.key ? 39 !== t.keyCode && "RightArrow" !== t.key || this.nextSlide() : this.previousSlide() } }, { key: "onMouseLeave", value: function() { this.pauseOnFocus || this.play() } }, { key: "onMouseUp", value: function() { this.pauseOnFocus || this.play() } }, { key: "pauseFocusIn", value: function() { this.pauseOnFocus && (this.isPlaying = !1, this.resetTimer()) } }, { key: "pauseFocusOut", value: function() { this.play() } }, { key: "selectSlide", value: function(t) { this.isPlaying && this.restartTimer(), this.multilist ? this.selectSlideRange(this.indicatorsByChunk ? t * this.itemsPerSlide : t) : this.activeSlide = this.indicatorsByChunk ? t * this.itemsPerSlide : t } }, { key: "play", value: function() { this.isPlaying || (this.isPlaying = !0, this.restartTimer()) } }, { key: "pause", value: function() { this.noPause || (this.isPlaying = !1, this.resetTimer()) } }, { key: "getCurrentSlideIndex", value: function() { return this._slides.findIndex(this.getActive) } }, { key: "isLast", value: function(t) { return t + 1 >= this._slides.length } }, { key: "isFirst", value: function(t) { return 0 === t } }, { key: "indicatorsSlides", value: function() { var t = this; return this.slides.filter(function(e, n) { return !t.indicatorsByChunk || n % t.itemsPerSlide == 0 }) } }, { key: "selectInitialSlides", value: function() { var e = this.startFromIndex <= this._slides.length ? this.startFromIndex : 0; if (this.hideSlides(), this.singleSlideOffset) { if (this._slidesWithIndexes = this.mapSlidesAndIndexes(), this._slides.length - e < this.itemsPerSlide) { var n = this._slidesWithIndexes.slice(0, e);
                                            this._slidesWithIndexes = [].concat(t(this._slidesWithIndexes), t(n)).slice(n.length).slice(0, this.itemsPerSlide) } else this._slidesWithIndexes = this._slidesWithIndexes.slice(e, e + this.itemsPerSlide);
                                        this._slidesWithIndexes.forEach(function(t) { return t.item.active = !0 }), this.makeSlidesConsistent(this._slidesWithIndexes) } else this.selectRangeByNestedIndex(e);
                                    this.slideRangeChange.emit(this.getVisibleIndexes()) } }, { key: "findNextSlideIndex", value: function(t, e) { var n = 0; if (e || !this.isLast(this.activeSlide) || t === Bt.PREV || !this.noWrap) { switch (t) {
                                            case Bt.NEXT:
                                                if (void 0 === this._currentActiveSlide) { n = 0; break } if (!this.isLast(this._currentActiveSlide)) { n = this._currentActiveSlide + 1; break }
                                                n = !e && this.noWrap ? this._currentActiveSlide : 0; break;
                                            case Bt.PREV:
                                                if (void 0 === this._currentActiveSlide) { n = 0; break } if (this._currentActiveSlide > 0) { n = this._currentActiveSlide - 1; break } if (!e && this.noWrap) { n = this._currentActiveSlide; break }
                                                n = this._slides.length - 1; break;
                                            default:
                                                throw new Error("Unknown direction") } return n } } }, { key: "mapSlidesAndIndexes", value: function() { return this.slides.slice().map(function(t, e) { return { index: e, item: t } }) } }, { key: "selectSlideRange", value: function(t) { if (!this.isIndexInRange(t)) { if (this.hideSlides(), this.singleSlideOffset) { var e = this.isIndexOnTheEdges(t) ? t : t - this.itemsPerSlide + 1,
                                                n = this.isIndexOnTheEdges(t) ? t + this.itemsPerSlide : t + 1;
                                            this._slidesWithIndexes = this.mapSlidesAndIndexes().slice(e, n), this.makeSlidesConsistent(this._slidesWithIndexes), this._slidesWithIndexes.forEach(function(t) { return t.item.active = !0 }) } else this.selectRangeByNestedIndex(t);
                                        this.slideRangeChange.emit(this.getVisibleIndexes()) } } }, { key: "selectRangeByNestedIndex", value: function(t) { if (this._chunkedSlides) { var e = this._chunkedSlides.map(function(t, e) { return { index: e, list: t } }).find(function(e) { return void 0 !== e.list.find(function(e) { return e.index === t }) });
                                        e && (this._currentVisibleSlidesIndex = e.index, this._chunkedSlides[e.index].forEach(function(t) { t.item.active = !0 })) } } }, { key: "isIndexOnTheEdges", value: function(t) { return t + 1 - this.itemsPerSlide <= 0 || t + this.itemsPerSlide <= this._slides.length } }, { key: "isIndexInRange", value: function(t) { return this.singleSlideOffset && this._slidesWithIndexes ? this._slidesWithIndexes.map(function(t) { return t.index }).indexOf(t) >= 0 : t <= this.getLastVisibleIndex() && t >= this.getFirstVisibleIndex() } }, { key: "hideSlides", value: function() { this.slides.forEach(function(t) { return t.active = !1 }) } }, { key: "isVisibleSlideListLast", value: function() { return !!this._chunkedSlides && this._currentVisibleSlidesIndex === this._chunkedSlides.length - 1 } }, { key: "isVisibleSlideListFirst", value: function() { return 0 === this._currentVisibleSlidesIndex } }, { key: "moveSliderByOneItem", value: function(e) { var n, i, s, o, r; if (this.noWrap) { n = this.getFirstVisibleIndex(), i = this.getLastVisibleIndex(), s = e === Bt.NEXT ? n : i, o = e !== Bt.NEXT ? n - 1 : this.isLast(i) ? 0 : i + 1; var a = this._slides.get(s);
                                        a && (a.active = !1); var l = this._slides.get(o);
                                        l && (l.active = !0); var u = this.mapSlidesAndIndexes().filter(function(t) { return t.item.active }); return this.makeSlidesConsistent(u), this.singleSlideOffset && (this._slidesWithIndexes = u), void this.slideRangeChange.emit(this.getVisibleIndexes()) } if (this._slidesWithIndexes && this._slidesWithIndexes[0]) { if (n = this._slidesWithIndexes[0].index, i = this._slidesWithIndexes[this._slidesWithIndexes.length - 1].index, e === Bt.NEXT) { this._slidesWithIndexes.shift(), r = this.isLast(i) ? 0 : i + 1; var c = this._slides.get(r);
                                            c && this._slidesWithIndexes.push({ index: r, item: c }) } else { this._slidesWithIndexes.pop(), r = this.isFirst(n) ? this._slides.length - 1 : n - 1; var d = this._slides.get(r);
                                            d && (this._slidesWithIndexes = [{ index: r, item: d }].concat(t(this._slidesWithIndexes))) }
                                        this.hideSlides(), this._slidesWithIndexes.forEach(function(t) { return t.item.active = !0 }), this.makeSlidesConsistent(this._slidesWithIndexes), this.slideRangeChange.emit(this._slidesWithIndexes.map(function(t) { return t.index })) } } }, { key: "moveMultilist", value: function(t) { this.singleSlideOffset ? this.moveSliderByOneItem(t) : (this.hideSlides(), this._currentVisibleSlidesIndex = this.noWrap ? t === Bt.NEXT ? this._currentVisibleSlidesIndex + 1 : this._currentVisibleSlidesIndex - 1 : t === Bt.NEXT ? this.isVisibleSlideListLast() ? 0 : this._currentVisibleSlidesIndex + 1 : this.isVisibleSlideListFirst() ? this._chunkedSlides ? this._chunkedSlides.length - 1 : 0 : this._currentVisibleSlidesIndex - 1, this._chunkedSlides && this._chunkedSlides[this._currentVisibleSlidesIndex].forEach(function(t) { return t.item.active = !0 }), this.slideRangeChange.emit(this.getVisibleIndexes())) } }, { key: "getVisibleIndexes", value: function() { return !this.singleSlideOffset && this._chunkedSlides ? this._chunkedSlides[this._currentVisibleSlidesIndex].map(function(t) { return t.index }) : this._slidesWithIndexes ? this._slidesWithIndexes.map(function(t) { return t.index }) : void 0 } }, { key: "_select", value: function(t) { if (isNaN(t)) this.pause();
                                    else { if (!this.multilist && void 0 !== this._currentActiveSlide) { var e = this._slides.get(this._currentActiveSlide);
                                            void 0 !== e && (e.active = !1) } var n = this._slides.get(t);
                                        void 0 !== n && (this._currentActiveSlide = t, n.active = !0, this.activeSlide = t, this.activeSlideChange.emit(t)) } } }, { key: "restartTimer", value: function() { var t = this;
                                    this.resetTimer(); var e = +this.interval;!isNaN(e) && e > 0 && (this.currentInterval = this.ngZone.runOutsideAngular(function() { return window.setInterval(function() { var e = +t.interval;
                                            t.ngZone.run(function() { t.isPlaying && !isNaN(t.interval) && e > 0 && t.slides.length ? t.nextSlideFromInterval() : t.pause() }) }, e) })) } }, { key: "multilist", get: function() { return this.itemsPerSlide > 1 } }, { key: "resetTimer", value: function() { this.currentInterval && (clearInterval(this.currentInterval), this.currentInterval = void 0) } }, { key: "checkDisabledClass", value: function(t) { return "prev" === t ? 0 === this.activeSlide && this.noWrap && !this.multilist || this.isFirstSlideVisible && this.noWrap && this.multilist : this.isLast(this.activeSlide) && this.noWrap && !this.multilist || this.isLastSlideVisible && this.noWrap && this.multilist } }]), e }(); return e.\u0275fac = function(t) { return new(t || e)(u.Y36(jt), u.Y36(u.R0b)) }, e.\u0275cmp = u.Xpm({ type: e, selectors: [
                                ["carousel"]
                            ], inputs: { noWrap: "noWrap", noPause: "noPause", showIndicators: "showIndicators", pauseOnFocus: "pauseOnFocus", indicatorsByChunk: "indicatorsByChunk", itemsPerSlide: "itemsPerSlide", singleSlideOffset: "singleSlideOffset", isAnimated: "isAnimated", activeSlide: "activeSlide", startFromIndex: "startFromIndex", interval: "interval" }, outputs: { activeSlideChange: "activeSlideChange", slideRangeChange: "slideRangeChange" }, ngContentSelectors: Rt, decls: 7, vars: 8, consts: [
                                ["tabindex", "0", 1, "carousel", "slide", 3, "id", "mouseenter", "mouseleave", "mouseup", "keydown", "focusin", "focusout"],
                                [4, "ngIf"],
                                [1, "carousel-inner", 3, "ngStyle"],
                                ["class", "left carousel-control carousel-control-prev", "tabindex", "0", "role", "button", 3, "disabled", "click", 4, "ngIf"],
                                ["class", "right carousel-control carousel-control-next", "tabindex", "0", "role", "button", 3, "disabled", "click", 4, "ngIf"],
                                [1, "carousel-indicators"],
                                [3, "active", "click", 4, "ngFor", "ngForOf"],
                                [3, "click"],
                                ["type", "button", "aria-current", "true", 3, "active", "click", 4, "ngFor", "ngForOf"],
                                ["type", "button", "aria-current", "true", 3, "click"],
                                ["tabindex", "0", "role", "button", 1, "left", "carousel-control", "carousel-control-prev", 3, "click"],
                                ["aria-hidden", "true", 1, "icon-prev", "carousel-control-prev-icon"],
                                ["class", "sr-only visually-hidden", 4, "ngIf"],
                                [1, "sr-only", "visually-hidden"],
                                ["tabindex", "0", "role", "button", 1, "right", "carousel-control", "carousel-control-next", 3, "click"],
                                ["aria-hidden", "true", 1, "icon-next", "carousel-control-next-icon"]
                            ], template: function(t, e) { 1 & t && (u.F$t(), u.TgZ(0, "div", 0), u.NdJ("mouseenter", function() { return e.pause() })("mouseleave", function() { return e.onMouseLeave() })("mouseup", function() { return e.onMouseUp() })("keydown", function(t) { return e.keydownPress(t) })("focusin", function() { return e.pauseFocusIn() })("focusout", function() { return e.pauseFocusOut() }), u.YNc(1, Mt, 3, 1, "ng-container", 1), u.YNc(2, Nt, 3, 1, "ng-container", 1), u.TgZ(3, "div", 2), u.Hsn(4), u.qZA(), u.YNc(5, Dt, 3, 4, "a", 3), u.YNc(6, Ft, 4, 3, "a", 4), u.qZA()), 2 & t && (u.Q6J("id", e.currentId), u.xp6(1), u.Q6J("ngIf", !e._bsVer.isBs5 && e.showIndicators && e.slides.length > 1), u.xp6(1), u.Q6J("ngIf", e._bsVer.isBs5 && e.showIndicators && e.slides.length > 1), u.xp6(1), u.Q6J("ngStyle", u.VKq(6, Lt, e.multilist ? "flex" : "block")), u.xp6(2), u.Q6J("ngIf", e.slides.length > 1), u.xp6(1), u.Q6J("ngIf", e.slides.length > 1)) }, directives: [a.O5, a.PC, a.sg], encapsulation: 2 }), e }(),
                    Qt = function() { var t = function() {
                            function t(e) { l(this, t), this.active = !1, this.itemWidth = "100%", this.order = 0, this.isAnimated = !1, this.addClass = !0, this.multilist = !1, this.carousel = e } return c(t, [{ key: "ngOnInit", value: function() { var t;
                                    this.carousel.addSlide(this), this.itemWidth = 100 / this.carousel.itemsPerSlide + "%", this.multilist = (null === (t = this.carousel) || void 0 === t ? void 0 : t.itemsPerSlide) > 1 } }, { key: "ngOnDestroy", value: function() { this.carousel.removeSlide(this) } }]), t }(); return t.\u0275fac = function(e) { return new(e || t)(u.Y36(Wt)) }, t.\u0275cmp = u.Xpm({ type: t, selectors: [
                                ["slide"]
                            ], hostVars: 15, hostBindings: function(t, e) { 2 & t && (u.uIk("aria-hidden", !e.active), u.Udp("width", e.itemWidth)("order", e.order), u.ekj("multilist-margin", e.multilist)("active", e.active)("carousel-animation", e.isAnimated)("item", e.addClass)("carousel-item", e.addClass)) }, inputs: { active: "active" }, ngContentSelectors: Rt, decls: 2, vars: 2, consts: [
                                [1, "item"]
                            ], template: function(t, e) { 1 & t && (u.F$t(), u.TgZ(0, "div", 0), u.Hsn(1), u.qZA()), 2 & t && u.ekj("active", e.active) }, styles: [".carousel-animation[_nghost-%COMP%]{transition:opacity .6s ease,visibility .6s ease;float:left}.carousel-animation.active[_nghost-%COMP%]{opacity:1;visibility:visible}.carousel-animation[_nghost-%COMP%]:not(.active){display:block;position:absolute;opacity:0;visibility:hidden}.multilist-margin[_nghost-%COMP%]{margin-right:auto}.carousel-item[_nghost-%COMP%]{perspective:1000px}"] }), t }(),
                    Yt = function() { var t = function() {
                            function t() { l(this, t) } return c(t, null, [{ key: "forRoot", value: function() { return { ngModule: t, providers: [] } } }]), t }(); return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275mod = u.oAB({ type: t }), t.\u0275inj = u.cJS({ imports: [
                                [a.ez]
                            ] }), t }(),
                    Gt = function() { var t = function() {
                            function t() { l(this, t) } return c(t, [{ key: "ngOnInit", value: function() {} }]), t }(); return t.\u0275fac = function(e) { return new(e || t) }, t.\u0275cmp = u.Xpm({ type: t, selectors: [
                                ["app-banner"]
                            ], decls: 24, vars: 1, consts: [
                                [1, "banner", "mb-3"],
                                [1, "banner-body"],
                                [3, "isAnimated"],
                                ["src", "https://th.bing.com/th/id/OIP.RVi3VgE9dH_PTT8OZLZaRwHaE7?pid=ImgDet&rs=1", "alt", "", 1, "img-fluid", "w-100"],
                                [1, "carousel-caption", "d-none", "d-md-block"],
                                ["src", "https://th.bing.com/th/id/OIP.FGnZeH8uWxsO6bAMsg8FzwHaE3?pid=ImgDet&rs=1", "alt", "", 1, "img-fluid", "w-100"],
                                ["src", "https://ftw.usatoday.com/wp-content/uploads/sites/90/2016/07/afp_552086019_81997609.jpg", "alt", "", 1, "img-fluid", "w-100"]
                            ], template: function(t, e) { 1 & t && (u.TgZ(0, "div", 0), u.TgZ(1, "div", 1), u.TgZ(2, "carousel", 2), u.TgZ(3, "slide"), u._UZ(4, "img", 3), u.TgZ(5, "div", 4), u.TgZ(6, "h3"), u._uU(7, "First slide label"), u.qZA(), u.TgZ(8, "p"), u._uU(9, "Nulla vitae elit libero, a pharetra augue mollis interdum."), u.qZA(), u.qZA(), u.qZA(), u.TgZ(10, "slide"), u._UZ(11, "img", 5), u.TgZ(12, "div", 4), u.TgZ(13, "h3"), u._uU(14, "First slide label"), u.qZA(), u.TgZ(15, "p"), u._uU(16, "Nulla vitae elit libero, a pharetra augue mollis interdum."), u.qZA(), u.qZA(), u.qZA(), u.TgZ(17, "slide"), u._UZ(18, "img", 6), u.TgZ(19, "div", 4), u.TgZ(20, "h3"), u._uU(21, "First slide label"), u.qZA(), u.TgZ(22, "p"), u._uU(23, "Nulla vitae elit libero, a pharetra augue mollis interdum."), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.qZA()), 2 & t && (u.xp6(2), u.Q6J("isAnimated", !0)) }, directives: [Wt, Qt], styles: [""] }), t }(),
                    Ht = function(t) { return { active: t } };

                function zt(t, e) { if (1 & t) { var n = u.EpF();
                        u.TgZ(0, "div", 19), u.NdJ("click", function() { var t = u.CHM(n).$implicit; return u.oxw().getRounds(t.SeriesID, t.SeriesGUID) }), u.TgZ(1, "div", 20), u._UZ(2, "img", 21), u.qZA(), u.TgZ(3, "div", 22), u._uU(4), u.qZA(), u.qZA() } if (2 & t) { var i = e.$implicit,
                            s = u.oxw();
                        u.xp6(2), u.Q6J("src", i.SeriesFlag, u.LSH), u.xp6(1), u.Q6J("ngClass", u.VKq(3, Ht, s.selectedSeriesId == i.SeriesID)), u.xp6(1), u.Oqu(i.SeriesName) } }

                function Xt(t, e) { if (1 & t) { var n = u.EpF();
                        u.TgZ(0, "div", 19), u.NdJ("click", function() { var t = u.CHM(n).$implicit; return u.oxw().getMatches(t.RoundID) }), u.TgZ(1, "div", 22), u.TgZ(2, "h5"), u._uU(3), u.qZA(), u.TgZ(4, "p"), u._uU(5), u.ALo(6, "date"), u.ALo(7, "date"), u.qZA(), u.qZA(), u.qZA() } if (2 & t) { var i = e.$implicit,
                            s = u.oxw();
                        u.xp6(1), u.Q6J("ngClass", u.VKq(10, Ht, s.selectedRoundsId == i.RoundID)), u.xp6(2), u.Oqu(i.RoundName), u.xp6(2), u.AsE("", u.xi3(6, 4, i.SeriesStartDateUTC, "dd-MM-yyy"), " - ", u.xi3(7, 7, i.SeriesEndDateUTC, "dd-MM-yyy"), " ") } }

                function Kt(t, e) { 1 & t && (u.TgZ(0, "div", 23), u.TgZ(1, "div", 24), u.TgZ(2, "span", 25), u._uU(3, "Make your predictions"), u.qZA(), u.TgZ(4, "span"), u._uU(5, "Monday, 19 July 2021"), u.qZA(), u.qZA(), u.qZA()) }

                function $t(t, e) { if (1 & t && (u.TgZ(0, "div", 26), u.TgZ(1, "div", 27), u.TgZ(2, "div", 28), u.TgZ(3, "a", 29), u._UZ(4, "i", 30), u.qZA(), u.qZA(), u.TgZ(5, "div", 4), u.TgZ(6, "div", 31), u._UZ(7, "img", 32), u.TgZ(8, "p"), u._uU(9), u.qZA(), u.qZA(), u.TgZ(10, "div", 31), u.TgZ(11, "p", 33), u._uU(12), u.ALo(13, "date"), u.qZA(), u.qZA(), u.TgZ(14, "div", 31), u._UZ(15, "img", 32), u.TgZ(16, "p"), u._uU(17), u.qZA(), u.qZA(), u.qZA(), u.TgZ(18, "div", 4), u.TgZ(19, "div", 34), u.TgZ(20, "button", 35), u._uU(21, "WIN"), u.qZA(), u.qZA(), u.TgZ(22, "div", 34), u.TgZ(23, "button", 35), u._uU(24, "TIE"), u.qZA(), u.qZA(), u.TgZ(25, "div", 34), u.TgZ(26, "button", 35), u._uU(27, "WIN"), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.qZA()), 2 & t) { var n = e.$implicit;
                        u.xp6(7), u.Q6J("src", n.TeamFlagLocal, u.LSH), u.xp6(2), u.Oqu(n.MatchScoreDetails.TeamScoreLocal.Name), u.xp6(3), u.Oqu(u.xi3(13, 5, n.MatchStartDateTimeUTC, "hh:mm a")), u.xp6(3), u.Q6J("src", n.TeamFlagVisitor, u.LSH), u.xp6(2), u.Oqu(n.MatchScoreDetails.TeamScoreVisitor.Name) } }

                function te(t, e) { 1 & t && (u.TgZ(0, "div", 36), u.TgZ(1, "div", 37), u.TgZ(2, "button", 38), u._uU(3, "My Prediction"), u.qZA(), u.TgZ(4, "button", 38), u._uU(5, "Results"), u.qZA(), u.TgZ(6, "button", 38), u._uU(7, "Our prizes"), u.qZA(), u.qZA(), u.qZA()) }

                function ee(t, e) { if (1 & t && (u.TgZ(0, "div", 41), u._UZ(1, "img", 42), u.TgZ(2, "small", 43), u._uU(3), u.ALo(4, "date"), u.qZA(), u.TgZ(5, "h2", 44), u._uU(6), u.qZA(), u.TgZ(7, "span"), u.TgZ(8, "a", 29), u._uU(9), u.qZA(), u.qZA(), u.qZA()), 2 & t) { var n = e.$implicit;
                        u.xp6(1), u.Q6J("src", n.media_url, u.LSH), u.xp6(2), u.Oqu(u.xi3(4, 4, n.news_date, "dd MMM yyyy")), u.xp6(3), u.Oqu(n.title), u.xp6(3), u.Oqu(n.source_name) } } var ne = function(t) { return { "show active news-tab": t } };

                function ie(t, e) { if (1 & t && (u.TgZ(0, "div", 39), u.YNc(1, ee, 10, 7, "div", 40), u.qZA()), 2 & t) { var n = u.oxw();
                        u.Q6J("ngClass", u.VKq(2, ne, "News" == n.tab)), u.xp6(1), u.Q6J("ngForOf", n.newsList.slice(0, 10)) } }

                function se(t, e) { if (1 & t && (u.TgZ(0, "div", 52), u.TgZ(1, "div", 4), u.TgZ(2, "div", 53), u._UZ(3, "img", 54), u._uU(4), u.qZA(), u.TgZ(5, "div", 55), u._uU(6), u.ALo(7, "date"), u.qZA(), u.TgZ(8, "div", 56), u._uU(9), u._UZ(10, "img", 54), u.qZA(), u.qZA(), u.qZA()), 2 & t) { var n = e.$implicit;
                        u.xp6(3), u.Q6J("src", n.TeamFlagLocal, u.LSH), u.xp6(1), u.hij(" ", n.MatchScoreDetails.TeamScoreLocal.Name, " "), u.xp6(2), u.hij(" ", u.xi3(7, 5, n.MatchStartDateTimeUTC, "hh:mm a"), " "), u.xp6(3), u.hij(" ", n.MatchScoreDetails.TeamScoreVisitor.Name, " "), u.xp6(1), u.Q6J("src", n.TeamFlagVisitor, u.LSH) } }

                function oe(t, e) { if (1 & t && (u.TgZ(0, "div", 45), u.TgZ(1, "div", 46), u.TgZ(2, "div", 47), u.TgZ(3, "p"), u.TgZ(4, "strong"), u._uU(5, "Yesterday"), u.qZA(), u.qZA(), u.TgZ(6, "span"), u._uU(7, "19-02-2021"), u.qZA(), u.qZA(), u.TgZ(8, "div", 47), u.TgZ(9, "p"), u.TgZ(10, "strong"), u._uU(11, "Today"), u.qZA(), u.qZA(), u.TgZ(12, "span"), u._uU(13, "19-02-2021"), u.qZA(), u.qZA(), u.TgZ(14, "div", 47), u.TgZ(15, "p"), u.TgZ(16, "strong"), u._uU(17, "Tomorrow"), u.qZA(), u.qZA(), u.TgZ(18, "span"), u._uU(19, "19-02-2021"), u.qZA(), u.qZA(), u.qZA(), u.TgZ(20, "div", 48), u.TgZ(21, "div", 49), u.TgZ(22, "h4"), u._uU(23, "Saudi Professional League"), u.qZA(), u.qZA(), u.TgZ(24, "div", 50), u.YNc(25, se, 11, 8, "div", 51), u.qZA(), u.qZA(), u.qZA()), 2 & t) { var n = u.oxw();
                        u.Q6J("ngClass", u.VKq(2, ne, "All Matches" == n.tab)), u.xp6(25), u.Q6J("ngForOf", n.matchesByRounds) } } var re, ae = function() { var t = function() {
                            function t(e, n) { l(this, t), this.newsService = e, this.seriesService = n, this.tab = "News", this.imagePlaceholderUrl = st.N.imagePlaceholderUrl, this.selectedSeriesId = "", this.selectedRoundsId = "", this.newsList = [], this.seriesList = [], this.roundsBySeries = [], this.matchesByRounds = [] } return c(t, [{ key: "ngOnInit", value: function() { this.getSeries(), this.getNews() } }, { key: "changeTab", value: function(t) { this.tab = t, "All Matches" == t && this.getMatches("") } }, { key: "getNews", value: function() { var t = this;
                                    this.newsService.getAllNews().subscribe(function(e) { t.newsList = e.Data.Records }) } }, { key: "getSeries", value: function() { var t = this;
                                    this.seriesService.getAllSeries().subscribe(function(e) { t.seriesList = e.Data.Records }) } }, { key: "getRounds", value: function(t, e) { var n = this;
                                    this.selectedSeriesId = t, this.selectedSeriesGUID = e, this.seriesService.getRoundsBySeries(e).subscribe(function(t) { n.roundsBySeries = t.Data.Records }) } }, { key: "getMatches", value: function(t) { var e = this;
                                    this.selectedRoundsId = t, this.seriesService.getMatchesByRounds(this.selectedSeriesGUID, t).subscribe(function(t) { e.matchesByRounds = t.Data.Records }) } }]), t }(); return t.\u0275fac = function(e) { return new(e || t)(u.Y36(St), u.Y36(Tt)) }, t.\u0275cmp = u.Xpm({ type: t, selectors: [
                                ["app-home"]
                            ], decls: 24, vars: 13, consts: [
                                [1, "wrapper"],
                                [1, "container"],
                                [1, "row", "mt-4", "mb-4"],
                                [1, "col-lg-8"],
                                [1, "row"],
                                [1, "owl-carousel"],
                                ["class", "owl-item", 3, "click", 4, "ngFor", "ngForOf"],
                                [1, "owl-carousel", "mt-4"],
                                ["class", "col-md-12 mt-4", 4, "ngIf"],
                                ["class", "col-md-6", 4, "ngFor", "ngForOf"],
                                ["class", "row main-btn mt-4", 4, "ngIf"],
                                [1, "col-lg-4"],
                                ["id", "pills-tab", "role", "tablist", 1, "nav", "nav-pills", "mb-1", "aside-tab"],
                                ["role", "presentation", 1, "nav-item", 2, "cursor", "pointer", 3, "click"],
                                ["id", "pills-home-tab", "data-toggle", "pill", "role", "tab", "aria-controls", "pills-home", "aria-selected", "true", 1, "nav-link", 3, "ngClass"],
                                ["id", "pills-profile-tab", "data-toggle", "pill", "role", "tab", "aria-controls", "pills-profile", "aria-selected", "false", 1, "nav-link", 3, "ngClass"],
                                ["id", "pills-tabContent", 1, "tab-content", "rounded-bottom"],
                                ["class", "tab-pane fade", "id", "pills-home", "role", "tabpanel", "aria-labelledby", "pills-home-tab", 3, "ngClass", 4, "ngIf"],
                                ["class", "tab-panel fade", "id", "pills-profile", "role", "tabpanel", "aria-labelledby", "pills-profile-tab", 3, "ngClass", 4, "ngIf"],
                                [1, "owl-item", 3, "click"],
                                [1, "league-logo"],
                                ["alt", "goalak", 3, "src"],
                                [1, "league-name", 3, "ngClass"],
                                [1, "col-md-12", "mt-4"],
                                [1, "heading-box"],
                                [1, "black"],
                                [1, "col-md-6"],
                                [1, "matches-box"],
                                [1, "icon"],
                                ["href", "#"],
                                [1, "bi", "bi-share-fill"],
                                [1, "col-md-4", "eboxes"],
                                ["alt", "", 3, "src"],
                                [1, "time"],
                                [1, "col-md-4"],
                                ["type", "button", 1, "btn", "btn-secondary"],
                                [1, "row", "main-btn", "mt-4"],
                                [1, "col-md-12"],
                                ["type", "button", 1, "btn", "btn-success", "shadow-sm"],
                                ["id", "pills-home", "role", "tabpanel", "aria-labelledby", "pills-home-tab", 1, "tab-pane", "fade", 3, "ngClass"],
                                ["class", "news-box", 4, "ngFor", "ngForOf"],
                                [1, "news-box"],
                                ["onerror", "this.src='https://forodesaopaulo.org/wp-content/uploads/2019/10/image-placeholder-26-min-uai-1032x688.jpg'", "alt", "goalak", 1, "mb-2", 3, "src"],
                                [1, "text-muted"],
                                [1, "mt-2"],
                                ["id", "pills-profile", "role", "tabpanel", "aria-labelledby", "pills-profile-tab", 1, "tab-panel", "fade", 3, "ngClass"],
                                [1, "match-tab"],
                                [1, "col-md-4", "match-day-box"],
                                [1, "row", "match-box"],
                                [1, "col-12", "mt-4", "mb-3"],
                                [1, "col-12"],
                                ["class", "list mr-1 ml-1 p-2 mb-1", 4, "ngFor", "ngForOf"],
                                [1, "list", "mr-1", "ml-1", "p-2", "mb-1"],
                                [1, "col-4"],
                                ["width", "22", "alt", "goalak", 3, "src"],
                                [1, "col-4", "bg-white", "text-center", "pt-1"],
                                [1, "col-4", 2, "text-align", "right"]
                            ], template: function(t, e) { 1 & t && (u.TgZ(0, "div", 0), u.TgZ(1, "div", 1), u.TgZ(2, "div", 2), u.TgZ(3, "div", 3), u._UZ(4, "app-banner"), u.TgZ(5, "div", 4), u.TgZ(6, "div", 5), u.YNc(7, zt, 5, 5, "div", 6), u.qZA(), u.TgZ(8, "div", 7), u.YNc(9, Xt, 8, 12, "div", 6), u.qZA(), u.YNc(10, Kt, 6, 0, "div", 8), u.YNc(11, $t, 28, 8, "div", 9), u.YNc(12, te, 8, 0, "div", 10), u.qZA(), u.qZA(), u.TgZ(13, "div", 11), u.TgZ(14, "ul", 12), u.TgZ(15, "li", 13), u.NdJ("click", function() { return e.changeTab("News") }), u.TgZ(16, "a", 14), u._uU(17, "News"), u.qZA(), u.qZA(), u.TgZ(18, "li", 13), u.NdJ("click", function() { return e.changeTab("All Matches") }), u.TgZ(19, "a", 15), u._uU(20, "All Matches"), u.qZA(), u.qZA(), u.qZA(), u.TgZ(21, "div", 16), u.YNc(22, ie, 2, 4, "div", 17), u.YNc(23, oe, 26, 4, "div", 18), u.qZA(), u.qZA(), u.qZA(), u.qZA(), u.qZA()), 2 & t && (u.xp6(7), u.Q6J("ngForOf", e.seriesList), u.xp6(2), u.Q6J("ngForOf", e.roundsBySeries), u.xp6(1), u.Q6J("ngIf", (null == e.matchesByRounds ? null : e.matchesByRounds.length) > 0), u.xp6(1), u.Q6J("ngForOf", e.matchesByRounds), u.xp6(1), u.Q6J("ngIf", (null == e.matchesByRounds ? null : e.matchesByRounds.length) > 0), u.xp6(4), u.Q6J("ngClass", u.VKq(9, Ht, "News" == e.tab)), u.xp6(3), u.Q6J("ngClass", u.VKq(11, Ht, "All Matches" == e.tab)), u.xp6(3), u.Q6J("ngIf", "News" == e.tab), u.xp6(1), u.Q6J("ngIf", "All Matches" == e.tab)) }, directives: [Gt, a.sg, a.O5, a.mk], pipes: [a.uU], styles: [".owl-carousel[_ngcontent-%COMP%]{width:100%;overflow:auto;white-space:nowrap;display:flex;flex-direction:row;grid-gap:1rem;gap:1rem}.owl-carousel[_ngcontent-%COMP%]::-webkit-scrollbar{display:none!important;width:0!important;height:0!important;background:transparent!important}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]{text-align:center;cursor:pointer}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]:hover   .league-name[_ngcontent-%COMP%]{background-color:#ffd710}.owl-carousel.owl-drag[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]{background-color:#fff}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .league-logo[_ngcontent-%COMP%]{height:80px}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .league-logo[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:auto;margin-left:auto;margin-right:auto;padding:10px;height:80px}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .league-name[_ngcontent-%COMP%]{border-top:1px solid #f4f4f4;border-right:1px solid #f4f4f4;padding:5px 10px;font-size:12px;text-align:center;font-weight:600;border-radius:3px}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .league-name[_ngcontent-%COMP%]   h5[_ngcontent-%COMP%]{font-size:14px;font-weight:600}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .league-name[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{font-size:12px;margin-bottom:0}.owl-nav[_ngcontent-%COMP%]{display:none}.owl-carousel[_ngcontent-%COMP%]   .owl-item[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]{background-color:#ffd710}.heading-box[_ngcontent-%COMP%]{display:flex;justify-content:space-between;align-items:center;background-color:red;background-image:linear-gradient(90deg,#f4f4f4,#777);padding:7px 15px;color:#fff;font-weight:500}.heading-box[_ngcontent-%COMP%]   .black[_ngcontent-%COMP%]{color:#000}.matches-box[_ngcontent-%COMP%]{padding:15px;background:transparent;cursor:pointer;margin-top:15px;border-top:1px solid #f4f4f4;border-right:1px solid #f4f4f4}.matches-box[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%]{background-color:#eb5d6e;color:#fff}.matches-box[_ngcontent-%COMP%]   .eboxes[_ngcontent-%COMP%]{display:flex;flex-direction:column;align-items:center;justify-content:center}.matches-box[_ngcontent-%COMP%]   .eboxes[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:50px}.matches-box[_ngcontent-%COMP%]   .eboxes[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{margin-top:10px;font-weight:400;font-size:14px}.matches-box[_ngcontent-%COMP%]   .eboxes[_ngcontent-%COMP%]   .time[_ngcontent-%COMP%]{color:#eb5d6e}.btn-secondary[_ngcontent-%COMP%]{width:100%;font-size:10px}.btn-secondary[_ngcontent-%COMP%]:hover{background:#007e0d}.main-btn[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]{background-color:#eb5d6e;min-width:120px;border:none;border-radius:30px;font-size:14px;margin:10px}.nav-item[_ngcontent-%COMP%]{border:1px solid #0d6efd;border-radius:5px}.nav-item[_ngcontent-%COMP%]:hover   a[_ngcontent-%COMP%]{background-color:#0d6efd;color:#fff}.news-box[_ngcontent-%COMP%]{padding:10px;cursor:pointer;margin-top:15px;border-top:1px solid #f4f4f4;border-left:1px solid #f4f4f4}.match-tab[_ngcontent-%COMP%]{display:flex;margin-top:20px}.match-day-box[_ngcontent-%COMP%]{cursor:pointer;padding:10px;font-size:12px;font-weight:500;text-align:center;border-radius:5px}.match-day-box[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{font-size:12px;margin-bottom:0}.match-day-box[_ngcontent-%COMP%]:hover{background:#ffd710}"] }), t }(),
                    le = ((re = c(function t() { l(this, t) })).\u0275fac = function(t) { return new(t || re) }, re.\u0275mod = u.oAB({ type: re }), re.\u0275inj = u.cJS({ imports: [
                            [a.ez, nt.Tx, it.ZX, tt, Yt.forRoot(), et.Bz.forChild([{ path: "", component: xt, children: [{ path: "", redirectTo: "dashboard", pathMatch: "full" }, { path: "dashboard", component: ae }] }])]
                        ] }), re) } }
    ]) }();