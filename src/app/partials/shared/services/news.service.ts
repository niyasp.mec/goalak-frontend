import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { RestService } from '../utils/rest.service';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient, private rest: RestService) { }

  getAllNews() {
    return this.rest.makeGetRequest(`${environment.apiUrl}/football/sports/getNews`)
  }
}
