import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { RestService } from '../utils/rest.service';

@Injectable({
  providedIn: 'root'
})
export class SeriesService {

  constructor(private http: HttpClient, private rest: RestService) { }

  getAllSeries() {
    let payload = {
      "Params": "Status,TotalMatches,SeriesFlag,SeriesID,hasStandings,seriesGUID",
      "StatusID": [1, 2],
      "OrderBy": "Status"
    }

    return this.rest.makePostRequest(`${environment.apiUrl}/football/sports/getSeries`, payload)
  }

  getRoundsBySeries(seriesGUID: string) {
    let payload = {
      "SeriesGUID": seriesGUID,
      "Params": "TotalLevels,RoundID,StatusID,SeriesType,RoundFormat,SeriesIDLive,SeriesStartDate,SeriesEndDate,SeriesStartDateUTC,SeriesEndDateUTC,TotalMatches,SeriesMatchStartDate,Status",
      "SessionKey": "08925584-3927-4517-30cf-c4a910cc4e4c"
    }

    return this.rest.makePostRequest(`${environment.apiUrl}/football/series/getRounds`, payload)
  }

  getMatchesByRounds(seriesGUID: string,roundID:string) {
    let payload = {
      "Params": "isPredicted,TeamNameShortVisitor,TeamNameShortLocal,Status,TeamFlagLocal,TeamFlagVisitor,MatchStartDateTimeUTC,WinPoints,PredictionData,Statistics,MatchScoreDetails,SeriesName,SeriesID,StatusID,SeriesGUID",
      "SeriesGUID": seriesGUID,
      "SessionKey":"08925584-3927-4517-30cf-c4a910cc4e4c",
      "RoundID":roundID,
      "Status":"Pending"

      
    }

    return this.rest.makePostRequest(`${environment.apiUrl}/football/sports/getMatches`, payload)
  }
}
