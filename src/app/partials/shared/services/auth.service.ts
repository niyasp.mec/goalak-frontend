import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { RestService } from '../utils/rest.service';
import { UserProfile } from '../models/auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private rest: RestService) { }

  generateOTP(phoneNo: string) {
    let payload = {
      "CountryPhoneCode": "91",
      "PhoneNumber": phoneNo,
      "Source": "Otp"
    }

    return this.rest.makePostRequest(`${environment.apiUrl}/signin/OtpSignIn`, payload)
  }

  signIn(phoneNo: string, otp: string) {
    let payload = {
      "PhoneNumber": phoneNo, 
      "OTP": otp,
      "Source": "Otp", 
      "DeviceType": "iPhone"
    }

    return this.rest.makePostRequest(`${environment.apiUrl}/signin`, payload)
  }

  getProfile() {
    let userProfile = JSON.parse(localStorage.getItem('userProfile')!);
    let payload = {
      "SessionKey": userProfile?.SessionKey, 
      "Params": "CountryPhoneCode,FirstName,Email,Gender,CountryCode,CityName,PhoneNumber,ProfilePic,Source"
    }

    return this.rest.makePostRequest(`${environment.apiUrl}/users/getProfile`, payload)
  }

  updateProfile(data: UserProfile) {
    let userProfile = JSON.parse(localStorage.getItem('userProfile')!);
    data.SessionKey = userProfile?.SessionKey
    let payload = data

    return this.rest.makePostRequest(`${environment.apiUrl}/users/updateUserInfo`, payload)
  }
}
