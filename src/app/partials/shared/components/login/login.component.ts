import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  @Output() closeMenuEvent = new EventEmitter<string>();
  @Input() formType: string = 'login';
  mobileNo: string = ''
  otp: string = ''

  constructor(
    private authService: AuthService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    
  }

  clearAll(isClose: boolean) {
    this.formType = 'login'
    this.mobileNo = ''
    this.otp = ''
    if (isClose) {
      this.closeMenuEvent.emit('closeMenu')
    }
  }

  generateOTP() {
    if (!this.mobileNo) {
      this._snackBar.open('Mobile number should not be empty!', '', {
        horizontalPosition: 'end',
        verticalPosition: 'bottom',
        duration: 2000
      });
    } else {
      this.authService.generateOTP(this.mobileNo).subscribe((res) => {
        this.formType = 'otp'
        this._snackBar.open('Your OTP: ' + res?.Data?.Token, '', {
          horizontalPosition: 'end',
          verticalPosition: 'bottom',
          duration: 2000
        });
      }, err => {
        this._snackBar.open('Sorry, Something went wrong. Please try again after some time!', '', {
          horizontalPosition: 'end',
          verticalPosition: 'bottom',
          duration: 2000
        });
        this.clearAll(true)
      })
    }
  }

  signIn() {
    if (!this.otp) {
      this._snackBar.open('OTP should not be empty!', '', {
        horizontalPosition: 'end',
        verticalPosition: 'bottom',
        duration: 2000
      });
    } else {
      this.authService.signIn(this.mobileNo, this.otp).subscribe((res) => {
        if (res.ResponseCode == "200") {
          localStorage.setItem('userProfile', JSON.stringify(res?.Data))
          this._snackBar.open('Login Successful.', '', {
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
            duration: 2000
          });
        } else {
          this._snackBar.open(res.Message, '', {
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
            duration: 2000
          });
        }
        this.clearAll(true)
      }, err => {
        this._snackBar.open('Sorry, Something went wrong. Please try again after some time!', '', {
          horizontalPosition: 'end',
          verticalPosition: 'bottom',
          duration: 2000
        });
        this.clearAll(true)
      })
    }
  }

  ngOnDestroy(): void {
    this.clearAll(true)
  }
}
