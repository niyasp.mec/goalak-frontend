import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../services/auth.service';
import { MyAccountComponent } from '../my-account/my-account.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @ViewChild('menuTrigger') trigger: any;
  @ViewChild('loginPage') loginPage: any;
  isCollapsed: boolean = true;
  isLoggedIn = false

  constructor(
    private authService: AuthService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.checkLoggedInOrNot()
  }

  closeLoginDialog() {
    this.trigger.closeMenu()
    this.checkLoggedInOrNot()
  }

  openLoginDialog() {
    this.trigger.openMenu()
    this.loginPage.clearAll(false)
  }  

  checkLoggedInOrNot() {
    if (localStorage.getItem('userProfile')) {
      this.isLoggedIn = true
    } else {
      this.isLoggedIn = false
    }
  }

  showMyAccount() {
    this.authService.getProfile().subscribe((res) => {
      if (res.ResponseCode == "200") {
        const dialogRef =  this.dialog.open(MyAccountComponent, {
          width: '100%',
          maxWidth: '500px',
          data: res.Data
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result?.event == '1') {
            
          } else if (result?.event == '0') {
  
          }
        });
      } else {
        this._snackBar.open('Sorry, some trouble to open profile!', '', {
          horizontalPosition: 'end',
          verticalPosition: 'bottom',
          duration: 2000
        });
      }
    })
  }

  logout() {
    localStorage.setItem('userProfile', '')
    this.checkLoggedInOrNot()
  }

}
