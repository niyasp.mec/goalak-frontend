import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../services/auth.service';
import { UserProfile } from '../../models/auth.model';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<MyAccountComponent>,
    @Inject(MAT_DIALOG_DATA) public info: UserProfile,
    private authService: AuthService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
  }

  updateProfile() {
    this.authService.updateProfile(this.info).subscribe((res) => {
      if (res.ResponseCode == "200") {
        this._snackBar.open('Profile updated successfully.', '', {
          horizontalPosition: 'end',
          verticalPosition: 'bottom',
          duration: 2000
        });
      } else {
        this._snackBar.open(res.Message, '', {
          horizontalPosition: 'end',
          verticalPosition: 'bottom',
          duration: 2000
        });
      }
    }, err=> {
      this._snackBar.open('Sorry, Unable to update profile!', '', {
        horizontalPosition: 'end',
        verticalPosition: 'bottom',
        duration: 2000
      });
    })
  }

}
