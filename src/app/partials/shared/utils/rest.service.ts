import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  /*
  * Get Methods
  */
  public makeGetRequest(url: string, isLoad: boolean = true): Observable<any> {
    return this.http
      .get(url,).pipe(tap(_ => {}),
        catchError(err => {
          return throwError(err);
        }));
  }

  public makePostRequest(url: string, payload: any): Observable<any> {
    return this.http
      .post(url, payload,).pipe(tap(_ => {}),
        catchError(err => {
          return throwError(err);
        }));
  }

}
