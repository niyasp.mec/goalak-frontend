export interface UserProfile {
    CityName: string
    CountryCode: string
    Email: string
    FullName: string
    Gender: string
    PhoneNumber: string
    ProfilePic: string
    UserGUID: string
    UserID: string
    SessionKey: string
}