export interface News {
  id: string;
  news_type: string;
  news_date: string;
  source_tag: string;
  source_name: string;
  title: string;
  link: string;
  guid: string;
  author: string;
  media_url: string;
  video_url: string;
  news_tags: string;
}
