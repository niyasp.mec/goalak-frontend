import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from "@angular/material/dialog";
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { PartialsComponent } from './partials.component';
import { HomeComponent } from './pages/home/home.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { BannerComponent } from './shared/components/banner/banner.component';
import { LoginComponent } from './shared/components/login/login.component';
import { MyAccountComponent } from './shared/components/my-account/my-account.component';



@NgModule({
  declarations: [
    PartialsComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    BannerComponent,
    LoginComponent,
    MyAccountComponent
  ],
  imports: [
    CommonModule,
    MatMenuModule,
    MatSnackBarModule,
    MatDialogModule,
    FormsModule,
    CarouselModule.forRoot(),
    RouterModule.forChild([
      {
        path: '', component: PartialsComponent, children: [
          { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
          { path: 'dashboard', component: HomeComponent },
        ]
      }
    ])
  ]
})
export class PartialsModule { }
