import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { NewsService } from '../../shared/services/news.service';
import { SeriesService } from '../../shared/services/series.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  tab = 'News'
  imagePlaceholderUrl = environment.imagePlaceholderUrl
  selectedSeriesId: string = ''
  selectedRoundsId: string = ''
  newsList: any = []
  seriesList: any = []
  roundsBySeries: any = []
  matchesByRounds: any = []
  selectedSeriesGUID!: string;

  constructor(
    private newsService: NewsService,
    private seriesService: SeriesService
  ) { }

  ngOnInit(): void {
    this.getSeries()
    this.getNews()
  }

  changeTab(name: string) {
    this.tab = name
    if (name == 'All Matches') {
      this.getMatches('')
    }
  }

  getNews(): void {
    this.newsService.getAllNews().subscribe((res) => {
      this.newsList = res.Data.Records
    })
  }

  getSeries() {
    this.seriesService.getAllSeries().subscribe((res) => {
      this.seriesList = res.Data.Records
    })
  }

  getRounds(seriesId: string,seriesGUID:string) {
    this.selectedSeriesId = seriesId
    this.selectedSeriesGUID = seriesGUID
    this.seriesService.getRoundsBySeries(seriesGUID).subscribe((res) => {
      this.roundsBySeries = res.Data.Records
    })
  }

  getMatches(id: string) {
    this.selectedRoundsId = id
    this.seriesService.getMatchesByRounds(this.selectedSeriesGUID,id).subscribe((res) => {
      this.matchesByRounds = res.Data.Records
    })
  }

}
